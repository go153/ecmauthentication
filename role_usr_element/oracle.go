package role_usr_element

import (
	"database/sql"

	"github.com/lib/pq"
	"gitlab.com/e-capture/ecatch-ecm/majosystem/db"
	"gitlab.com/e-capture/ecatch-ecm/majosystem/logger_trace"
)

type orcl struct{}

const (
	orclInsert                  = `INSERT INTO usr_role_usr_element (role_id, element_id) VALUES (:1, :2)`
	orclUpdate                  = `UPDATE usr_role_usr_element SET role_id = :1, element_id = :2, updated_at = getdate() WHERE id = :3`
	orclDelete                  = `DELETE FROM usr_role_usr_element WHERE id = :1`
	orclDeleteByRoleID          = `DELETE FROM usr_role_usr_element WHERE role_id = :1`
	orclDeleteByRoleComponentID = `DELETE FROM dbo.usr_role_usr_element WHERE id in (
									SELECT rue.id
									FROM dbo.usr_elements e WITH (NOLOCK)
									INNER JOIN dbo.usr_components c WITH (NOLOCK) ON (c.id = e.component_id AND e.component_id = :2)
									INNER JOIN dbo.usr_roles r WITH (NOLOCK) ON (r.id = :1)
									INNER JOIN dbo.usr_role_usr_element rue WITH (NOLOCK) ON (rue.role_id = r.id AND rue.element_id = e.id)
								)`
	orclGetByID = `SELECT isnull(rue.id, 0) id, isnull(e.id, 0) element_id, e.name elementname, r.id role_id, r.name as rolename, c.id component_id, c.name componentname, isnull(rue.get, 0) get, isnull(rue.post, 0) post, isnull(rue.del, 0) del, isnull(rue.upd, 0) upd, rue.created_at, rue.updated_at
							FROM dbo.usr_elements e WITH (NOLOCK)
							INNER JOIN dbo.usr_components c WITH (NOLOCK) ON (c.id = e.component_id)
							INNER JOIN dbo.usr_role_usr_element rue WITH (NOLOCK) ON (rue.element_id = e.id)
							INNER JOIN dbo.usr_roles r WITH (NOLOCK) ON (r.id = rue.role_id)
							WHERE rue.id = :1`
	orclGetByRoleID = `SELECT isnull(rue.id, 0) id, isnull(e.id, 0) element_id, e.name elementname, r.id role_id, r.name as rolename, c.id component_id, c.name componentname, isnull(rue.get, 0) get, isnull(rue.post, 0) post, isnull(rue.del, 0) del, isnull(rue.upd, 0) upd, rue.created_at, rue.updated_at
							FROM dbo.usr_elements e WITH (NOLOCK)
							INNER JOIN dbo.usr_components c WITH (NOLOCK) ON (c.id = e.component_id)
							INNER JOIN dbo.usr_roles r WITH (NOLOCK) ON (r.id = :1)
							LEFT JOIN dbo.usr_role_usr_element rue WITH (NOLOCK) ON (rue.role_id = r.id AND rue.element_id = e.id)
							ORDER BY component_id, element_id`
	orclGetByComponentID = `SELECT isnull(rue.id, 0) id, isnull(e.id, 0) element_id, e.name elementname, r.id role_id, r.name as rolename, c.id component_id, c.name componentname, isnull(rue.get, 0) get, isnull(rue.post, 0) post, isnull(rue.del, 0) del, isnull(rue.upd, 0) upd, rue.created_at, rue.updated_at
							FROM dbo.usr_elements e WITH (NOLOCK)
							INNER JOIN dbo.usr_components c WITH (NOLOCK) ON (c.id = e.component_id AND e.component_id = :2)
							INNER JOIN dbo.usr_roles r WITH (NOLOCK) ON (r.id = :1)
							LEFT JOIN dbo.usr_role_usr_element rue WITH (NOLOCK) ON (rue.role_id = r.id AND rue.element_id = e.id)
							ORDER BY component_id, element_id`
	orclGetAll = `SELECT isnull(rue.id, 0) id, isnull(e.id, 0) element_id, e.name elementname, r.id role_id, r.name as rolename, c.id component_id, c.name componentname, isnull(rue.get, 0) get, isnull(rue.post, 0) post, isnull(rue.del, 0) del, isnull(rue.upd, 0) upd, rue.created_at, rue.updated_at
							FROM dbo.usr_elements e WITH (NOLOCK)
							INNER JOIN dbo.usr_components c WITH (NOLOCK) ON (c.id = e.component_id)
							INNER JOIN dbo.usr_role_usr_element rue WITH (NOLOCK) ON (rue.element_id = e.id)
							INNER JOIN dbo.usr_roles r WITH (NOLOCK) ON (r.id = rue.role_id)
							ORDER BY component_id, element_id`
)

func (o orcl) Create(m *Model) error {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(orclInsert)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en Create role_usr_element: %v", err)
		return err
	}
	defer stmt.Close()

	ID, err := db.ExecGettingID(
		stmt,
		&m.RoleId, &m.ElementId,
	)
	if err != nil {
		logger_trace.Error.Printf("ejecutando la consulta en Create role_usr_element: %v", err)
		return err
	}
	m.ID = ID

	return nil
}

func (o orcl) CreateOrOverwrite(m *RoleUsrElemtSmallModel) error {
	conn := db.GetConnection()

	tx, err := conn.Begin()
	if err != nil {
		logger_trace.Error.Printf("creando la transacción en role_usr_element CreateOrOverwrite: %v", err)
		return err
	}

	// Eliminar todos elementos (privilegios) de usuario de un determinado rol
	stmt, err := tx.Prepare(orclDeleteByRoleComponentID)
	if err != nil {
		tx.Rollback()
		logger_trace.Error.Printf("preparando la sentencia orclDeleteByRoleComponentID en role_usr_element CreateOrOverwrite: %v", err)
		return err
	}
	defer stmt.Close()

	err = db.Execute(
		stmt,
		sql.Named("id", m.RoleId),
		sql.Named("component_id", m.ComponentId),
	)
	if err != nil {
		tx.Rollback()
		logger_trace.Error.Printf("insertando orclDeleteByRoleComponentID: %v", err)
		return err
	}

	// Inserta el registro en role_usr_element
	stmt, err = tx.Prepare(orclInsert)
	if err != nil {
		tx.Rollback()
		logger_trace.Error.Printf("preparando la sentencia orclInsert en role_usr_element CreateOrOverwrite: %v", err)
		return err
	}
	defer stmt.Close()

	for _, num := range m.ElementId {
		ID, err := db.ExecGettingID(
			stmt,
			sql.Named("element_id", num),
			sql.Named("role_id", m.RoleId),
		)
		if err != nil {
			tx.Rollback()
			logger_trace.Error.Printf("insertando orclInsert: %v", err)
			return err
		}

		m.ID = ID
	}

	tx.Commit()
	return nil
}

func (o orcl) Update(ID int64, m *Model) error {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(orclUpdate)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en Update role_usr_element: %v", err)
		return err
	}
	defer stmt.Close()

	err = db.ExecAffectingOneRow(
		stmt,
		&m.RoleId, &m.ElementId,
		ID,
	)
	if err != nil {
		logger_trace.Error.Printf("ejecutando la consulta en Update role_usr_element: %v", err)
		return err
	}
	m.ID = ID

	return nil
}

func (o orcl) Delete(ID int64) error {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(orclDelete)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en Delete role_usr_element: %v", err)
		return err
	}
	defer stmt.Close()

	err = db.ExecAffectingOneRow(
		stmt,
		ID,
	)
	if err != nil {
		logger_trace.Error.Printf("ejecutando la consulta en Delete role_usr_element: %v", err)
		return err
	}

	return nil
}

func (o orcl) DeleteByRoleID(ID int64) error {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(orclDeleteByRoleID)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en DeleteByRoleID role_usr_element: %v", err)
		return err
	}
	defer stmt.Close()

	err = db.Execute(
		stmt,
		sql.Named("id", ID),
	)

	if err != nil {
		logger_trace.Error.Printf("ejecutando la consulta en DeleteByRoleID role_usr_element: %v", err)
		return err
	}

	return nil
}

func (o orcl) GetByID(ID int64) (*Model, error) {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(orclGetByID)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en GetByID role_usr_element: %v", err)
		return nil, err
	}
	defer stmt.Close()

	row := stmt.QueryRow(ID)
	return o.scanRow(row)
}

func (o orcl) GetByRoleID(ID int64) (Models, error) {
	conn := db.GetConnection()
	ms := make(Models, 0)

	stmt, err := conn.Prepare(orclGetByRoleID)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en GetByRoleID role_usr_element: %v", err)
		return nil, err
	}
	defer stmt.Close()

	rows, err := stmt.Query(sql.Named("id", ID))
	if err != nil {
		logger_trace.Error.Printf("ejecutando GetByRoleID: %v", err)
		return nil, err
	}

	for rows.Next() {
		m, err := o.scanRow(rows)
		if err != nil {
			logger_trace.Error.Printf("escaneando el registro en role_usr_element: %v", err)
			return ms, err
		}

		ms = append(ms, *m)
	}

	return ms, nil
}

func (o orcl) GetByComponentID(ID int64, ComponentID int64) (Models, error) {
	conn := db.GetConnection()
	ms := make(Models, 0)

	stmt, err := conn.Prepare(orclGetByComponentID)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en GetByComponentID role_usr_element: %v", err)
		return nil, err
	}
	defer stmt.Close()

	rows, err := stmt.Query(sql.Named("id", ID), sql.Named("component_id", ComponentID))
	if err != nil {
		logger_trace.Error.Printf("ejecutando GetByComponentID: %v", err)
		return nil, err
	}

	for rows.Next() {
		m, err := o.scanRow(rows)
		if err != nil {
			logger_trace.Error.Printf("escaneando el registro en role_usr_element: %v", err)
			return ms, err
		}

		ms = append(ms, *m)
	}

	return ms, nil
}

func (o orcl) GetAll() (Models, error) {
	conn := db.GetConnection()
	ms := make(Models, 0)

	stmt, err := conn.Prepare(orclGetAll)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en GetAll role_usr_element: %v", err)
		return nil, err
	}
	defer stmt.Close()

	rows, err := stmt.Query()
	if err != nil {
		logger_trace.Error.Printf("ejecutando la consulta en GetAll role_usr_element: %v", err)
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		m, err := o.scanRow(rows)
		if err != nil {
			logger_trace.Error.Printf("escaneando el registro en role_usr_element: %v", err)
			return ms, err
		}

		ms = append(ms, *m)
	}

	return ms, nil
}

func (o orcl) scanRow(rs db.RowScanner) (*Model, error) {
	m := &Model{}
	cn := pq.NullTime{}
	un := pq.NullTime{}

	err := rs.Scan(
		&m.ID,

		&m.ElementId,
		&m.ElementName,

		&m.RoleId,
		&m.RoleName,

		&m.ComponentId,
		&m.ComponentName,

		&cn,
		&un,
	)
	if err != nil {
		logger_trace.Error.Printf("escaneando el modelo role_usr_element: %v", err)
		return nil, err
	}

	m.CreatedAt = cn.Time
	m.UpdatedAt = un.Time
	return m, nil
}
