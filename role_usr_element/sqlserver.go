package role_usr_element

import (
	"database/sql"


	"github.com/lib/pq"
	"gitlab.com/e-capture/ecatch-ecm/majosystem/db"
	"gitlab.com/e-capture/ecatch-ecm/majosystem/logger_trace"
)

type sqlserver struct{}

const (
	sqlInsert                = `INSERT INTO usr.role_usr_elements (role_id, element_id) VALUES (@role_id, @element_id) SELECT ID = convert(bigint, SCOPE_IDENTITY()) `
	sqlUpdate                = `UPDATE usr.role_usr_elements SET role_id = @role_id, element_id = @element_id, updated_at = getdate() WHERE id = @id`
	sqlDelete                = `DELETE FROM usr.role_usr_elements WHERE id = @id`
	sqlDeleteByElementRoleID = `DELETE FROM usr.role_usr_elements WHERE role_id = @role_id and element_id = @element_id`
	sqlGetByID               = `SELECT isnull(rue.id, 0) id, isnull(e.id, 0) element_id, e.name elementname, r.id role_id, r.name as rolename, c.id component_id, c.name componentname, isnull(rue.get_value, 0) get, isnull(rue.post, 0) post, isnull(rue.del, 0) del, isnull(rue.upd, 0) upd, rue.created_at, rue.updated_at
						 FROM usr_elements e WITH (NOLOCK)
						 INNER JOIN usr.components c WITH (NOLOCK) ON (c.id = e.component_id)
						 INNER JOIN usr.role_usr_elements rue WITH (NOLOCK) ON (rue.element_id = e.id)
						 INNER JOIN usr.roles r WITH (NOLOCK) ON (r.id = rue.role_id)
						 WHERE rue.id = @id`
	sqlGetByRoleID = `SELECT isnull(rue.id, 0) id, isnull(e.id, 0) element_id, e.name elementname, r.id role_id, r.name as rolename, c.id component_id, c.name componentname, isnull(rue.get_value, 0) get, isnull(rue.post, 0) post, isnull(rue.del, 0) del, isnull(rue.upd, 0) upd, rue.created_at, rue.updated_at
					  FROM usr.elements e WITH (NOLOCK)
					  INNER JOIN usr.components c WITH (NOLOCK) ON (c.id = e.component_id)
					  INNER JOIN usr.roles r WITH (NOLOCK) ON (r.id = @id)
				      LEFT JOIN usr.role_usr_elements rue WITH (NOLOCK) ON (rue.role_id = r.id AND rue.element_id = e.id)
					  ORDER BY component_id, element_id`
	sqlGetByComponentID = `SELECT  distinct e.id element_id, e.name elementname, e.description element_description, CASE WHEN rue.role_id  is null then 0 ELSE 1 END privilege FROM usr.elements e  WITH (NOLOCK) LEFT JOIN usr.role_usr_elements rue WITH (NOLOCK) ON (rue.element_id = e.id and rue.role_id = @role_id) WHERE e.component_id = @component_id `

	sqlGetAll = `SELECT isnull(rue.id, 0) id, isnull(e.id, 0) element_id, e.name elementname, r.id role_id, r.name as rolename, c.id component_id, c.name componentname, isnull(rue.get_value, 0) get, isnull(rue.post, 0) post, isnull(rue.del, 0) del, isnull(rue.upd, 0) upd, rue.created_at, rue.updated_at
				 FROM usr.elements e WITH (NOLOCK)
				 INNER JOIN usr.components c WITH (NOLOCK) ON (c.id = e.component_id)
				 INNER JOIN usr.role_usr_elements rue WITH (NOLOCK) ON (rue.element_id = e.id)
				 INNER JOIN usr.roles r WITH (NOLOCK) ON (r.id = rue.role_id)
				 ORDER BY component_id, element_id`
)

func (s sqlserver) Create(m *Model) error {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(sqlInsert)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en Create role_usr_element: %v", err)
		return err
	}

	defer stmt.Close()

	id, err := db.ExecGettingID(
		stmt,

		sql.Named("role_id", m.RoleId),

		sql.Named("element_id", m.ElementId),
	)
	if err != nil {
		logger_trace.Error.Printf("ejecutando la consulta en Create role_usr_element: %v", err)
		return err
	}

	m.ID = id

	return nil
}

func (s sqlserver) Update(ID int64, m *Model) error {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(sqlUpdate)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en Update role_usr_element: %v", err)
		return err
	}
	defer stmt.Close()

	err = db.ExecAffectingOneRow(
		stmt,
		sql.Named("role_id", m.RoleId),
		sql.Named("element_id", m.ElementId),
		sql.Named("id", ID),
	)
	if err != nil {
		logger_trace.Error.Printf("ejecutando la consulta en Update role_usr_element: %v", err)
		return err
	}
	m.ID = ID

	return nil
}

func (s sqlserver) Delete(ID int64) error {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(sqlDelete)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en Delete role_usr_element: %v", err)
		return err
	}
	defer stmt.Close()

	err = db.ExecAffectingOneRow(
		stmt,
		sql.Named("id", ID),
	)
	if err != nil {
		logger_trace.Error.Printf("ejecutando la consulta en Delete role_usr_element: %v", err)
		return err
	}

	return nil
}

func (s sqlserver) DeleteByElementRoleID(roleID, elementID int64) error {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(sqlDeleteByElementRoleID)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en DeleteByElementRoleID role_usr_element: %v", err)
		return err
	}
	defer stmt.Close()

	err = db.ExecAffectingOneRow(
		stmt,
		sql.Named("role_id", roleID),
		sql.Named("element_id", elementID),
	)

	if err != nil {
		logger_trace.Error.Printf("ejecutando la consulta en DeleteByElementRoleID role_usr_element: %v", err)
		return err
	}

	return nil
}

func (s sqlserver) GetByID(ID int64) (*Model, error) {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(sqlGetByID)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en GetByID usr_component: %v", err)
		return nil, err
	}
	defer stmt.Close()

	row := stmt.QueryRow(sql.Named("id", ID))
	return s.scanRow(row)
}

func (s sqlserver) GetByRoleID(ID int64) (Models, error) {
	conn := db.GetConnection()
	ms := make(Models, 0)

	stmt, err := conn.Prepare(sqlGetByRoleID)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en GetByRoleID usr_component: %v", err)
		return nil, err
	}
	defer stmt.Close()

	rows, err := stmt.Query(sql.Named("id", ID))
	if err != nil {
		logger_trace.Error.Printf("ejecutando GetByRoleID: %v", err)
		return nil, err
	}

	for rows.Next() {
		m, err := s.scanRow(rows)
		if err != nil {
			logger_trace.Error.Printf("escaneando el registro en usr_component: %v", err)
			return ms, err
		}

		ms = append(ms, *m)
	}

	return ms, nil
}

func (s sqlserver) GetByComponentID(roleID, componentID int64) (RoleElements, error) {
	conn := db.GetConnection()
	ms := make(RoleElements, 0)

	stmt, err := conn.Prepare(sqlGetByComponentID)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en GetByComponentID usr_component: %v", err)
		return nil, err
	}
	defer stmt.Close()

	rows, err := stmt.Query(sql.Named("role_id", roleID), sql.Named("component_id", componentID))
	if err != nil {
		logger_trace.Error.Printf("ejecutando GetByComponentID: %v", err)
		return nil, err
	}

	for rows.Next() {

		m := &RoleElement{}

		err := rows.Scan(
			&m.ElementId,
			&m.ElementName,
			&m.ElementDescription,
			&m.Privilege,
		)
		if err != nil {
			logger_trace.Error.Printf("escaneando el modelo role_usr_element: %v", err)
			return nil, err
		}

		ms = append(ms, *m)
	}

	return ms, nil
}

func (s sqlserver) GetAll() (Models, error) {
	conn := db.GetConnection()
	ms := make(Models, 0)

	stmt, err := conn.Prepare(sqlGetAll)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en GetAll role_usr_element: %v", err)
		return nil, err
	}
	defer stmt.Close()

	rows, err := stmt.Query()
	if err != nil {
		logger_trace.Error.Printf("ejecutando la consulta en GetAll role_usr_element: %v", err)
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		m, err := s.scanRow(rows)
		if err != nil {
			logger_trace.Error.Printf("escaneando el registro en role_usr_element: %v", err)
			return ms, err
		}

		ms = append(ms, *m)
	}

	return ms, nil
}

func (s sqlserver) scanRow(rs db.RowScanner) (*Model, error) {
	m := &Model{}
	cn := pq.NullTime{}
	un := pq.NullTime{}

	err := rs.Scan(
		&m.ID,
		&m.ElementId,
		&m.ElementName,
		&m.RoleId,
		&m.RoleName,
		&m.ComponentId,
		&m.ComponentName,
		&m.Get,
		&m.Post,
		&m.Del,
		&m.Upd,
		&cn,
		&un,
	)
	if err != nil {
		logger_trace.Error.Printf("escaneando el modelo role_usr_element: %v", err)
		return nil, err
	}

	m.CreatedAt = cn.Time
	m.UpdatedAt = un.Time
	return m, nil
}
