package role_usr_element

import "time"

type Model struct {
	ID                 int64     `json:"id"`
	ElementId          int       `json:"element_id"`
	ElementName        string    `json:"elementname"`
	ElementDescription string    `json:"elemen_description`
	RoleId             int64     `json:"role_id"`
	RoleName           string    `json:"rolename"`
	ComponentId        int64     `json:"component_id"`
	ComponentName      string    `json:"componentname"`
	Get                bool      `json:"get"`
	Post               bool      `json:"post"`
	Del                bool      `json:"del"`
	Upd                bool      `json:"upd"`
	CreatedAt          time.Time `json:"created_at"`
	UpdatedAt          time.Time `json:"updated_at"`
}

type RoleElement struct {
	ElementId          int    `json:"element_id"`
	ElementName        string `json:"elementname"`
	ElementDescription string `json:"element_description"`
	Privilege          int    `json:"privilege"`
}

type RoleElements []RoleElement

type RoleUsrElemtSmallModel struct {
	ID          int64   `json:"id"`
	ElementId   []int64 `json:"element_id"`
	RoleId      int64   `json:"role_id"`
	ComponentId int64   `json:"component_id"`
	Get         []bool  `json:"get"`
	Post        []bool  `json:"post"`
	Del         []bool  `json:"del"`
	Upd         []bool  `json:"upd"`
}

type Models []Model

func (m *Model) Create() error {
	return s.Create(m)
}

func (m *Model) Update(ID int64) error {
	return s.Update(ID, m)
}

func (m *Model) Delete(ID int64) error {
	return s.Delete(ID)
}

func (m *Model) DeleteByElementRoleID(roleID, elementID int64) error {
	return s.DeleteByElementRoleID(roleID, elementID)
}

func (m *Model) GetByID(ID int64) (*Model, error) {
	return s.GetByID(ID)
}

func (m *Model) GetByRoleID(ID int64) (Models, error) {
	return s.GetByRoleID(ID)
}

func (m *Model) GetByComponentID(roleID, componentID int64) (RoleElements, error) {
	return s.GetByComponentID(roleID, componentID)
}

func (m *Model) GetAll() (Models, error) {
	return s.GetAll()
}
