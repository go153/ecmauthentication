package role_usr_element

import (
	"database/sql"

	"github.com/lib/pq"
	"gitlab.com/e-capture/ecatch-ecm/majosystem/db"
	"gitlab.com/e-capture/ecatch-ecm/majosystem/logger_trace"
)

type pgsql struct{}

const (
	pgsqlInsert                  = `INSERT INTO usr_role_usr_element (role_id, element_id, get, post, del, upd) VALUES ($1, $2, $3, $4, $5, $6)`
	pgsqlUpdate                  = `UPDATE usr_role_usr_element SET role_id = $1, element_id = $2, get = $3, post = $4, del = $5, upd = $6, updated_at = getdate() WHERE id = $7`
	pgsqlDelete                  = `DELETE FROM usr_role_usr_element WHERE id = $1`
	pgsqlDeleteByRoleID          = `DELETE FROM usr_role_usr_element WHERE role_id = $1`
	pgsqlDeleteByRoleComponentID = `DELETE FROM dbo.usr_role_usr_element WHERE id in (
									SELECT rue.id
									FROM dbo.usr_elements e WITH (NOLOCK)
									INNER JOIN dbo.usr_components c WITH (NOLOCK) ON (c.id = e.component_id AND e.component_id = $2)
									INNER JOIN dbo.usr_roles r WITH (NOLOCK) ON (r.id = $1)
									INNER JOIN dbo.usr_role_usr_element rue WITH (NOLOCK) ON (rue.role_id = r.id AND rue.element_id = e.id)
								)`
	pgsqlGetByID = `SELECT isnull(rue.id, 0) id, isnull(e.id, 0) element_id, (case when e.name = 'load' then c.name else e.name end) elementname, r.id role_id, r.name as rolename, c.id component_id, c.name componentname, isnull(rue.get, 0) get, isnull(rue.post, 0) post, isnull(rue.del, 0) del, isnull(rue.upd, 0) upd, rue.created_at, rue.updated_at
						   FROM dbo.usr_elements e WITH (NOLOCK)
						   INNER JOIN dbo.usr_components c WITH (NOLOCK) ON (c.id = e.component_id)
						   INNER JOIN dbo.usr_role_usr_element rue WITH (NOLOCK) ON (rue.element_id = e.id)
						   INNER JOIN dbo.usr_roles r WITH (NOLOCK) ON (r.id = rue.role_id)
						   WHERE rue.id = $1`
	pgsqlGetByRoleID = `SELECT isnull(rue.id, 0) id, isnull(e.id, 0) element_id, e.name elementname, r.id role_id, r.name as rolename, c.id component_id, c.name componentname, isnull(rue.get, 0) get, isnull(rue.post, 0) post, isnull(rue.del, 0) del, isnull(rue.upd, 0) upd, rue.created_at, rue.updated_at
						   FROM dbo.usr_elements e WITH (NOLOCK)
						   INNER JOIN dbo.usr_components c WITH (NOLOCK) ON (c.id = e.component_id)
						   INNER JOIN dbo.usr_roles r WITH (NOLOCK) ON (r.id = $1)
						   LEFT JOIN dbo.usr_role_usr_element rue WITH (NOLOCK) ON (rue.role_id = r.id AND rue.element_id = e.id)
						   ORDER BY component_id, element_id`
	pgsqlGetByComponentID = `SELECT isnull(rue.id, 0) id, isnull(e.id, 0) element_id, e.name elementname, r.id role_id, r.name as rolename, c.id component_id, c.name componentname, isnull(rue.get, 0) get, isnull(rue.post, 0) post, isnull(rue.del, 0) del, isnull(rue.upd, 0) upd, rue.created_at, rue.updated_at
						   FROM dbo.usr_elements e WITH (NOLOCK)
						   INNER JOIN dbo.usr_components c WITH (NOLOCK) ON (c.id = e.component_id AND e.component_id = $2)
						   INNER JOIN dbo.usr_roles r WITH (NOLOCK) ON (r.id = $1)
						   LEFT JOIN dbo.usr_role_usr_element rue WITH (NOLOCK) ON (rue.role_id = r.id AND rue.element_id = e.id)
						   ORDER BY component_id, element_id`
	pgsqlGetAll = `SELECT isnull(rue.id, 0) id, isnull(e.id, 0) element_id, e.name elementname, r.id role_id, r.name as rolename, c.id component_id, c.name componentname, isnull(rue.get, 0) get, isnull(rue.post, 0) post, isnull(rue.del, 0) del, isnull(rue.upd, 0) upd, rue.created_at, rue.updated_at
						   FROM dbo.usr_elements e WITH (NOLOCK)
						   INNER JOIN dbo.usr_components c WITH (NOLOCK) ON (c.id = e.component_id)
						   INNER JOIN dbo.usr_role_usr_element rue WITH (NOLOCK) ON (rue.element_id = e.id)
						   INNER JOIN dbo.usr_roles r WITH (NOLOCK) ON (r.id = rue.role_id)
						   ORDER BY component_id, element_id`
)

func (p pgsql) Create(m *Model) error {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(pgsqlInsert)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en Create role_usr_element: %v", err)
		return err
	}
	defer stmt.Close()

	ID, err := db.ExecGettingID(
		stmt,
		&m.RoleId, &m.ElementId,
	)
	if err != nil {
		logger_trace.Error.Printf("ejecutando la consulta en Create role_usr_element: %v", err)
		return err
	}
	m.ID = ID

	return nil
}

func (p pgsql) CreateOrOverwrite(m *RoleUsrElemtSmallModel) error {
	conn := db.GetConnection()

	tx, err := conn.Begin()
	if err != nil {
		logger_trace.Error.Printf("creando la transacción en role_usr_element CreateOrOverwrite: %v", err)
		return err
	}

	// Eliminar todos elementos (privilegios) de usuario de un determinado rol
	stmt, err := tx.Prepare(pgsqlDeleteByRoleComponentID)
	if err != nil {
		tx.Rollback()
		logger_trace.Error.Printf("preparando la sentencia pgsqlDeleteByRoleComponentID en role_usr_element CreateOrOverwrite: %v", err)
		return err
	}
	defer stmt.Close()

	err = db.Execute(
		stmt,
		sql.Named("id", m.RoleId),
		sql.Named("component_id", m.ComponentId),
	)
	if err != nil {
		tx.Rollback()
		logger_trace.Error.Printf("insertando pgsqlDeleteByRoleComponentID: %v", err)
		return err
	}

	// Inserta el registro en role_usr_element
	stmt, err = tx.Prepare(pgsqlInsert)
	if err != nil {
		tx.Rollback()
		logger_trace.Error.Printf("preparando la sentencia pgsqlInsert en role_usr_element CreateOrOverwrite: %v", err)
		return err
	}
	defer stmt.Close()

	for _, num := range m.ElementId {
		ID, err := db.ExecGettingID(
			stmt,
			sql.Named("element_id", num),
			sql.Named("role_id", m.RoleId),
		)
		if err != nil {
			tx.Rollback()
			logger_trace.Error.Printf("insertando pgsqlInsert: %v", err)
			return err
		}

		m.ID = ID
	}

	tx.Commit()
	return nil
}

func (p pgsql) Update(ID int64, m *Model) error {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(pgsqlUpdate)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en Update role_usr_element: %v", err)
		return err
	}
	defer stmt.Close()

	err = db.ExecAffectingOneRow(
		stmt,
		&m.RoleId, &m.ElementId,
		ID,
	)
	if err != nil {
		logger_trace.Error.Printf("ejecutando la consulta en Update role_usr_element: %v", err)
		return err
	}
	m.ID = ID

	return nil
}

func (p pgsql) Delete(ID int64) error {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(pgsqlDelete)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en Delete role_usr_element: %v", err)
		return err
	}
	defer stmt.Close()

	err = db.ExecAffectingOneRow(
		stmt,
		ID,
	)
	if err != nil {
		logger_trace.Error.Printf("ejecutando la consulta en Delete role_usr_element: %v", err)
		return err
	}

	return nil
}

func (p pgsql) DeleteByRoleID(ID int64) error {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(pgsqlDeleteByRoleID)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en DeleteByRoleID role_usr_element: %v", err)
		return err
	}
	defer stmt.Close()

	err = db.Execute(
		stmt,
		sql.Named("id", ID),
	)

	if err != nil {
		logger_trace.Error.Printf("ejecutando la consulta en DeleteByRoleID role_usr_element: %v", err)
		return err
	}

	return nil
}

func (p pgsql) GetByID(ID int64) (*Model, error) {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(pgsqlGetByID)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en GetByID role_usr_element: %v", err)
		return nil, err
	}
	defer stmt.Close()

	row := stmt.QueryRow(ID)
	return p.scanRow(row)
}

func (p pgsql) GetByRoleID(ID int64) (Models, error) {
	conn := db.GetConnection()
	ms := make(Models, 0)

	stmt, err := conn.Prepare(pgsqlGetByRoleID)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en GetByRoleID role_usr_element: %v", err)
		return nil, err
	}
	defer stmt.Close()

	rows, err := stmt.Query(sql.Named("id", ID))
	if err != nil {
		logger_trace.Error.Printf("ejecutando GetByRoleID: %v", err)
		return nil, err
	}

	for rows.Next() {
		m, err := p.scanRow(rows)
		if err != nil {
			logger_trace.Error.Printf("escaneando el registro en role_usr_element: %v", err)
			return ms, err
		}

		ms = append(ms, *m)
	}

	return ms, nil
}

func (p pgsql) GetByComponentID(ID int64, ComponentID int64) (Models, error) {
	conn := db.GetConnection()
	ms := make(Models, 0)

	stmt, err := conn.Prepare(pgsqlGetByComponentID)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en GetByComponentID usr_compoenent: %v", err)
		return nil, err
	}
	defer stmt.Close()

	rows, err := stmt.Query(sql.Named("id", ID), sql.Named("component_id", ComponentID))
	if err != nil {
		logger_trace.Error.Printf("ejecutando GetByComponentID: %v", err)
		return nil, err
	}

	for rows.Next() {
		m, err := p.scanRow(rows)
		if err != nil {
			logger_trace.Error.Printf("escaneando el registro en usr_component: %v", err)
			return ms, err
		}

		ms = append(ms, *m)
	}

	return ms, nil
}

func (p pgsql) GetAll() (Models, error) {
	conn := db.GetConnection()
	ms := make(Models, 0)

	stmt, err := conn.Prepare(pgsqlGetAll)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en GetAll role_usr_element: %v", err)
		return nil, err
	}
	defer stmt.Close()

	rows, err := stmt.Query()
	if err != nil {
		logger_trace.Error.Printf("ejecutando la consulta en GetAll role_usr_element: %v", err)
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		m, err := p.scanRow(rows)
		if err != nil {
			logger_trace.Error.Printf("escaneando el registro en role_usr_element: %v", err)
			return ms, err
		}

		ms = append(ms, *m)
	}

	return ms, nil
}

func (p pgsql) scanRow(rs db.RowScanner) (*Model, error) {
	m := &Model{}
	cn := pq.NullTime{}
	un := pq.NullTime{}

	err := rs.Scan(
		&m.ID,

		&m.ElementId,
		&m.ElementName,

		&m.RoleId,
		&m.RoleName,

		&m.ComponentId,
		&m.ComponentName,

		&cn,
		&un,
	)
	if err != nil {
		logger_trace.Error.Printf("escaneando el modelo role_usr_element: %v", err)
		return nil, err
	}

	m.CreatedAt = cn.Time
	m.UpdatedAt = un.Time
	return m, nil
}
