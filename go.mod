module gitlab.com/e-capture/ecatch-ecm/ecmauthentication

go 1.13

replace gitlab.com/e-capture/ecatch-ecm/majosystem => ../majosystem/v2

require (
	gitlab.com/e-capture/ecatch-ecm/majosystem v0.0.0 //
	github.com/denisenkom/go-mssqldb v0.0.0-20200206145737-bbfc9a55622e // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/estebanbacl/go-openssl v2.0.0+incompatible
	github.com/go-sql-driver/mysql v1.5.0 // indirect
	github.com/golang/protobuf v1.3.3
	github.com/joho/godotenv v1.3.0 // indirect
	github.com/labstack/echo v3.3.10+incompatible
	github.com/labstack/gommon v0.3.0 // indirect
	github.com/lib/pq v1.3.0
	github.com/satori/go.uuid v1.2.0
	github.com/valyala/fasttemplate v1.1.0 // indirect
	golang.org/x/crypto v0.0.0-20200214034016-1d94cc7ab1c6
	google.golang.org/grpc v1.27.1
	gopkg.in/alexcesaro/quotedprintable.v3 v3.0.0-20150716171945-2caba252f4dc // indirect
	gopkg.in/asn1-ber.v1 v1.0.0-20181015200546-f715ec2f112d // indirect
	gopkg.in/gomail.v2 v2.0.0-20160411212932-81ebce5c23df // indirect
	gopkg.in/ldap.v2 v2.5.1
)
