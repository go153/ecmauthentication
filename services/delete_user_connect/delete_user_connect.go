package delete_user_connect

import (
	"time"

	"gitlab.com/e-capture/ecatch-ecm/ecmauthentication/loggeduser"
	"gitlab.com/e-capture/ecatch-ecm/majosystem/logger_trace"
)

func DeleteUserConnect() {

	var iut int
	iut = 5
	ticker := time.NewTicker(time.Duration(iut) * time.Hour)
	go helperDeleteUserConnect(ticker)
}

func helperDeleteUserConnect(ticker *time.Ticker) {
	mdl := loggeduser.Model{}
	for range ticker.C {
		currentTime := time.Now()
		if currentTime.Hour() > 22 {
			err := mdl.DeleteAll()
			if err != nil {
				logger_trace.Error.Printf("ENo se pudo eliminar los usuarios conectados: %v", err)

			}
		}

	}
}
