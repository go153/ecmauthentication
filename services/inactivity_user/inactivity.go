package inactivity_user

import (
	"strconv"
	"time"

	"gitlab.com/e-capture/ecatch-ecm/ecmauthentication/password_policy"
	"gitlab.com/e-capture/ecatch-ecm/ecmauthentication/user"
	"gitlab.com/e-capture/ecatch-ecm/majosystem/configuration"
	"gitlab.com/e-capture/ecatch-ecm/majosystem/logger_trace"
)

func LockUserByInactivityTime() {
	logger_trace.Trace.Printf("inicio LockUserByInactivityTime")
	cnfg := configuration.FromFile()
	var iut int

	iut, err := strconv.Atoi(cnfg.InactivityUserTime)
	if err != nil {
		logger_trace.Error.Printf("El valor de InactivityUserTime no es válido: %v", err)
		iut = 24
	}

	ticker := time.NewTicker(time.Duration(iut) * time.Hour)
	go helperLockUserByInactivityTime(ticker)
}

func helperLockUserByInactivityTime(ticker *time.Ticker) {
	u := user.Model{}
	for range ticker.C {
		us, err := u.GetAllEnabled()
		if err != nil {
			logger_trace.Error.Printf("no se pudo consultar los usuarios habilitados (servicio de inactivar usuarios): %v", err)
			continue
		}
		pp := password_policy.Model{}
		pps, err := pp.GetAll()
		// tim es un map del tiempo de inactividad x role
		tim := make(map[int64]int, 0)
		for _, p := range pps {
			// Si la política está habilitada, se guarda en el map
			if p.Enable {
				tim[p.RoleId] = p.InactivityTime
			}
		}

		for _, ut := range us {
			disableUser(tim, ut)
		}
	}
}

func disableUser(tim map[int64]int, ut user.Model) {
	ti := tim[int64(ut.RoleID)]
	if ti > 0 {
		if time.Now().After(ut.LastLogin.Add(time.Duration(ti) * time.Hour)) {
			err := ut.Disable()
			if err != nil {
				logger_trace.Error.Printf("no se pudo deshabilitar el usuario: %d, %v", ut.ID, err)
			}
		}
	}
}
