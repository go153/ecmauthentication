package administration

import (
	"net/http"

	"github.com/labstack/echo"
	"gitlab.com/e-capture/ecatch-ecm/ecmauthentication/user"
	"gitlab.com/e-capture/ecatch-ecm/majosystem/logger_trace"
	"gitlab.com/e-capture/ecatch-ecm/majosystem/response"
)

// UserLockUnlock permite bloquear temporalmente o desbloquear un usuario
func UserLockUnlock(c echo.Context) error {
	type request struct {
		UserID int64 `json:"user_id"`
		Lock   bool  `json:"lock"`
	}
	rm := response.Model{}
	r := &request{}
	u := user.Model{}

	err := c.Bind(r)
	if err != nil {
		logger_trace.Error.Printf("la estructura del objeto no es válida: %v", err)

		rm.Set(true, nil, 1)
		return c.JSON(http.StatusAccepted, rm)
	}

	u.ID = r.UserID
	if r.Lock {
		err = u.Block()
		if err != nil {
			logger_trace.Error.Printf("no fue posible bloquear el usuario: %d, %v", u.ID, err)
			rm.Set(true, nil, 73)
			return c.JSON(http.StatusAccepted, rm)
		}
	} else {
		err = u.Unblock()
		if err != nil {
			logger_trace.Error.Printf("no fue posible desbloquear el usuario: %d, %v", u.ID, err)
			rm.Set(true, nil, 73)
			return c.JSON(http.StatusAccepted, rm)
		}
	}

	rm.Set(false, nil, 29)
	return c.JSON(http.StatusOK, rm)
}

// UserDisableEnable deshabilita de forma definitiva o habilita un usuario
func UserDisableEnable(c echo.Context) error {
	type request struct {
		UserID  int64 `json:"user_id"`
		Disable bool  `json:"disable"`
	}
	rm := response.Model{}
	r := &request{}
	u := user.Model{}

	err := c.Bind(r)
	if err != nil {
		logger_trace.Error.Printf("la estructura del objeto no es válida: %v", err)
		rm.Set(true, nil, 1)
		return c.JSON(http.StatusAccepted, rm)
	}

	u.ID = r.UserID
	if r.Disable {
		err = u.Disable()
		if err != nil {
			logger_trace.Error.Printf("no fue posible deshabilitar el usuario: %d, %v", u.ID, err)
			rm.Set(true, nil, 73)
			return c.JSON(http.StatusAccepted, rm)
		}
	} else {
		err = u.Enable()
		if err != nil {
			logger_trace.Error.Printf("no fue posible habilitar el usuario: %d, %v", u.ID, err)
			rm.Set(true, nil, 73)
			return c.JSON(http.StatusAccepted, rm)
		}
	}

	rm.Set(false, nil, 29)
	return c.JSON(http.StatusOK, rm)
}
