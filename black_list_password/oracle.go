package black_list_password

import (
	"github.com/lib/pq"
	"gitlab.com/e-capture/ecatch-ecm/majosystem/db"
	"gitlab.com/e-capture/ecatch-ecm/majosystem/logger_trace"
)

type orcl struct{}

const (
	orclInsert  = `INSERT INTO usr.black_list_passwords (passwd ) VALUES (:0 )`
	orclUpdate  = `UPDATE usr.black_list_passwords SET passwd = :0,  updated_at = getdate() WHERE id = :1`
	orclDelete  = `DELETE FROM usr.black_list_passwords WHERE id = :1`
	orclGetByID = `SELECT id, passwd,  created_at, updated_at FROM usr.black_list_passwords WITH (NOLOCK) WHERE id = :1`
	orclGetAll  = `SELECT id, passwd,  created_at, updated_at FROM usr.black_list_passwords WITH (NOLOCK)`
)

func (o orcl) Create(m *Model) error {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(orclInsert)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en Create lack_list_password: %v", err)
		return err
	}
	defer stmt.Close()

	ID, err := db.ExecGettingID(
		stmt,
		&m.Passwd,
	)
	if err != nil {
		logger_trace.Error.Printf("ejecutando la consulta en Create lack_list_password: %v", err)
		return err
	}
	m.ID = ID

	return nil
}

func (o orcl) Update(ID int64, m *Model) error {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(orclUpdate)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en Update lack_list_password: %v", err)
		return err
	}
	defer stmt.Close()

	err = db.ExecAffectingOneRow(
		stmt,
		&m.Passwd,
		ID,
	)
	if err != nil {
		logger_trace.Error.Printf("ejecutando la consulta en Update lack_list_password: %v", err)
		return err
	}
	m.ID = ID

	return nil
}

func (o orcl) Delete(ID int64) error {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(orclDelete)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en Delete lack_list_password: %v", err)
		return err
	}
	defer stmt.Close()

	err = db.ExecAffectingOneRow(
		stmt,
		ID,
	)
	if err != nil {
		logger_trace.Error.Printf("ejecutando la consulta en Delete lack_list_password: %v", err)
		return err
	}

	return nil
}

func (o orcl) GetByID(ID int64) (*Model, error) {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(orclGetByID)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en GetByID lack_list_password: %v", err)
		return nil, err
	}
	defer stmt.Close()

	row := stmt.QueryRow(ID)
	return o.scanRow(row)
}

func (o orcl) GetAll() (Models, error) {
	conn := db.GetConnection()
	ms := make(Models, 0)

	stmt, err := conn.Prepare(orclGetAll)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en GetAll lack_list_password: %v", err)
		return nil, err
	}
	defer stmt.Close()

	rows, err := stmt.Query()
	if err != nil {
		logger_trace.Error.Printf("ejecutando la consulta en GetAll lack_list_password: %v", err)
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		m, err := o.scanRow(rows)
		if err != nil {
			logger_trace.Error.Printf("escaneando el registro en lack_list_password: %v", err)
			return ms, err
		}

		ms = append(ms, *m)
	}

	return ms, nil
}

func (o orcl) scanRow(rs db.RowScanner) (*Model, error) {
	m := &Model{}
	cn := pq.NullTime{}
	un := pq.NullTime{}

	err := rs.Scan(
		&m.ID,

		&m.Passwd,

		&cn,
		&un,
	)
	if err != nil {
		logger_trace.Error.Printf("escaneando el modelo lack_list_password: %v", err)
		return nil, err
	}

	m.CreatedAt = cn.Time
	m.UpdatedAt = un.Time
	return m, nil
}
