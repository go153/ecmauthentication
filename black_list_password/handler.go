package black_list_password

import (
	"fmt"
	"net/http"
	"strconv"

	"github.com/labstack/echo"

	// Esta importación se debe modificar a los paquetes del proyecto
	"io"
	"mime/multipart"
	"os"
	"path"

	"time"

	"encoding/csv"

	"gitlab.com/e-capture/ecatch-ecm/majosystem/helper"
	"gitlab.com/e-capture/ecatch-ecm/majosystem/logger_trace"
	"gitlab.com/e-capture/ecatch-ecm/majosystem/response"
)

func Create(c echo.Context) error {
	rm := response.Model{}
	m := Model{}

	err := c.Bind(&m)
	if err != nil {
		errStr := fmt.Sprintf("estructura no válida: %v", err)
		logger_trace.Error.Print(errStr)
		rm.Set(true, nil, 1)
		return c.JSON(http.StatusAccepted, rm)
	}

	err = m.Create()
	if err != nil {
		errStr := fmt.Sprintf("no se pudo insertar el registro: %v", err)
		logger_trace.Error.Print(errStr)
		rm.Set(true, nil, 3)
		return c.JSON(http.StatusAccepted, rm)
	}
	rm.Set(false, m, 4)
	return c.JSON(http.StatusCreated, rm)
}

func Update(c echo.Context) error {
	rm := response.Model{}
	m := Model{}

	ID, err := strconv.ParseInt(c.Param("id"), 10, 64)
	if err != nil {
		errStr := "el id debe ser un número entero positivo"
		logger_trace.Error.Print(errStr)
		rm.Set(true, nil, 17)
		return c.JSON(http.StatusAccepted, rm)
	}

	err = c.Bind(&m)
	if err != nil {
		errStr := fmt.Sprintf("estructura no válida: %v", err)
		logger_trace.Error.Print(errStr)
		rm.Set(true, nil, 1)
		return c.JSON(http.StatusAccepted, rm)
	}
	m.ID = ID

	err = m.Update(m.ID)
	if err != nil {
		errStr := fmt.Sprintf("no se pudo actualizar el registro: %v", err)
		logger_trace.Error.Print(errStr)
		rm.Set(true, nil, 18)
		return c.JSON(http.StatusAccepted, rm)
	}

	rm.Set(false, nil, 19)
	return c.JSON(http.StatusOK, rm)
}

func Delete(c echo.Context) error {
	rm := response.Model{}
	m := Model{}

	ID, err := strconv.ParseInt(c.Param("id"), 10, 64)
	if err != nil {
		errStr := "el id debe ser un número entero positivo"
		logger_trace.Error.Print(errStr)
		rm.Set(true, nil, 17)
		return c.JSON(http.StatusAccepted, rm)
	}

	err = m.Delete(ID)
	if err != nil {
		errStr := fmt.Sprintf("no se eliminó el registro: %v", err)
		logger_trace.Error.Print(errStr)
		rm.Set(true, nil, 20)
		return c.JSON(http.StatusAccepted, rm)
	}
	rm.Set(false, nil, 28)
	return c.JSON(http.StatusOK, rm)
}

func GetByID(c echo.Context) error {
	rm := response.Model{}
	m := Model{}

	ID, err := strconv.ParseInt(c.Param("id"), 10, 64)
	if err != nil {
		errStr := "el id debe ser un número entero positivo"
		logger_trace.Error.Print(errStr)
		rm.Set(true, nil, 17)
		return c.JSON(http.StatusAccepted, rm)
	}

	r, err := m.GetByID(ID)
	if err != nil {
		errStr := fmt.Sprintf("no se pudo consultar el registro: %v", err)
		logger_trace.Error.Print(errStr)
		rm.Set(true, nil, 22)
		return c.JSON(http.StatusAccepted, rm)
	}

	rm.Set(false, r, 29)
	return c.JSON(http.StatusOK, rm)
}

func GetAll(c echo.Context) error {
	rm := response.Model{}
	m := Model{}

	rs, err := m.GetAll()
	if err != nil {
		errStr := fmt.Sprintf("no se pudo consultar el registro: %v", err)
		logger_trace.Error.Print(errStr)
		rm.Set(true, nil, 22)
		return c.JSON(http.StatusAccepted, rm)
	}

	rm.Set(false, rs, 29)
	return c.JSON(http.StatusOK, rm)
}

// BulkLoad carga un archivo plano para registrar
// masivamente passwords no válidos
func BulkLoad(c echo.Context) error {
	rm := response.Model{}

	file, err := c.FormFile("file")
	if err != nil {
		logger_trace.Error.Printf("leyendo el archivo enviado desde el cliente: %v", err)
		rm.Set(true, nil, 43)
		return c.JSON(http.StatusAccepted, rm)
	}

	fn := file.Filename + time.Now().String()
	err = uploadFile(file, "./password_files", fn)
	if err != nil {
		logger_trace.Error.Printf("no se pudo crear el archivo: %s, %v", file.Filename, err)
		rm.Set(true, nil, 45)
		return c.JSON(http.StatusAccepted, rm)
	}

	err = bulkFile("./password_files/" + fn)
	if err != nil {
		logger_trace.Error.Printf("no se pudo registrar los password en la BD")
		rm.Set(true, nil, 71)
		return c.JSON(http.StatusAccepted, rm)
	}

	rm.Set(false, "procesado correctamente", 29)
	return c.JSON(http.StatusCreated, rm)
}

func uploadFile(file *multipart.FileHeader, fpath, filename string) error {
	if _, err := os.Stat(fpath); os.IsNotExist(err) {
		if err = os.MkdirAll(fpath, 0777); err != nil {
			logger_trace.Error.Printf("no se pudo crear la ruta %s: %v", fpath, err)
			return err
		}
	}

	src, err := file.Open()
	if err != nil {
		logger_trace.Error.Printf("abriendo el archivo a cargar: %v", err)
		return err
	}
	defer src.Close()

	out, err := os.Create(path.Join(fpath, filename))
	if err != nil {
		logger_trace.Error.Printf("creando el archivo destino: %v", err)
		return err
	}
	defer out.Close()

	_, err = io.Copy(out, src)
	if err != nil {
		logger_trace.Error.Printf("creando el archivo: %v", err)
		return err
	}

	return nil
}

// bulkFile procesa el archivo cargado
// registrando los password
func bulkFile(fullPath string) error {
	var i int
	blp := Model{}
	file, err := os.Open(fullPath)
	if err != nil {
		logger_trace.Error.Printf("error al tratar de leer el archivo %s para registrar las contraseñas: %v", fullPath, err)
		return err
	}
	defer file.Close()

	r := csv.NewReader(file)
	r.Comment = '#'
	for {
		i++
		row, err := r.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			logger_trace.Error.Printf("error leyendo la fila %d: %v", i, err)
			continue
		}

		blp.Passwd = row[0]
		err = blp.Create()

		// Si el error es violación de Unique Key o Primary Key, continua con el proceso
		if errorWithNumber, ok := err.(helper.ErrorWithNumber); ok {
			if errorWithNumber.SQLErrorNumber() == 2627 || errorWithNumber.SQLErrorNumber() == 2601 {
				continue
			}
		}
		if err != nil {
			logger_trace.Error.Printf("no se pudo registrar el password: %s, %v", blp.Passwd, err)
		}
	}

	return nil
}
