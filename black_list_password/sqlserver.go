package black_list_password

import (
	"database/sql"

	"github.com/lib/pq"
	"gitlab.com/e-capture/ecatch-ecm/majosystem/db"
	"gitlab.com/e-capture/ecatch-ecm/majosystem/logger_trace"
)

type sqlserver struct{}

const (
	sqlInsert        = `INSERT INTO usr.black_list_passwords (passwd) VALUES (@passwd) SELECT ID = convert(bigint, SCOPE_IDENTITY()) `
	sqlUpdate        = `UPDATE usr.black_list_passwords SET passwd = @passwd, updated_at = getdate() WHERE id = @id`
	sqlDelete        = `DELETE FROM usr.black_list_passwords WHERE id = @id`
	sqlGetByID       = `SELECT id, passwd, created_at, updated_at FROM usr.black_list_passwords WITH (NOLOCK) WHERE id = @id`
	sqlGetAll        = `SELECT id, passwd, created_at, updated_at FROM usr.black_list_passwords WITH (NOLOCK)`
	sqlGetByPassword = sqlGetAll + " WHERE passwd = @passwd"
)

func (s sqlserver) Create(m *Model) error {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(sqlInsert)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en Create lack_list_password: %v", err)
		return err
	}
	defer stmt.Close()

	ID, err := db.ExecGettingID(
		stmt,
		sql.Named("passwd", m.Passwd),
	)
	if err != nil {
		logger_trace.Error.Printf("ejecutando la consulta en Create lack_list_password: %v", err)
		return err
	}
	m.ID = ID

	return nil
}

func (s sqlserver) Update(ID int64, m *Model) error {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(sqlUpdate)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en Update lack_list_password: %v", err)
		return err
	}
	defer stmt.Close()

	err = db.ExecAffectingOneRow(
		stmt,
		sql.Named("passwd", m.Passwd),
		sql.Named("id", ID),
	)
	if err != nil {
		logger_trace.Error.Printf("ejecutando la consulta en Update lack_list_password: %v", err)
		return err
	}
	m.ID = ID

	return nil
}

func (s sqlserver) Delete(ID int64) error {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(sqlDelete)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en Delete lack_list_password: %v", err)
		return err
	}
	defer stmt.Close()

	err = db.ExecAffectingOneRow(
		stmt,
		sql.Named("id", ID),
	)
	if err != nil {
		logger_trace.Error.Printf("ejecutando la consulta en Delete lack_list_password: %v", err)
		return err
	}

	return nil
}

func (s sqlserver) GetByID(ID int64) (*Model, error) {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(sqlGetByID)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en GetByID lack_list_password: %v", err)
		return nil, err
	}
	defer stmt.Close()

	row := stmt.QueryRow(sql.Named("id", ID))
	return s.scanRow(row)
}

func (s sqlserver) GetAll() (Models, error) {
	conn := db.GetConnection()
	ms := make(Models, 0)

	stmt, err := conn.Prepare(sqlGetAll)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en GetAll lack_list_password: %v", err)
		return nil, err
	}
	defer stmt.Close()

	rows, err := stmt.Query()
	if err != nil {
		logger_trace.Error.Printf("ejecutando la consulta en GetAll lack_list_password: %v", err)
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		m, err := s.scanRow(rows)
		if err != nil {
			logger_trace.Error.Printf("escaneando el registro en lack_list_password: %v", err)
			return ms, err
		}

		ms = append(ms, *m)
	}

	return ms, nil
}

func (s sqlserver) scanRow(rs db.RowScanner) (*Model, error) {
	m := &Model{}
	cn := pq.NullTime{}
	un := pq.NullTime{}

	err := rs.Scan(
		&m.ID,
		&m.Passwd,
		&cn,
		&un,
	)
	if err == sql.ErrNoRows {
		return nil, err
	}
	if err != nil {
		logger_trace.Error.Printf("escaneando el modelo lack_list_password: %v", err)
		return nil, err
	}

	m.CreatedAt = cn.Time
	m.UpdatedAt = un.Time
	return m, nil
}

func (s sqlserver) GetByPassword(pwd string) (*Model, error) {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(sqlGetByPassword)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en sqlGetByPassword Black_list_password: %v", err)
		return nil, err
	}
	defer stmt.Close()

	row := stmt.QueryRow(sql.Named("passwd", pwd))
	return s.scanRow(row)
}
