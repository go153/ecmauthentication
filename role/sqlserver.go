package role

import (
	"database/sql"

	"github.com/lib/pq"
	"gitlab.com/e-capture/ecatch-ecm/majosystem/db"
	"gitlab.com/e-capture/ecatch-ecm/majosystem/logger_trace"
)

// sqlserver estructura de almacenamiento en postgres
type sqlserver struct{}

const (
	sqlserverInsert  = `INSERT INTO usr.roles (name, description, sessions_allowed) VALUES (@name, @description, @sessions_allowed)  SELECT ID = convert(bigint, SCOPE_IDENTITY())`
	sqlserverUpdate  = `UPDATE usr.roles SET name = @name, description = @description, sessions_allowed = @sessions_allowed WHERE id = @id`
	sqlserverDelete  = `DELETE FROM usr.roles WHERE id = @id`
	sqlserverGetByID = `SELECT id, name, description, sessions_allowed, created_at, updated_at FROM usr.roles WHERE id = @id`
	sqlserverGetAll  = `SELECT id, name, description, sessions_allowed, created_at, updated_at FROM usr.roles`
)

func (p sqlserver) Create(m *Model) error {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(sqlserverInsert)
	if err != nil {
		logger_trace.Error.Printf("sqlserver: Prepare Create Role: %v", err)
		return err
	}
	defer stmt.Close()

	result, err := db.ExecGettingID(
		stmt,
		sql.Named("name", m.Name),
		sql.Named("description", m.Description),
		sql.Named("sessions_allowed", m.SessionsAllowed),
	)
	if err != nil {
		logger_trace.Error.Printf("sqlserver: Excecute Create Role: %v", err)
		return err
	}

	m.ID = result

	/*
		// Error al envar un id que no posible capturar en la consulta.
		id, err := result.LastInsertId()
		if err != nil {
			logger_trace.Error.Printf("sqlserver: obteniendo el id en Create Role: %v", err)
			return err
		}
	*/
	m.ID = result

	return nil
}

func (p sqlserver) Update(m *Model, id int) error {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(sqlserverUpdate)
	if err != nil {
		logger_trace.Error.Printf("sqlserver: Prepare Update Role: %v", err)
		return err
	}
	defer stmt.Close()

	err = db.ExecAffectingOneRow(stmt,
		sql.Named("name", m.Name),
		sql.Named("description", m.Description),
		sql.Named("sessions_allowed", m.SessionsAllowed),
		sql.Named("id", id))
	if err != nil {
		logger_trace.Error.Printf("sqlserver: Excecute Update Role: %v", err)
		return err
	}

	return nil
}

func (p sqlserver) Delete(id int) error {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(sqlserverDelete)
	if err != nil {
		logger_trace.Error.Printf("sqlserver: Prepare Delete role: %v", err)
		return err
	}
	defer stmt.Close()

	err = db.ExecAffectingOneRow(stmt, sql.Named("id", id))
	if err != nil {
		logger_trace.Error.Printf("sqlserver: Execute Delete role: %v", err)
		return err
	}

	return nil
}

func (p sqlserver) GetByID(id int) (*Model, error) {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(sqlserverGetByID)
	if err != nil {
		logger_trace.Error.Printf("sqlserver: Prepare GetByID role: %v", err)
		return nil, err
	}
	defer stmt.Close()

	row := stmt.QueryRow(sql.Named("id", id))
	return p.scanRow(row)
}

func (p sqlserver) GetAll() (Models, error) {
	ms := make(Models, 0)
	conn := db.GetConnection()

	stmt, err := conn.Prepare(sqlserverGetAll)
	if err != nil {
		logger_trace.Error.Printf("sqlserver: Prepare GetAll role: %v", err)
		return ms, err
	}
	defer stmt.Close()

	rows, err := stmt.Query()
	if err != nil {
		logger_trace.Error.Printf("sqlserver: Querying GetAll role: %v", err)
		return ms, err
	}
	defer rows.Close()

	for rows.Next() {
		m, err := p.scanRow(rows)
		if err != nil {
			logger_trace.Error.Printf("sqlserver: Scanning single row in role: %v", err)
			return ms, err
		}

		ms = append(ms, *m)
	}

	return ms, nil
}

func (p sqlserver) scanRow(s db.RowScanner) (*Model, error) {
	m := &Model{}
	cn := pq.NullTime{}
	un := pq.NullTime{}

	err := s.Scan(
		&m.ID,
		&m.Name,
		&m.Description,
		&m.SessionsAllowed,
		&cn,
		&un,
	)
	if err != nil {
		logger_trace.Error.Printf("escaneando el role: %v", err)
		return nil, err
	}

	m.CreatedAt = cn.Time
	m.UpdatedAt = un.Time
	return m, nil
}
