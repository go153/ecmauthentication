package role

import (
	"fmt"
	"net/http"

	"gitlab.com/e-capture/ecatch-ecm/majosystem/logger_trace"
	"gitlab.com/e-capture/ecatch-ecm/majosystem/response"

	"strconv"

	"github.com/labstack/echo"
)

// Create handler role
func Create(c echo.Context) error {
	rm := response.Model{}
	m := Model{}

	err := c.Bind(&m)
	if err != nil {
		errStr := fmt.Sprintf("estructura no válida: %v", err)
		logger_trace.Error.Print(errStr)
		rm.Set(true, nil, 1)
		return c.JSON(http.StatusAccepted, rm)
	}

	err = m.Create()
	if err != nil {
		errStr := fmt.Sprintf("no se pudo crear el registro: %v", err)
		logger_trace.Error.Print(errStr)
		rm.Set(true, nil, 3)
		return c.JSON(http.StatusAccepted, rm)
	}

	rm.Set(false, m, 4)
	return c.JSON(http.StatusCreated, rm)
}

// Update handler role
func Update(c echo.Context) error {
	rm := response.Model{}
	m := Model{}

	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		errStr := fmt.Sprint("el id debe ser numérico")
		logger_trace.Error.Print(errStr)
		rm.Set(true, nil, 17)
		return c.JSON(http.StatusAccepted, rm)
	}

	err = c.Bind(&m)
	if err != nil {
		errStr := fmt.Sprintf("la estructura no es válida: %v", err)
		logger_trace.Error.Print(errStr)
		rm.Set(true, nil, 1)
		return c.JSON(http.StatusAccepted, rm)
	}

	err = m.Update(id)
	if err != nil {
		errStr := fmt.Sprintf("no se pudo actualizar el registro: %v", err)
		logger_trace.Error.Print(errStr)
		rm.Set(true, nil, 18)
		return c.JSON(http.StatusAccepted, rm)
	}

	rm.Set(false, nil, 19)
	return c.JSON(http.StatusOK, rm)
}

// Delete handler role
func Delete(c echo.Context) error {
	rm := response.Model{}
	m := Model{}

	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		errStr := fmt.Sprint("el id debe ser numérico")
		logger_trace.Error.Print(errStr)
		rm.Set(true, nil, 17)
		return c.JSON(http.StatusAccepted, rm)
	}

	err = m.Delete(id)
	if err != nil {
		errStr := fmt.Sprintf("no se pudo eliminar el registro: %v", err)
		logger_trace.Error.Print(errStr)
		rm.Set(true, nil, 20)
		return c.JSON(http.StatusAccepted, rm)
	}

	rm.Set(false, nil, 28)
	return c.JSON(http.StatusOK, rm)
}

// GetByID handler role
func GetByID(c echo.Context) error {
	rm := response.Model{}
	m := Model{}

	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		errStr := fmt.Sprint("el id debe ser numérico")
		logger_trace.Error.Print(errStr)
		rm.Set(true, nil, 17)
		return c.JSON(http.StatusAccepted, rm)
	}

	r, err := m.GetByID(id)
	if err != nil {
		errStr := fmt.Sprintf("no se pudo consultar el registro: %v", err)
		logger_trace.Error.Print(errStr)
		rm.Set(true, nil, 22)
		return c.JSON(http.StatusAccepted, rm)
	}

	rm.Set(false, r, 29)
	return c.JSON(http.StatusOK, rm)
}

// GetAll handler role
func GetAll(c echo.Context) error {
	rm := response.Model{}
	m := Model{}

	r, err := m.GetAll()
	if err != nil {
		errStr := fmt.Sprintf("no se pudo consultar los registros: %v", err)
		logger_trace.Error.Print(errStr)
		rm.Set(true, nil, 22)
		return c.JSON(http.StatusAccepted, rm)
	}

	rm.Set(false, r, 29)
	return c.JSON(http.StatusOK, rm)
}
