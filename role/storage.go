package role

import (
	"strings"

	"gitlab.com/e-capture/ecatch-ecm/majosystem/configuration"
	"gitlab.com/e-capture/ecatch-ecm/majosystem/db"
	"gitlab.com/e-capture/ecatch-ecm/majosystem/logger_trace"
)

var s storage

func init() {
	setStorage()
}

// storage interface para el uso de DAO
type storage interface {
	Create(m *Model) error
	Update(m *Model, id int) error
	Delete(id int) error
	GetByID(id int) (*Model, error)
	GetAll() (Models, error)
	scanRow(scanner db.RowScanner) (*Model, error)
}

// setStorage asigna el sistema de almacenamiento
func setStorage() {
	c := configuration.FromFile()
	switch strings.ToLower(c.DBConnection) {
	case "sqlserver":
		s = sqlserver{}
	case "postgres":
		fallthrough
	case "mysql":
		fallthrough
	case "oracle":
		fallthrough
	default:
		logger_trace.Error.Printf("este motor de bd no está configurado aún: %s", c.DBConnection)
	}
}
