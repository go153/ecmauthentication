package role

import "time"

// Model estructura del role
type Model struct {
	ID              int64       `json:"id"`
	Name            string    `json:"name"`
	Description     string    `json:"description"`
	SessionsAllowed int       `json:"sessions_allowed"`
	CreatedAt       time.Time `json:"created_at"`
	UpdatedAt       time.Time `json:"updated_at"`
}

// Models slice of Model
type Models []Model

// Create registra un nuevo role
func (m *Model) Create() error {
	return s.Create(m)
}

// Update actualiza el role
func (m *Model) Update(id int) error {
	return s.Update(m, id)
}

// Delete borra un role
func (m *Model) Delete(id int) error {
	return s.Delete(id)
}

// GetByID consulta un role por su id
func (m *Model) GetByID(id int) (*Model, error) {
	return s.GetByID(id)
}

// GetAll consulta todos los roles
func (m *Model) GetAll() (Models, error) {
	return s.GetAll()
}
