package role

import (
	"github.com/lib/pq"
	"gitlab.com/e-capture/ecatch-ecm/majosystem/db"
	"gitlab.com/e-capture/ecatch-ecm/majosystem/logger_trace"
)

// psql estructura de almacenamiento en postgres
type psql struct{}

const (
	psqlInsert  = `INSERT INTO roles (name, description) VALUES ($1, $2) RETURNING id`
	psqlUpdate  = `UPDATE roles SET name = $1, description = $2 WHERE id = $3`
	psqlDelete  = `DELETE FROM roles WHERE id = $1`
	psqlGetByID = `SELECT id, name, description, created_at, updated_at FROM roles WHERE id = $1`
	psqlGetAll  = `SELECT id, name, description, created_at, updated_at FROM roles`
)

func (p psql) Create(m *Model) error {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(psqlInsert)
	if err != nil {
		logger_trace.Error.Printf("psql: Prepare Create Role: %v", err)
		return err
	}
	defer stmt.Close()

	err = stmt.QueryRow(m.Name, m.Description).Scan(&m.ID)
	if err != nil {
		logger_trace.Error.Printf("psql: Excecute Create Role: %v", err)
		return err
	}

	return nil
}

func (p psql) Update(m *Model, id int) error {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(psqlUpdate)
	if err != nil {
		logger_trace.Error.Printf("psql: Prepare Update Role: %v", err)
		return err
	}
	defer stmt.Close()

	err = db.ExecAffectingOneRow(stmt, m.Name, m.Description, id)
	if err != nil {
		logger_trace.Error.Printf("psql: Excecute Update Role: %v", err)
		return err
	}

	return nil
}

func (p psql) Delete(id int) error {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(psqlDelete)
	if err != nil {
		logger_trace.Error.Printf("psql: Prepare Delete role: %v", err)
		return err
	}
	defer stmt.Close()

	err = db.ExecAffectingOneRow(stmt, id)
	if err != nil {
		logger_trace.Error.Printf("psql: Execute Delete role: %v", err)
		return err
	}

	return nil
}

func (p psql) GetByID(id int) (*Model, error) {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(psqlGetByID)
	if err != nil {
		logger_trace.Error.Printf("psql: Prepare GetByID role: %v", err)
		return nil, err
	}
	defer stmt.Close()

	row := stmt.QueryRow()
	return p.scanRow(row)
}

func (p psql) GetAll() (Models, error) {
	ms := make(Models, 0)
	conn := db.GetConnection()

	stmt, err := conn.Prepare(psqlGetAll)
	if err != nil {
		logger_trace.Error.Printf("psql: Prepare GetAll role: %v", err)
		return ms, err
	}
	defer stmt.Close()

	rows, err := stmt.Query()
	if err != nil {
		logger_trace.Error.Printf("psql: Querying GetAll role: %v", err)
		return ms, err
	}
	defer rows.Close()

	for rows.Next() {
		m, err := p.scanRow(rows)
		if err != nil {
			logger_trace.Error.Printf("psql: Scanning single row in role: %v", err)
			return ms, err
		}

		ms = append(ms, *m)
	}

	return ms, nil
}

func (p psql) scanRow(s db.RowScanner) (*Model, error) {
	m := &Model{}
	cn := pq.NullTime{}
	un := pq.NullTime{}

	err := s.Scan(
		&m.ID,
		&m.Name,
		&m.Description,
		&cn,
		&un,
	)
	if err != nil {
		logger_trace.Error.Printf("escaneando el role: %v", err)
		return nil, err
	}

	m.CreatedAt = cn.Time
	m.UpdatedAt = un.Time

	return m, nil
}
