package role

import (
	"fmt"
	"testing"
)

func TestRoleCreate(t *testing.T) {
	m := Model{
		Name:        "TEST",
		Description: "TEST CREATE",
		PrivilegeID: 1,
	}

	err := m.Create()
	if err != nil {
		t.Errorf("no se creó el registro: %v", err)
	}

	if m.ID == 0 {
		t.Errorf("no se obtuvo el id del registro")
	}
}

func TestRoleUpdate(t *testing.T) {
	m := Model{
		Name:        "TEST UPDATE",
		Description: "TEST UPDATE",
		PrivilegeID: 1,
	}

	err := m.Create()
	if err != nil {
		t.Errorf("no se creó el registro: %v", err)
	}

	if m.ID == 0 {
		t.Error("no se obtuvo el id del registro")
	}

	m.Description = "Updated"
	err = m.Update(m.ID)
	if err != nil {
		t.Errorf("no se actualizó el registro: %v", err)
	}
}

func TestRoleDelete(t *testing.T) {
	m := Model{
		Name:        "TEST DELETE",
		Description: "TEST DELETE",
		PrivilegeID: 1,
	}

	err := m.Create()
	if err != nil {
		t.Errorf("no se creó el registro: %v", err)
	}

	if m.ID == 0 {
		t.Error("no se obtuvo el id del registro")
	}

	err = m.Delete(m.ID)
	if err != nil {
		t.Errorf("no se eliminó el registro: %v", err)
	}
}

func TestRoleGetByID(t *testing.T) {
	m := Model{
		Name:        "TEST QUERY",
		Description: "TEST QUERY",
		PrivilegeID: 1,
	}

	err := m.Create()
	if err != nil {
		t.Errorf("no se creó el registro: %v", err)
	}

	if m.ID == 0 {
		t.Error("no se obtuvo el id del registro")
	}

	r, err := m.GetByID(m.ID)
	if err != nil {
		t.Errorf("no se pudo consultar el registro: %v", err)
	}

	if r.ID == 0 {
		t.Errorf("registro no es el deseado: %v", r)
	}
}

func TestRoleGetAll(t *testing.T) {
	m := Model{}

	ms, err := m.GetAll()
	if err != nil {
		t.Errorf("no se pudo obtener la información: %v", err)
	}

	fmt.Println(ms)
}
