package dates_disallowed

import (
	"database/sql"

	"github.com/lib/pq"
	"gitlab.com/e-capture/ecatch-ecm/majosystem/db"
	"gitlab.com/e-capture/ecatch-ecm/majosystem/logger_trace"
)

type sqlserver struct{}

const (
	sqlInsert           = `INSERT INTO usr.dates_disalloweds (role_id, begins_at, ends_at, description) VALUES (@role_id, @begins_at, @ends_at, @description)  SELECT ID = convert(bigint, SCOPE_IDENTITY()) `
	sqlUpdate           = `UPDATE usr.dates_disalloweds SET role_id = @role_id, begins_at = @begins_at, ends_at = @ends_at, description = @description, updated_at = getdate() WHERE id = @id`
	sqlDelete           = `DELETE FROM usr.dates_disalloweds WHERE id = @id`
	sqlGetByID          = `SELECT id, role_id, begins_at, ends_at, description, created_at, pdated_at FROM usr.dates_disalloweds WITH (NOLOCK) WHERE id = @id`
	sqlGetAll           = `SELECT id, role_id, begins_at, ends_at, description,  created_at, updated_at FROM usr.dates_disalloweds WITH (NOLOCK)`
	sqlGetByDateBetween = sqlGetAll + ` WHERE role_id = @roleid AND getdate() BETWEEN begins_at AND ends_at`
)

func (s sqlserver) Create(m *Model) error {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(sqlInsert)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en Create dates_disallowed: %v", err)
		return err
	}
	defer stmt.Close()

	ID, err := db.ExecGettingID(
		stmt,
		sql.Named("role_id", m.RoleId),
		sql.Named("begins_at", m.BeginsAt),
		sql.Named("ends_at", m.EndsAt),
		sql.Named("description", m.Description),
	)
	if err != nil {
		logger_trace.Error.Printf("ejecutando la consulta en Create dates_disallowed: %v", err)
		return err
	}
	m.ID = ID

	return nil
}

func (s sqlserver) Update(ID int64, m *Model) error {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(sqlUpdate)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en Update dates_disallowed: %v", err)
		return err
	}
	defer stmt.Close()

	err = db.ExecAffectingOneRow(
		stmt,
		sql.Named("role_id", m.RoleId),
		sql.Named("begins_at", m.BeginsAt),
		sql.Named("ends_at", m.EndsAt),
		sql.Named("description", m.Description),
		sql.Named("id", ID),
	)
	if err != nil {
		logger_trace.Error.Printf("ejecutando la consulta en Update dates_disallowed: %v", err)
		return err
	}
	m.ID = ID

	return nil
}

func (s sqlserver) Delete(ID int64) error {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(sqlDelete)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en Delete dates_disallowed: %v", err)
		return err
	}
	defer stmt.Close()

	err = db.ExecAffectingOneRow(
		stmt,
		sql.Named("id", ID),
	)
	if err != nil {
		logger_trace.Error.Printf("ejecutando la consulta en Delete dates_disallowed: %v", err)
		return err
	}

	return nil
}

func (s sqlserver) GetByID(ID int64) (*Model, error) {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(sqlGetByID)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en GetByID dates_disallowed: %v", err)
		return nil, err
	}
	defer stmt.Close()

	row := stmt.QueryRow(sql.Named("id", ID))
	return s.scanRow(row)
}

func (s sqlserver) GetAll() (Models, error) {
	conn := db.GetConnection()
	ms := make(Models, 0)

	stmt, err := conn.Prepare(sqlGetAll)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en GetAll dates_disallowed: %v", err)
		return nil, err
	}
	defer stmt.Close()

	rows, err := stmt.Query()

	if err != nil {
		logger_trace.Error.Printf("ejecutando la consulta en GetAll dates_disallowed: %v", err)
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		m, err := s.scanRow(rows)

		if err != nil {
			logger_trace.Error.Printf("escaneando el registro en dates_disallowed: %v", err)
			return ms, err
		}

		ms = append(ms, *m)
	}

	return ms, nil
}

func (s sqlserver) scanRow(rs db.RowScanner) (*Model, error) {
	m := &Model{}
	cn := pq.NullTime{}
	un := pq.NullTime{}

	err := rs.Scan(
		&m.ID,
		&m.RoleId,
		&m.BeginsAt,
		&m.EndsAt,
		&m.Description,
		&cn,
		&un,
	)
	if err == sql.ErrNoRows {
		return nil, nil
	}
	if err != nil {
		logger_trace.Error.Printf("escaneando el modelo dates_disallowed: %v", err)
		return nil, err
	}

	m.CreatedAt = cn.Time
	m.UpdatedAt = un.Time
	return m, nil
}

func (s sqlserver) GetByDateBetween(rID int) (*Model, error) {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(sqlGetByDateBetween)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en GetByID dates_disallowed: %v", err)
		return nil, err
	}
	defer stmt.Close()

	row := stmt.QueryRow(sql.Named("roleid", rID))
	return s.scanRow(row)
}
