package dates_disallowed

import "time"

type Model struct {
	ID          int64     `json:"id"`
	RoleId      int       `json:"role_id"`
	BeginsAt    time.Time `json:"begins_at"`
	EndsAt      time.Time `json:"ends_at"`
	Description string    `json:"description"`
	CreatedAt   time.Time `json:"created_at"`
	UpdatedAt   time.Time `json:"updated_at"`
}

type Models []Model

func (m *Model) Create() error {
	return s.Create(m)
}

func (m *Model) Update(ID int64) error {
	return s.Update(ID, m)
}

func (m *Model) Delete(ID int64) error {
	return s.Delete(ID)
}

func (m *Model) GetByID(ID int64) (*Model, error) {
	return s.GetByID(ID)
}

func (m *Model) GetAll() (Models, error) {
	return s.GetAll()
}

func (m *Model) GetByDateBetween() (*Model, error) {
	return s.GetByDateBetween(m.RoleId)
}
