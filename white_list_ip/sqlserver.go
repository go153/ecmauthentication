package white_list_ip

import (
	"database/sql"

	"github.com/lib/pq"
	"gitlab.com/e-capture/ecatch-ecm/majosystem/db"
	"gitlab.com/e-capture/ecatch-ecm/majosystem/logger_trace"
)

type sqlserver struct{}

const (
	sqlInsert  = `INSERT INTO usr.white_list_ips (ip, description) VALUES (@ip, @description) SELECT ID = convert(bigint, SCOPE_IDENTITY()) `
	sqlUpdate  = `UPDATE usr.white_list_ips SET ip = @ip, description = @description,  updated_at = getdate() WHERE id = @id`
	sqlDelete  = `DELETE FROM usr.white_list_ips WHERE id = @id`
	sqlGetByID = `SELECT id, ip, description,  created_at, updated_at FROM usr.white_list_ips WITH (NOLOCK) WHERE id = @id`
	sqlGetAll  = `SELECT id, ip, description,  created_at, updated_at FROM usr.white_list_ips WITH (NOLOCK)`
	sqlGetByIP = sqlGetAll + ` WHERE ip = @ip`
)

func (s sqlserver) Create(m *Model) error {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(sqlInsert)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en Create white_list_ip: %v", err)
		return err
	}
	defer stmt.Close()

	ID, err := db.ExecGettingID(
		stmt,
		sql.Named("ip", m.Ip),
		sql.Named("description", m.Description),
	)
	if err != nil {
		logger_trace.Error.Printf("ejecutando la consulta en Create white_list_ip: %v", err)
		return err
	}
	m.ID = ID

	return nil
}

func (s sqlserver) Update(ID int64, m *Model) error {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(sqlUpdate)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en Update white_list_ip: %v", err)
		return err
	}
	defer stmt.Close()

	err = db.ExecAffectingOneRow(
		stmt,
		sql.Named("ip", m.Ip),
		sql.Named("description", m.Description),
		sql.Named("id", ID),
	)
	if err != nil {
		logger_trace.Error.Printf("ejecutando la consulta en Update white_list_ip: %v", err)
		return err
	}
	m.ID = ID

	return nil
}

func (s sqlserver) Delete(ID int64) error {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(sqlDelete)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en Delete white_list_ip: %v", err)
		return err
	}
	defer stmt.Close()

	err = db.ExecAffectingOneRow(
		stmt,
		sql.Named("id", ID),
	)
	if err != nil {
		logger_trace.Error.Printf("ejecutando la consulta en Delete white_list_ip: %v", err)
		return err
	}

	return nil
}

func (s sqlserver) GetByID(ID int64) (*Model, error) {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(sqlGetByID)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en GetByID white_list_ip: %v", err)
		return nil, err
	}
	defer stmt.Close()

	row := stmt.QueryRow(sql.Named("id", ID))
	return s.scanRow(row)
}

func (s sqlserver) GetAll() (Models, error) {
	conn := db.GetConnection()
	ms := make(Models, 0)

	stmt, err := conn.Prepare(sqlGetAll)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en GetAll white_list_ip: %v", err)
		return nil, err
	}
	defer stmt.Close()

	rows, err := stmt.Query()
	if err != nil {
		logger_trace.Error.Printf("ejecutando la consulta en GetAll white_list_ip: %v", err)
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		m, err := s.scanRow(rows)
		if err != nil {
			logger_trace.Error.Printf("escaneando el registro en white_list_ip: %v", err)
			return ms, err
		}

		ms = append(ms, *m)
	}

	return ms, nil
}

func (s sqlserver) scanRow(rs db.RowScanner) (*Model, error) {
	m := &Model{}
	cn := pq.NullTime{}
	un := pq.NullTime{}

	err := rs.Scan(
		&m.ID,
		&m.Ip,
		&m.Description,
		&cn,
		&un,
	)
	if err != nil {
		logger_trace.Error.Printf("escaneando el modelo white_list_ip: %v", err)
		return nil, err
	}

	m.CreatedAt = cn.Time
	m.UpdatedAt = un.Time
	return m, nil
}

func (s sqlserver) GetByIP(m *Model) (*Model, error) {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(sqlGetByIP)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en sqlGetByIP white_list_ip: %v", err)
		return nil, err
	}
	defer stmt.Close()

	row := stmt.QueryRow(sql.Named("ip", m.Ip))
	return s.scanRow(row)
}
