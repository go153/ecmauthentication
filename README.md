# ecmauthentication

## Crear archivos para firmar token
```bash
openssl genrsa -out app.rsa 1024
openssl rsa -in app.rsa -pubout > app.rsa.pub
```

## Crear archivos autofirmados para SSL
```bash
openssl req -x509 -newkey rsa:4096 -keyout key.pem -nodes -out cert.pem -days 365
```
