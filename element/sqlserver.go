package element

import (
	"database/sql"

	"github.com/lib/pq"
	"gitlab.com/e-capture/ecatch-ecm/majosystem/db"
	"gitlab.com/e-capture/ecatch-ecm/majosystem/logger_trace"
)

type sqlserver struct{}

const (
	sqlInsert                = `INSERT INTO usr.elements (name, description ,url_back, component_id) VALUES (@name, @description, @url_back, @component_id)  SELECT ID = convert(bigint, SCOPE_IDENTITY()) `
	sqlUpdate                = `UPDATE usr.elements SET name = @name, description=@description, url_back = @url_back, component_id = @component_id,  updated_at = getdate() WHERE id = @id`
	sqlDelete                = `DELETE FROM usr.elements WHERE id = @id`
	sqlGetByID               = `SELECT id, name, description, url_back, component_id,  created_at, updated_at FROM usr.elements WITH (NOLOCK) WHERE id = @id`
	sqlGetAll                = `SELECT id, name, description, url_back, component_id,  created_at, updated_at FROM usr.elements WITH (NOLOCK)`
	sqlGetElementByComponent = `SELECT ue.name FROM usr.elements ue WITH (NOLOCK) INNER JOIN usr.components uc WITH (NOLOCK) ON (ue.component_id = uc.id)  INNER JOIN usr.role_usr_elements re  WITH (NOLOCK) ON (ue.id = re.element_id) WHERE uc.name = 'Index' and re.role_id =1`
)

func (s sqlserver) Create(m *Model) error {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(sqlInsert)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en Create element: %v", err)
		return err
	}
	defer stmt.Close()

	ID, err := db.ExecGettingID(
		stmt,
		sql.Named("description", m.Description),
		sql.Named("name", m.Name),
		sql.Named("url_back", m.UrlBack),
		sql.Named("component_id", m.ComponentId),
	)
	if err != nil {
		logger_trace.Error.Printf("ejecutando la consulta en Create element: %v", err)
		return err
	}
	m.ID = ID

	return nil
}

func (s sqlserver) Update(ID int64, m *Model) error {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(sqlUpdate)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en Update element: %v", err)
		return err
	}
	defer stmt.Close()

	err = db.ExecAffectingOneRow(
		stmt,

		sql.Named("name", m.Name),
		sql.Named("description", m.Description),
		sql.Named("url_back", m.UrlBack),
		sql.Named("component_id", m.ComponentId),
		sql.Named("id", ID),
	)
	if err != nil {
		logger_trace.Error.Printf("ejecutando la consulta en Update element: %v", err)
		return err
	}
	m.ID = ID

	return nil
}

func (s sqlserver) Delete(ID int64) error {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(sqlDelete)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en Delete element: %v", err)
		return err
	}
	defer stmt.Close()

	err = db.ExecAffectingOneRow(
		stmt,
		sql.Named("id", ID),
	)
	if err != nil {
		logger_trace.Error.Printf("ejecutando la consulta en Delete element: %v", err)
		return err
	}

	return nil
}

func (s sqlserver) GetByID(ID int64) (*Model, error) {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(sqlGetByID)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en GetByID element: %v", err)
		return nil, err
	}
	defer stmt.Close()

	row := stmt.QueryRow(sql.Named("id", ID))
	return s.scanRow(row)
}

func (s sqlserver) GetAll() (Models, error) {
	conn := db.GetConnection()
	ms := make(Models, 0)

	stmt, err := conn.Prepare(sqlGetAll)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en GetAll element: %v", err)
		return nil, err
	}
	defer stmt.Close()

	rows, err := stmt.Query()
	if err != nil {
		logger_trace.Error.Printf("ejecutando la consulta en GetAll element: %v", err)
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		m, err := s.scanRow(rows)
		if err != nil {
			logger_trace.Error.Printf("escaneando el registro en element: %v", err)
			return ms, err
		}

		ms = append(ms, *m)
	}

	return ms, nil
}

func (s sqlserver) scanRow(rs db.RowScanner) (*Model, error) {
	m := &Model{}
	cn := pq.NullTime{}
	un := pq.NullTime{}

	err := rs.Scan(
		&m.ID,
		&m.Description,
		&m.Name,
		&m.UrlBack,
		&m.ComponentId,
		&cn,
		&un,
	)
	if err != nil {
		logger_trace.Error.Printf("escaneando el modelo element: %v", err)
		return nil, err
	}

	m.CreatedAt = cn.Time
	m.UpdatedAt = un.Time
	return m, nil
}

func (s sqlserver) GetElementByComponent(component string, role_id int) (*Models, error) {
	conn := db.GetConnection()
	m := Model{}
	ms := Models{}

	stmt, err := conn.Prepare(sqlGetElementByComponent)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en sqlGetElementByComponent element: %v", err)
		return nil, err
	}
	defer stmt.Close()

	rows, err := stmt.Query(sql.Named("component", component), sql.Named("role", role_id))
	if err != nil {
		logger_trace.Error.Printf("ejecutando la consulta en sqlGetElementByComponent element: %v", err)
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		err := rows.Scan(&m.Name)
		if err != nil {
			logger_trace.Error.Printf("escaneando el modelo element: %v", err)
			return nil, err
		}
		ms = append(ms, m)
	}

	return &ms, nil
}
