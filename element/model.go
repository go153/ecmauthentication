package element

import "time"

type Model struct {
	ID          int64     `json:"id"`
	Description string    `json:description`
	Name        string    `json:"name"`
	UrlBack     string    `json:"url_back"`
	ComponentId int       `json:"component_id"`
	CreatedAt   time.Time `json:"created_at"`
	UpdatedAt   time.Time `json:"updated_at"`
}

type Models []Model

func (m *Model) Create() error {
	return s.Create(m)
}

func (m *Model) Update(ID int64) error {
	return s.Update(ID, m)
}

func (m *Model) Delete(ID int64) error {
	return s.Delete(ID)
}

func (m *Model) GetByID(ID int64) (*Model, error) {
	return s.GetByID(ID)
}

func (m *Model) GetAll() (Models, error) {
	return s.GetAll()
}

func (m *Model) GetElementByComponent(component string, role_id int) (*Models, error) {
	return s.GetElementByComponent(component, role_id)
}
