package main

import (
	"fmt"

	"gitlab.com/e-capture/ecatch-ecm/ecmauthentication/user"
)

func main() {
	var opt int
	showHeader()
	fmt.Scan(&opt)
	switch opt {
	case 1:
		createUser()
	default:
		fmt.Println("Not valid option. Bye!")
	}
}

func createUser() {
	u := user.Model{}
	for u.Name == "" {
		fmt.Print("Name*: ")
		fmt.Scanln(&u.Name)
	}
	for u.Lastname == "" {
		fmt.Print("Lastname*: ")
		fmt.Scanln(&u.Lastname)
	}
	for u.Identification == "" {
		fmt.Print("Identification*: ")
		fmt.Scanln(&u.Identification)
	}
	for u.Phonenumber == "" {
		fmt.Print("Phone: ")
		fmt.Scanln(&u.Phonenumber)
	}
	for u.Email == "" {
		fmt.Print("Email*: ")
		fmt.Scanln(&u.Email)
	}
	for u.Password == "" {
		fmt.Print("Password*: ")
		fmt.Scanln(&u.Password)
	}
	for u.RoleID == 0 {
		fmt.Print("Role ID*: ")
		fmt.Scanf("%d", &u.RoleID)
	}
	fmt.Println(u)
	err := u.Create()
	if err != nil {
		fmt.Printf("no se pudo crear el usuario: %v", err)
		return
	}

	fmt.Printf("usuario creado con el ID: %d", u.ID)
}

func showHeader() {
	fmt.Println("==========================")
	fmt.Println("|   AUTHENTICATION CLI   |")
	fmt.Println("|                        |")
	fmt.Println("|  1. CREATE USER        |")
	fmt.Println("|                        |")
	fmt.Println("==========================")
	fmt.Println()
	fmt.Print(" Select an option: ")
}
