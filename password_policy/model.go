package password_policy

import "time"

type Model struct {
	ID     int64 `json:"id"`
	RoleId int64 `json:"role_id"`
	Validity int `json:"validity"` 		// Validity es la cantidad de días que una contraseña es válida
	MaxLength int `json:"max_length"`	// MaxLength es el tamaño máximo de una contraseña
	MinLength int `json:"min_length"`	// MinLength es el tamaño mínimo de una contraseña
	Times int `json:"times"`		// Times cantidad de contraseñas almacenadas que no puede repetir el usuario
	FailedAttempts int `json:"failed_attempts"`	// FailedAttempts es la cantidad máxima de intentos fallidos de login
	TimeUnlock int `json:"time_unlock"`	// TimeUnlock es el tiempo que un usuario estará bloqueado (minutos)
	Alpha int `json:"alpha"`		// Alpha Cantidad de caracteres que debe tener la contraseña
	Digits int `json:"digits"`		// Digits Cantidad de números que debe tener la contraseña
	Special int `json:"special"`		// Special Cantidad de caracteres especiales que debe tener la contraseña
	UpperCase int `json:"upper_case"`	// UpperCase cantidad de mayúsculas que debe tener la contraseña
	LowerCase int `json:"lower_case"`	// LowerCase cantidad de minúsculas que debe tener la contraseña
	Enable bool `json:"enable"`		// Enable Identifica si la política está habilitada o no
	InactivityTime int `json:"inactivity_time"`	// InactivityTime es el tiempo máximo que un usuario puede tener sin haberse logueado al sistema
	Timeout   int       `json:"timeout"`		// Timeout es el tiempo de inactividad de una sesión activa
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}

type Models []Model

func (m *Model) Create() error {
	return s.Create(m)
}

func (m *Model) Update(ID int64) error {
	return s.Update(ID, m)
}

func (m *Model) UpdateByRoleID(ID int64) error {
	return s.UpdateByRoleID(ID, m)
}

func (m *Model) Delete(ID int64) error {
	return s.Delete(ID)
}

func (m *Model) GetByID(ID int64) (*Model, error) {
	return s.GetByID(ID)
}

func (m *Model) GetAll() (Models, error) {
	return s.GetAll()
}

func (m *Model) GetByRoleID(ID int64) (*Model, error) {
	return s.GetByRoleID(ID)
}
