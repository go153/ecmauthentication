package password_policy

import (
	"github.com/lib/pq"
	"gitlab.com/e-capture/ecatch-ecm/majosystem/db"
	"gitlab.com/e-capture/ecatch-ecm/majosystem/logger_trace"
)

type orcl struct{}

const (
	orclInsert  = `INSERT INTO usr_password_policy (role_id, validity, max_length, min_length, times, failed_attempts, time_unlock, alpha, digits, special, upper_case, lower_case, enable, ) VALUES (:0, :1, :2, :3, :4, :5, :6, :7, :8, :9, :10, :11, :12, )`
	orclUpdate  = `UPDATE usr_password_policy SET role_id = :0, validity = :1, max_length = :2, min_length = :3, times = :4, failed_attempts = :5, time_unlock = :6, alpha = :7, digits = :8, special = :9, upper_case = :10, lower_case = :11, enable = :12,  updated_at = getdate() WHERE id = :1`
	orclDelete  = `DELETE FROM usr_password_policy WHERE id = :1`
	orclGetByID = `SELECT id, role_id, validity, max_length, min_length, times, failed_attempts, time_unlock, alpha, digits, special, upper_case, lower_case, enable,  created_at, updated_at FROM usr_password_policy WITH (NOLOCK) WHERE id = :1`
	orclGetAll  = `SELECT id, role_id, validity, max_length, min_length, times, failed_attempts, time_unlock, alpha, digits, special, upper_case, lower_case, enable,  created_at, updated_at FROM usr_password_policy WITH (NOLOCK)`
)

func (o orcl) Create(m *Model) error {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(orclInsert)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en Create password_policy: %v", err)
		return err
	}
	defer stmt.Close()

	ID, err := db.ExecGettingID(
		stmt,
		&m.RoleId, &m.Validity, &m.MaxLength, &m.MinLength, &m.Times, &m.FailedAttempts, &m.TimeUnlock, &m.Alpha, &m.Digits, &m.Special, &m.UpperCase, &m.LowerCase, &m.Enable,
	)
	if err != nil {
		logger_trace.Error.Printf("ejecutando la consulta en Create password_policy: %v", err)
		return err
	}
	m.ID = ID

	return nil
}

func (o orcl) Update(ID int64, m *Model) error {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(orclUpdate)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en Update password_policy: %v", err)
		return err
	}
	defer stmt.Close()

	err = db.ExecAffectingOneRow(
		stmt,
		&m.RoleId, &m.Validity, &m.MaxLength, &m.MinLength, &m.Times, &m.FailedAttempts, &m.TimeUnlock, &m.Alpha, &m.Digits, &m.Special, &m.UpperCase, &m.LowerCase, &m.Enable,
		ID,
	)
	if err != nil {
		logger_trace.Error.Printf("ejecutando la consulta en Update password_policy: %v", err)
		return err
	}
	m.ID = ID

	return nil
}

func (o orcl) Delete(ID int64) error {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(orclDelete)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en Delete password_policy: %v", err)
		return err
	}
	defer stmt.Close()

	err = db.ExecAffectingOneRow(
		stmt,
		ID,
	)
	if err != nil {
		logger_trace.Error.Printf("ejecutando la consulta en Delete password_policy: %v", err)
		return err
	}

	return nil
}

func (o orcl) GetByID(ID int64) (*Model, error) {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(orclGetByID)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en GetByID password_policy: %v", err)
		return nil, err
	}
	defer stmt.Close()

	row := stmt.QueryRow(ID)
	return o.scanRow(row)
}

func (o orcl) GetAll() (Models, error) {
	conn := db.GetConnection()
	ms := make(Models, 0)

	stmt, err := conn.Prepare(orclGetAll)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en GetAll password_policy: %v", err)
		return nil, err
	}
	defer stmt.Close()

	rows, err := stmt.Query()
	if err != nil {
		logger_trace.Error.Printf("ejecutando la consulta en GetAll password_policy: %v", err)
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		m, err := o.scanRow(rows)
		if err != nil {
			logger_trace.Error.Printf("escaneando el registro en password_policy: %v", err)
			return ms, err
		}

		ms = append(ms, *m)
	}

	return ms, nil
}

func (o orcl) scanRow(rs db.RowScanner) (*Model, error) {
	m := &Model{}
	cn := pq.NullTime{}
	un := pq.NullTime{}

	err := rs.Scan(
		&m.ID,

		&m.RoleId,

		&m.Validity,

		&m.MaxLength,

		&m.MinLength,

		&m.Times,

		&m.FailedAttempts,

		&m.TimeUnlock,

		&m.Alpha,

		&m.Digits,

		&m.Special,

		&m.UpperCase,

		&m.LowerCase,

		&m.Enable,

		&cn,
		&un,
	)
	if err != nil {
		logger_trace.Error.Printf("escaneando el modelo password_policy: %v", err)
		return nil, err
	}

	m.CreatedAt = cn.Time
	m.UpdatedAt = un.Time
	return m, nil
}
