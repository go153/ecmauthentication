package password_policy

import (
	"database/sql"

	"github.com/lib/pq"
	"gitlab.com/e-capture/ecatch-ecm/majosystem/db"
	"gitlab.com/e-capture/ecatch-ecm/majosystem/logger_trace"
)

type sqlserver struct{}

const (
	sqlInsert      = `INSERT INTO usr.password_policy (role_id, validity, max_length, min_length, times, failed_attempts, time_unlock, alpha, digits, special, upper_case, lower_case, enable, inactivity_time, timeout) VALUES (@role_id, @validity, @max_length, @min_length, @times, @failed_attempts, @time_unlock, @alpha, @digits, @special, @upper_case, @lower_case, @enable, @inactivity_time, @timeout) SELECT ID = convert(bigint, SCOPE_IDENTITY()) `
	sqlUpdate      = `UPDATE usr.password_policy SET role_id = @role_id, validity = @validity, max_length = @max_length, min_length = @min_length, times = @times, failed_attempts = @failed_attempts, time_unlock = @time_unlock, alpha = @alpha, digits = @digits, special = @special, upper_case = @upper_case, lower_case = @lower_case, enable = @enable, inactivity_time = @inactivity_time, timeout = @timeout, updated_at = getdate() WHERE id = @id`
	sqlUpdateRol   = `UPDATE usr.password_policy SET  validity = @validity, max_length = @max_length, min_length = @min_length, times = @times, failed_attempts = @failed_attempts, time_unlock = @time_unlock, alpha = @alpha, digits = @digits, special = @special, upper_case = @upper_case, lower_case = @lower_case, enable = @enable, inactivity_time = @inactivity_time, timeout = @timeout, updated_at = getdate() WHERE role_id = @role_id`
	sqlDelete      = `DELETE FROM usr.password_policy WHERE id = @id`
	sqlGetByID     = `SELECT id, role_id, validity, max_length, min_length, times, failed_attempts, time_unlock, alpha, digits, special, upper_case, lower_case, enable, inactivity_time, timeout, created_at, updated_at FROM usr.password_policy WITH (NOLOCK) WHERE id = @id`
	sqlGetAll      = `SELECT id, role_id, validity, max_length, min_length, times, failed_attempts, time_unlock, alpha, digits, special, upper_case, lower_case, enable, inactivity_time, timeout, created_at, updated_at FROM usr.password_policy WITH (NOLOCK)`
	sqlGetByRoleID = sqlGetAll + " WHERE role_id = @roleid"
)

func (s sqlserver) Create(m *Model) error {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(sqlInsert)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en Create password_policy: %v", err)
		return err
	}
	defer stmt.Close()

	ID, err := db.ExecGettingID(
		stmt,
		sql.Named("role_id", m.RoleId),
		sql.Named("validity", m.Validity),
		sql.Named("max_length", m.MaxLength),
		sql.Named("min_length", m.MinLength),
		sql.Named("times", m.Times),
		sql.Named("failed_attempts", m.FailedAttempts),
		sql.Named("time_unlock", m.TimeUnlock),
		sql.Named("alpha", m.Alpha),
		sql.Named("digits", m.Digits),
		sql.Named("special", m.Special),
		sql.Named("upper_case", m.UpperCase),
		sql.Named("lower_case", m.LowerCase),
		sql.Named("enable", m.Enable),
		sql.Named("inactivity_time", m.InactivityTime),
		sql.Named("timeout", m.Timeout),
	)
	if err != nil {
		logger_trace.Error.Printf("ejecutando la consulta en Create password_policy: %v", err)
		return err
	}
	m.ID = ID

	return nil
}

func (s sqlserver) Update(ID int64, m *Model) error {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(sqlUpdate)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en Update password_policy: %v", err)
		return err
	}
	defer stmt.Close()

	err = db.ExecAffectingOneRow(
		stmt,
		sql.Named("role_id", m.RoleId),
		sql.Named("validity", m.Validity),
		sql.Named("max_length", m.MaxLength),
		sql.Named("min_length", m.MinLength),
		sql.Named("times", m.Times),
		sql.Named("failed_attempts", m.FailedAttempts),
		sql.Named("time_unlock", m.TimeUnlock),
		sql.Named("alpha", m.Alpha),
		sql.Named("digits", m.Digits),
		sql.Named("special", m.Special),
		sql.Named("upper_case", m.UpperCase),
		sql.Named("lower_case", m.LowerCase),
		sql.Named("enable", m.Enable),
		sql.Named("inactivity_time", m.InactivityTime),
		sql.Named("timeout", m.Timeout),
		sql.Named("id", ID),
	)
	if err != nil {
		logger_trace.Error.Printf("ejecutando la consulta en Update password_policy: %v", err)
		return err
	}
	m.ID = ID

	return nil
}

func (s sqlserver) Delete(ID int64) error {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(sqlDelete)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en Delete password_policy: %v", err)
		return err
	}
	defer stmt.Close()

	err = db.ExecAffectingOneRow(
		stmt,
		sql.Named("id", ID),
	)
	if err != nil {
		logger_trace.Error.Printf("ejecutando la consulta en Delete password_policy: %v", err)
		return err
	}

	return nil
}

func (s sqlserver) GetByID(ID int64) (*Model, error) {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(sqlGetByID)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en GetByID password_policy: %v", err)
		return nil, err
	}
	defer stmt.Close()

	row := stmt.QueryRow(sql.Named("id", ID))
	return s.scanRow(row)
}

func (s sqlserver) GetAll() (Models, error) {
	conn := db.GetConnection()
	ms := make(Models, 0)

	stmt, err := conn.Prepare(sqlGetAll)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en GetAll password_policy: %v", err)
		return nil, err
	}
	defer stmt.Close()

	rows, err := stmt.Query()
	if err != nil {
		logger_trace.Error.Printf("ejecutando la consulta en GetAll password_policy: %v", err)
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		m, err := s.scanRow(rows)
		if err != nil {
			logger_trace.Error.Printf("escaneando el registro en password_policy: %v", err)
			return ms, err
		}

		ms = append(ms, *m)
	}

	return ms, nil
}

func (s sqlserver) GetByRoleID(ID int64) (*Model, error) {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(sqlGetByRoleID)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en sqlGetByRoleID password_policy: %v", err)
		return nil, err
	}
	defer stmt.Close()

	row := stmt.QueryRow(sql.Named("roleid", ID))
	return s.scanRow(row)
}

func (s sqlserver) scanRow(rs db.RowScanner) (*Model, error) {
	m := &Model{}
	cn := pq.NullTime{}
	un := pq.NullTime{}

	err := rs.Scan(
		&m.ID,
		&m.RoleId,
		&m.Validity,
		&m.MaxLength,
		&m.MinLength,
		&m.Times,
		&m.FailedAttempts,
		&m.TimeUnlock,
		&m.Alpha,
		&m.Digits,
		&m.Special,
		&m.UpperCase,
		&m.LowerCase,
		&m.Enable,
		&m.InactivityTime,
		&m.Timeout,
		&cn,
		&un,
	)
	if err != nil {
		logger_trace.Error.Printf("escaneando el modelo password_policy: %v", err)
		return nil, err
	}

	m.CreatedAt = cn.Time
	m.UpdatedAt = un.Time
	return m, nil
}

func (s sqlserver) UpdateByRoleID(ID int64, m *Model) error {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(sqlUpdateRol)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en Update password_policy: %v", err)
		return err
	}
	defer stmt.Close()

	err = db.ExecAffectingOneRow(
		stmt,
		sql.Named("role_id", ID),
		sql.Named("validity", m.Validity),
		sql.Named("max_length", m.MaxLength),
		sql.Named("min_length", m.MinLength),
		sql.Named("times", m.Times),
		sql.Named("failed_attempts", m.FailedAttempts),
		sql.Named("time_unlock", m.TimeUnlock),
		sql.Named("alpha", m.Alpha),
		sql.Named("digits", m.Digits),
		sql.Named("special", m.Special),
		sql.Named("upper_case", m.UpperCase),
		sql.Named("lower_case", m.LowerCase),
		sql.Named("enable", m.Enable),
		sql.Named("inactivity_time", m.InactivityTime),
		sql.Named("timeout", m.Timeout),
	)
	if err != nil {
		logger_trace.Error.Printf("ejecutando la consulta en Update password_policy: %v", err)
		return err
	}
	m.ID = ID

	return nil
}
