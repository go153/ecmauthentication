package user

import (

	"time"

	"errors"

	"gitlab.com/e-capture/ecatch-ecm/majosystem/logger_database/usrloggeduser"
	"gitlab.com/e-capture/ecatch-ecm/majosystem/logger_trace"
	"golang.org/x/crypto/bcrypt"
)

// Model estructura de usuario
type Model struct {
	ID                     int64     `json:"id"`
	Name                   string    `json:"name"`
	Lastname               string    `json:"lastname"`
	Identification         string    `json:"identification"`
	Phonenumber            string    `json:"phonenumber"`
	Email                  string    `json:"email"`
	Password               string    `json:"password"`
	FailedAttempts         int       `json:"failed_attempts"`
	LastChangePassword     time.Time `json:"last_change_password"`
	ChangePassword         bool      `json:"change_password"`
	IsBlock                bool      `json:"is_block"`
	BlockDate              time.Time `json:"block_date"`
	IsDisabled             bool      `json:"is_disabled"`
	DisabledDate           time.Time `json:"disabled_date"`
	LastLogin              time.Time `json:"last_login"`
	RoleID                 int       `json:"role_id"`
	RoleName               string    `json:"rolename"`
	CreatedAt              time.Time `json:"created_at"`
	UpdatedAt              time.Time `json:"updated_at"`
	ChangePasswordDaysLeft int       `json:"change_password_days_left"` // Días restantes para cambio de contraseña (no está en la tabla)
	Timeout                int       `json:"timeout"`                   // Tiempo de inactividad de un usuario en minutos para una sesión activa (lo controla el front)
	MachineName            string    `json:"machine_name"`              // MachineName nombre de la máquina que está solicitando conectarse
	ClientID               int       `json:"client_id"`                 // ClientID identificador del cliente (navegador, app, desktop windows, desktop mac, etc)
	SessionID              string    `json:"session_id"`                // SessionID es el identificador único de la sesión
}

// Models slice of Model
type Models []Model

func (m *Model) Create() error {
	return s.Create(m)
}

func (m *Model) Update(id int64) error {
	return s.Update(m, id)
}

func (m *Model) Delete(id int64) error {
	return s.Delete(id)
}

func (m *Model) GetByID(id int64) (*Model, error) {
	return s.GetByID(id)
}

func (m *Model) GetAll() (Models, error) {
	return s.GetAll()
}

func (m *Model) GetByEmail(e string) (*Model, error) {
	return s.GetByEmail(e)
}

func (m *Model) Login() (*Model, error) {
	r, err := s.GetByEmail(m.Email)
	if err != nil {
		logger_trace.Error.Printf("consultando un usuario por email: %s, %v", m.Email, err)
		return nil, err
	}

	if !r.ComparePassword(m.Password) {
		return nil, errors.New("usuario o contraseña no válidos")
	}
	r.Password = ""

	return r, nil
}

// IsLogged valida la cantidad de veces que el usuario está logueado
func (m *Model) IsLogged() (int, error) {
	return s.IsLogged(m.ID)
}

func (m *Model) RegistryLogin() (int64, error) {
	return s.RegistryLogin(m.ID)
}

func (m *Model) Logout() error {
	return s.Logout(m.ID)
}

func (m *Model) GetUserConnect() (Models, error) {
	return s.GetUserConnect()
}

func (m *Model) DeleteUserConnect(id int64) error {
	return s.DeleteUserConnect(id)
}

func (m *Model) IfExistsIdentification(identification int64) (bool, error) {
	return s.IfExistsIdentification(identification)
}

func (m *Model) IfExistsEmail(email string) (bool, error) {
	return s.IfExistsEmail(email)
}

func (m *Model) EncryptPassword() string {
	bp, err := bcrypt.GenerateFromPassword([]byte(m.Password), bcrypt.DefaultCost)
	if err != nil {
		logger_trace.Error.Printf("generando el hash del password: %v", err)
	}
	return string(bp)
}

func (m *Model) EncryptNewPassword(p string) string {
	bp, err := bcrypt.GenerateFromPassword([]byte(p), bcrypt.DefaultCost)
	if err != nil {
		logger_trace.Error.Printf("generando el hash del password: %v", err)
	}
	return string(bp)
}

func (m *Model) ComparePassword(p string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(m.Password), []byte(p))
	if err != nil {
		logger_trace.Warning.Printf("la contraseña de %s no es válida: %v", m.Name, err)
		return false
	}

	return true
}

// GetFailedAttempts consulta la cantidad de intentos fallidos
func (m *Model) GetFailedAttempts() (int, error) {
	return s.GetFailedAttempts(m)
}

// FailedAttempt registra un intento fallido de login
func (m *Model) FailedAttempt() error {
	return s.FailedAttempt(m)
}

// FailedAttemptReset resetea la cantidad de intentos fallidos de login
func (m *Model) FailedAttemptReset() error {
	return s.FailedAttemptReset(m)
}

// Block bloquea un usuario
func (m *Model) Block() error {
	return s.Block(m.ID)
}

// Unblock desbloquea un usario
func (m *Model) Unblock() error {
	return s.Unblock(m.ID)
}

// ChangePassword marca al usuario para que lo obligue a cambiar contraseña
func (m *Model) SetChangePassword() error {
	return s.SetChangePassword(m.ID)
}

// Disable deshabilita un usuario de forma permanente
func (m *Model) Disable() error {
	return s.Disable(m.ID)
}

// Enable habilita un usuario que haya sido deshabilitado
func (m *Model) Enable() error {
	return s.Enable(m.ID)
}

// GetAllEnabled busca los usuarios que se encuentran habilitados
func (m *Model) GetAllEnabled() (Models, error) {
	return s.GetAllEnabled()
}

// SetLastLogin actualiza la fecha y hora del último login
func (m *Model) SetLastLogin() error {
	return s.SetLastLogin(m)
}

// ChangeNewPassword cambia el password del usuario, asigna un pasword random
func (m *Model) ChangeNewPassword() error {
	m.Password = m.EncryptPassword()
	return s.ChangeNewPassword(m)
}

func (m *Model) AdminChangePassword(p string) error {
	m.Password = m.EncryptNewPassword(p)
	return s.ChangeNewPassword(m)
}

// Estructura del modulo para un usuario
type Module struct {
	Module     string `json:"module"`
	Componente string `json:"componente"`
	Url        string `json:"url"`
	Class      string `json:"class"`
}

// Estructura de los modulos para un usuario
type Modules []Module

func (m *Model) GetModules() (*Modules, error) {
	return s.GetModules(m)
}

// Estructura para administrar passwords
type Password struct {
	Actual     string `json:"actual"`
	New        string `json:"new"`
	ConfirmNew string `json:"confirm_new"`
}

// Loginldap Valida si el usuario existe en ecatch
func (m *Model) Loginldap() (*Model, error) {
	r, err := s.GetByEmail(m.Email)
	if err != nil {
		logger_trace.Error.Printf("consultando un usuario por email: %s, %v", m.Email, err)
		return nil, err
	}

	return r, nil
}

func (m *Model) GetUserByQueue(QueueID int64) (Models, error) {
	return s.GetUserByQueue(QueueID)
}

type ModelRole struct {
	ID     int64  `json:"id"`
	RoleID int    `json:"role_id"`
	Route  string `json:"route"`
	Get    bool   `json:"get"`
	Post   bool   `json:"post"`
	Delete bool   `json:"delete"`
	Put    bool   `json:"put"`
}

type ModelRoles []ModelRole

func (m *ModelRole) GetByRouteAndRole() (*ModelRoles, error) {
	return s.GetByRouteAndRole(m)
}

func (m *Model) RegisterLoginLog(MachineName, ipReal, IPRemote, DomainUser string, evento int, userID int64) error {

	mdlLoggerUserLog := usrloggeduser.Model{}
	mdlLoggerUserLog.HostName = MachineName
	mdlLoggerUserLog.IpRequest = ipReal
	mdlLoggerUserLog.IpRemote = IPRemote
	mdlLoggerUserLog.UserID = userID
	mdlLoggerUserLog.Evento = evento
	mdlLoggerUserLog.DomainUser = DomainUser
	err := mdlLoggerUserLog.Create()
	if err != nil {
		logger_trace.Error.Printf("no se pudo registrar el usuario en mdlLoggerUserLog: %v", err)
		return err
	}
	return nil
}
