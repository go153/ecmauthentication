package user

import (
	"crypto/rsa"
	"database/sql"
	"errors"

	"io/ioutil"
	"net/http"
	"strings"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo"
	"gitlab.com/e-capture/ecatch-ecm/majosystem/configuration"
	"gitlab.com/e-capture/ecatch-ecm/majosystem/logger_trace"
	"gitlab.com/e-capture/ecatch-ecm/majosystem/response"
)

// Contenido de las llaves pública y privada
var (
	signKey   *rsa.PrivateKey
	verifyKey *rsa.PublicKey
)

// JWT personzalizado
type jwtCustomClaims struct {
	User      Model  `json:"user"`
	IPAddress string `json:"ip_address"`
	jwt.StandardClaims
}

// init lee los archivos de firma y validación RSA
func init() {
	c := configuration.FromFile()
	signBytes, err := ioutil.ReadFile(c.AppRSAPrivateKey)
	if err != nil {
		logger_trace.Error.Printf("leyendo el archivo privado de firma: %s", err)
	}

	signKey, err = jwt.ParseRSAPrivateKeyFromPEM(signBytes)
	if err != nil {
		logger_trace.Error.Printf("realizando el parse en jwt RSA private: %s", err)
	}

	verifyBytes, err := ioutil.ReadFile(c.AppRSAPublicKey)
	if err != nil {
		logger_trace.Error.Printf("leyendo el archivo público de confirmación: %s", err)
	}

	verifyKey, err = jwt.ParseRSAPublicKeyFromPEM(verifyBytes)
	if err != nil {
		logger_trace.Error.Printf("realizando el parse en jwt RSA public: %s", err)
	}
}

// Genera el token
func GenerateJWT(u Model, ipAddress string) (string, error) {
	c := &jwtCustomClaims{
		User:      u,
		IPAddress: ipAddress,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: time.Now().Add(time.Hour * 72).Unix(),
			Issuer:    "Ecatch",
		},
	}

	t := jwt.NewWithClaims(jwt.SigningMethodRS256, c)
	token, err := t.SignedString(signKey)
	if err != nil {
		logger_trace.Error.Printf("firmando el token: %v", err)
		return "", err
	}


	return token, nil
}

// Validación del token
func ValidateJWT(next echo.HandlerFunc) echo.HandlerFunc {
	r := response.Model{}

	return func(c echo.Context) error {
		t, err := getTokenFromHeader(c.Request())
		if err != nil {
			logger_trace.Warning.Printf("la petición no contiene el token en el encabezado: %v", err)
			r.Set(true, nil, 34)
			return c.JSON(http.StatusForbidden, r)
		}

		verifyFunction := func(token *jwt.Token) (interface{}, error) {
			return verifyKey, nil
		}

		token, err := jwt.ParseWithClaims(t, &jwtCustomClaims{}, verifyFunction)
		if err != nil {
			switch err.(type) {
			case *jwt.ValidationError:
				vErr := err.(*jwt.ValidationError)
				switch vErr.Errors {
				case jwt.ValidationErrorExpired:
					logger_trace.Warning.Printf("Token expirado: %v", err)
					r.Code = 35
				default:
					logger_trace.Warning.Printf("Error de validacion del token: %v", err)
					r.Code = 36
				}
			default:
				logger_trace.Warning.Printf("Error al procesar el token: %v", err)
				r.Code = 37
			}
			r.Error = true
			return c.JSON(http.StatusForbidden, r)
		}

		if !token.Valid {
			logger_trace.Warning.Printf("Token no Valido: %v", err)
			r.Set(true, nil, 38)
			return c.JSON(http.StatusForbidden, r)
		}
		// Variable de entorno, !!
		// *
		//

		if token.Claims.(*jwtCustomClaims).IPAddress != c.RealIP() {

			logger_trace.Warning.Printf("intento de validación de token con IP diferentes: origen: %s, validación: %s", token.Claims.(*jwtCustomClaims).IPAddress, c.RealIP())
			r.Set(true, nil, 38)
			return c.JSON(http.StatusForbidden, r)
		}

		// Set user to request
		c.Set("user", token.Claims.(*jwtCustomClaims).User)

		return next(c)
	}
}

// Busca el token en los header
func getTokenFromHeader(r *http.Request) (string, error) {
	ah := r.Header.Get("Authorization")
	if ah == "" {
		return "", errors.New("el encabezado no contiene la autorización")
	}

	// Should be a bearer token
	if len(ah) > 6 && strings.ToUpper(ah[0:6]) == "BEARER" {
		return ah[7:], nil
	}

	return "", errors.New("el header no contiene la palabra Bearer")
}

// getUserFromToken obtiene el usuario del token
func getUserFromToken(c echo.Context) Model {
	return c.Get("user").(Model)
}

// ValidatePermission valida los permisos del usuario de acuerdo al role y a la ruta
type Scope struct {
	Route string
}

func (s *Scope) ValidatePermissions(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		rm := response.Model{}
		u := c.Get("user").(Model)
		e := &ModelRole{}
		authorized := false

		e.Route = s.Route
		e.RoleID = u.RoleID
		er, err := e.GetByRouteAndRole()
		if err == sql.ErrNoRows {
			rm.Set(true, "no existen permisos para acceder al recurso", 38)
			return c.JSON(http.StatusForbidden, rm)
		}
		if err != nil {
			logger_trace.Error.Printf("no se pudo consultar los permisos para acceder al recurso")
			rm.Set(true, "no se pudieron consultar los permisos para el recurso. consulte con el adminstrador", 38)
			return c.JSON(http.StatusInternalServerError, rm)
		}

		switch c.Request().Method {
		case http.MethodGet:
			for _, k := range *er {
				if k.Get == true {
					authorized = true
					break
				}
			}
		case http.MethodPost:
			for _, k := range *er {
				if k.Post == true {
					authorized = true
					break
				}
			}
		case http.MethodPut:
			for _, k := range *er {
				if k.Put == true {
					authorized = true
					break
				}
			}
		case http.MethodDelete:
			for _, k := range *er {
				if k.Delete == true {
					authorized = true
					break
				}
			}
		}

		if !authorized {
			rm.Set(true, "no está autorizado a realizar esta acción en este recurso: ", 38)
			return c.JSON(http.StatusForbidden, rm)
		}

		// Si la validación es correcta
		return next(c)
	}
}

func ValidateJWTInternal(next echo.HandlerFunc) echo.HandlerFunc {
	r := response.Model{}

	return func(c echo.Context) error {
		t, err := getTokenFromHeader(c.Request())
		if err != nil {
			logger_trace.Warning.Printf("la petición no contiene el token en el encabezado: %v", err)
			r.Set(true, nil, 34)
			return c.JSON(http.StatusForbidden, r)
		}

		verifyFunction := func(token *jwt.Token) (interface{}, error) {
			return verifyKey, nil
		}

		token, err := jwt.ParseWithClaims(t, &jwtCustomClaims{}, verifyFunction)
		if err != nil {
			switch err.(type) {
			case *jwt.ValidationError:
				vErr := err.(*jwt.ValidationError)
				switch vErr.Errors {
				case jwt.ValidationErrorExpired:
					logger_trace.Warning.Printf("Token expirado: %v", err)
					r.Code = 35
				default:
					logger_trace.Warning.Printf("Error de validacion del token: %v", err)
					r.Code = 36
				}
			default:
				logger_trace.Warning.Printf("Error al procesar el token: %v", err)
				r.Code = 37
			}
			r.Error = true
			return c.JSON(http.StatusForbidden, r)
		}

		if !token.Valid {
			logger_trace.Warning.Printf("Token no Valido: %v", err)
			r.Set(true, nil, 38)
			return c.JSON(http.StatusForbidden, r)
		}
		// Set user to request
		c.Set("user", token.Claims.(*jwtCustomClaims).User)

		return next(c)
	}
}
