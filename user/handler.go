package user

import (
	"errors"
	"fmt"
	"net/http"
	"strings"

	"strconv"

	"time"

	"database/sql"

	"github.com/estebanbacl/go-openssl"
	"github.com/labstack/echo"
	satoriuuid "github.com/satori/go.uuid"

	"gitlab.com/e-capture/ecatch-ecm/ecmauthentication/dates_disallowed"
	"gitlab.com/e-capture/ecatch-ecm/ecmauthentication/password_policy"

	"gitlab.com/e-capture/ecatch-ecm/ecmauthentication/role"
	"gitlab.com/e-capture/ecatch-ecm/ecmauthentication/white_list_ip"
	"gitlab.com/e-capture/ecatch-ecm/majosystem/configuration"
	"gitlab.com/e-capture/ecatch-ecm/majosystem/logger_trace"
	"gitlab.com/e-capture/ecatch-ecm/majosystem/response"
	ld "gopkg.in/ldap.v2"
)

// Create handler user
func Create(c echo.Context) error {
	rm := response.Model{}
	m := Model{}

	err := c.Bind(&m)
	if err != nil {
		errStr := fmt.Sprintf("la estructura no es válida: %v", err)
		logger_trace.Error.Printf(errStr)
		rm.Set(true, nil, 1)
		return c.JSON(http.StatusAccepted, rm)
	}
	//c.Set("bodyRequest", m)

	err = m.Create()
	if err != nil {
		errStr := fmt.Sprintf("no se pudo crear el registro: %v", err)
		logger_trace.Error.Printf(errStr)
		rm.Set(true, nil, 3)
		return c.JSON(http.StatusAccepted, rm)
	}

	rm.Set(false, m, 4)
	return c.JSON(http.StatusCreated, rm)
}

func CreateWithoutToken(c echo.Context) error {

	rm := response.Model{}
	m := Model{}

	err := c.Bind(&m)
	if err != nil {
		errStr := fmt.Sprintf("la estructura no es válida: %v", err)
		logger_trace.Error.Printf(errStr)
		rm.Set(true, nil, 1)
		return c.JSON(http.StatusAccepted, rm)
	}
	//c.Set("bodyRequest", m)

	err = m.Create()
	if err != nil {

		errStr := fmt.Sprintf("no se pudo crear el registro: %v", err)
		logger_trace.Error.Printf(errStr)
		rm.Set(true, nil, 3)
		return c.JSON(http.StatusAccepted, rm)
	}

	rm.Set(false, m, 4)
	return c.JSON(http.StatusCreated, rm)
}

// Update handler user
func Update(c echo.Context) error {
	rm := response.Model{}
	m := Model{}

	//  id := c.Get("user").(Model).ID
	id, err := strconv.ParseInt(c.Param("id"), 10, 64)

	err = c.Bind(&m)
	if err != nil {
		errStr := fmt.Sprintf("la estructura no es válida: %v", err)
		logger_trace.Error.Printf(errStr)
		rm.Set(true, nil, 1)
		return c.JSON(http.StatusAccepted, rm)
	}

	err = m.Update(id)
	if err != nil {
		errStr := fmt.Sprintf("no se pudo actualizar el registro: %v", err)
		logger_trace.Error.Printf(errStr)
		rm.Set(true, nil, 18)
		return c.JSON(http.StatusAccepted, rm)
	}

	rm.Set(false, nil, 19)
	return c.JSON(http.StatusOK, rm)
}

// Delete handler user
func Delete(c echo.Context) error {
	rm := response.Model{}
	m := Model{}

	//i d := c.Get("user").(Model).ID
	id, err := strconv.ParseInt(c.Param("id"), 10, 64)

	err = m.Delete(id)
	if err != nil {
		errStr := fmt.Sprintf("no se pudo eliminar el registro: %v", err)
		logger_trace.Error.Printf(errStr)
		rm.Set(true, nil, 20)
		return c.JSON(http.StatusAccepted, rm)
	}

	rm.Set(false, nil, 28)
	return c.JSON(http.StatusOK, rm)
}

// GetByID handler user
func GetByID(c echo.Context) error {
	rm := response.Model{}
	m := Model{}

	// uID := c.Get("user").(Model).ID

	uID, err := strconv.ParseInt(c.Param("id"), 10, 64)

	r, err := m.GetByID(uID)
	if err != nil {
		errStr := fmt.Sprintf("no se pudo consultar el registro: %v", err)
		logger_trace.Error.Printf(errStr)
		rm.Set(true, nil, 22)
		return c.JSON(http.StatusAccepted, rm)
	}

	rm.Set(false, r, 29)
	return c.JSON(http.StatusOK, rm)
}

// IfExistsIdentification handler user
func IfExistsIdentification(c echo.Context) error {
	rm := response.Model{}
	m := Model{}

	identification, err := strconv.ParseInt(c.Param("identification"), 10, 64)
	if err != nil {
		errStr := fmt.Sprintf("la identificación debe ser numero: %v", err)
		logger_trace.Error.Printf(errStr)
		rm.Set(true, nil, 17)
		return c.JSON(http.StatusAccepted, rm)
	}

	r, err := m.IfExistsIdentification(identification)
	if err != nil {
		errStr := fmt.Sprintf("no se pudo consultar el registro: %v", err)
		logger_trace.Error.Printf(errStr)
		rm.Set(true, nil, 22)
		return c.JSON(http.StatusAccepted, rm)
	}

	rm.Set(false, map[string]interface{}{"identification": r}, 30)

	//rm.Set(false, r, 29)
	return c.JSON(http.StatusOK, rm)
}

// IfExistsEmail handler user
func IfExistsEmail(c echo.Context) error {
	rm := response.Model{}
	m := Model{}

	email := c.Param("email")

	r, err := m.IfExistsEmail(email)
	if err != nil {
		errStr := fmt.Sprintf("no se pudo consultar el registro: %v", err)
		logger_trace.Error.Printf(errStr)
		rm.Set(true, nil, 22)
		return c.JSON(http.StatusAccepted, rm)
	}

	rm.Set(false, map[string]interface{}{"email": r}, 30)

	//rm.Set(false, r, 29)
	return c.JSON(http.StatusOK, rm)
}

// GetAll handler user
func GetAll(c echo.Context) error {
	rm := response.Model{}
	m := Model{}

	rs, err := m.GetAll()
	if err != nil {
		errStr := fmt.Sprintf("no se pudo consultar los registros: %v", err)
		logger_trace.Error.Printf(errStr)
		rm.Set(true, nil, 22)
		return c.JSON(http.StatusAccepted, rm)
	}

	rm.Set(false, rs, 29)
	return c.JSON(http.StatusOK, rm)
}

// Login handler de usuario
func Login(c echo.Context) error {
	var timeOut int
	rm := response.Model{}
	u := Model{}
	pp := &password_policy.Model{}
	ro := &role.Model{}
	dd := &dates_disallowed.Model{}
	wl := &white_list_ip.Model{}
	cnf := configuration.FromFile()
	user := &Model{}

	err := c.Bind(&u)
	if err != nil {
		errStr := fmt.Sprintf("la estructura no es válida: %v", err)
		logger_trace.Error.Printf(errStr)
		rm.Set(true, nil, 1)
		return c.JSON(http.StatusAccepted, rm)
	}

	// u.ClientID 2 Peticiones realizas por el front end
	if u.ClientID == 2 {
		u.Email = DecryptData(u.Email)
		u.Password = DecryptData(u.Password)
	}
	// logger response
	//  c.Set("bodyRequest", u)

	// Valida si se requiere permite el ingreso al sistema por listas blancas de IP
	if strings.ToLower(cnf.AppValidateIp) == "t" {
		wl.Ip = c.RealIP()
		wl, err = wl.GetByIP()
		if err == sql.ErrNoRows {
			logger_trace.Warning.Printf("intento de ingreso desde una IP no autorizada: Usuario: %s, IP: %s", u.Email, c.RealIP())
			rm.Set(true, nil, 91)
			return c.JSON(http.StatusAccepted, rm)
		}
		if err != nil {
			logger_trace.Error.Printf("no fue posible consultar la información de IP de lista blanca: %v", err)
			rm.Set(true, nil, 92)
			return c.JSON(http.StatusAccepted, rm)
		}
	}
	// valida si requiere autenticacion por ldap
	if strings.ToLower(cnf.Ldap) == "t" {
		var groupvalid bool
		var username, bindusername, bindpassword string
		if strings.ToLower(cnf.LdapSSO) == "t" {
			username = strings.Split(user.Email, "@")[0]
			bindusername = cnf.LdapUsernameSSO
			bindpassword = cnf.LdapPasswordSSO
		} else {
			username = strings.Split(user.Email, "@")[0]
			bindusername = strings.Split(user.Email, "@")[0]
			bindpassword = user.Password
		}
		groups, err := autenticationldap(username, bindusername, bindpassword)

		if err != nil {
			logger_trace.Error.Printf("no fue posible consultar la información de grupos en LDAP: %v", err)
			rm.Set(true, nil, 103)
			return c.JSON(http.StatusAccepted, rm)
		}
		user, err = u.Loginldap()
		for _, k := range groups {
			if strings.ToLower(k) == strings.ToLower(user.RoleName) {
				groupvalid = true
			}
		}
		if !groupvalid {
			logger_trace.Error.Printf("el usuario no pertenece a ningun role: %v", err)
			rm.Set(true, nil, 92)
			return c.JSON(http.StatusAccepted, rm)
		}
	} else {
		user, err = u.Login()
	}

	// Valida el login del usuario
	if err != nil {
		logger_trace.Error.Printf("intentando loguearse: %v", err)
		// Registra conexiones existosas
		err = u.RegisterLoginLog("", c.RealIP(), c.RealIP(), u.Email, 3, 0)
		if err != nil {
			logger_trace.Error.Printf("no se pudo registrar el usuario en mdlLoggerUserLog: %v", err)
		}
		r, err := u.GetByEmail(u.Email)
		if err != nil {
			logger_trace.Error.Printf("consultando el usuario por el email: %s, %v", u.Email, err)
		}

		err = r.FailedAttempt()
		if err != nil {
			logger_trace.Error.Printf("intentando registrar el login fallido de: %s: %v", u.Email, err)
		}

		fac, err := r.GetFailedAttempts()
		if err != nil {
			logger_trace.Error.Printf("intentando obtener los intentos de login fallidos de: %s: %v", u.Email, err)
		}

		// Si supera la cantidad de intentos fallidos, se bloquea el usuario.
		pp, err := pp.GetByRoleID(int64(r.RoleID))
		if err != nil {
			logger_trace.Error.Printf("no se pudo obtener la política de contraseñas: %v", err)
			rm.Set(true, nil, 70)
			return c.JSON(http.StatusAccepted, rm)
		}
		if pp.Enable {
			if fac >= pp.FailedAttempts {
				r.Block()
			}
		}

		rm.Set(true, nil, 10)
		return c.JSON(http.StatusAccepted, rm)
	}

	// Le asigna el client_id al usuario consultado, desde el usuario recibido
	// con el fin de conocer el id del aplicativo que usó para conectarse
	user.ClientID = u.ClientID

	// Le asigna el nombre de la máquina desde el usuario recibido.
	user.MachineName = u.MachineName

	// Busca si el role está deshabilitado por fechas
	dd.RoleId = user.RoleID
	dd, err = dd.GetByDateBetween()
	if err != nil && err != sql.ErrNoRows {
		logger_trace.Error.Printf("no se pudo obtener las fechas no permitidas de login: %v", err)
		rm.Set(true, nil, 89)
		return c.JSON(http.StatusAccepted, rm)
	}
	if dd != nil {
		logger_trace.Warning.Printf("intento de login en una fecha no válida: %d, %s: %v", user.ID, user.Email, err)
		rm.Set(true, dd, 90)
		return c.JSON(http.StatusAccepted, rm)
	}

	// Se busca el Role del usuario para saber cuantas veces le hes permitido tener sesiones activas
	ro, err = ro.GetByID(user.RoleID)
	if err != nil {
		logger_trace.Error.Printf("no fue posible consultar el role del usuario en el Login: %d, %s: %v", user.ID, user.Email, err)
		rm.Set(true, nil, 87)
		return c.JSON(http.StatusAccepted, rm)
	}

	// Obtiene la cantidad de veces que el usuario está logueado.
	// No aplica para el usuario con ID 1
	if user.ID != 1 {
		cl, err := user.IsLogged()
		if err != nil {
			logger_trace.Error.Printf("buscando la cantidad de veces logueado del usuario: %d, %s: %v", user.ID, user.Email, err)
			rm.Set(true, nil, 32)
			return c.JSON(http.StatusAccepted, rm)
		}

		if ro.SessionsAllowed < cl {
			logger_trace.Warning.Printf("el usuario: %d, %s está superando la cantidad de sesiones permitidas: Permitidas = %d, logueado = %d:", user.ID, user.Email, ro.SessionsAllowed, cl)
			rm.Set(true, nil, 88)
			return c.JSON(http.StatusAccepted, rm)
		}
	}

	// Obtiene la parametrización de políticas de contraseñas
	pp, err = pp.GetByRoleID(int64(user.RoleID))
	if err == sql.ErrNoRows {
		pp = &password_policy.Model{}
		pp.Enable = false
	}
	if err != nil && err != sql.ErrNoRows {
		logger_trace.Error.Printf("no se pudo obtener la política de contraseñas: %v", err)
		rm.Set(true, nil, 70)
		return c.JSON(http.StatusAccepted, rm)
	}
	timeOut = pp.Timeout
	if pp.Enable {
		// Si el usuario está bloqueado se valida si ya cumplió el tiempo de bloqueo
		userUnblock(pp, user)

		// Valida si el usuario debe cambiar la contraseña
		mustChangePassword(pp, user)

		// Días restantes para el cambio de contraseña
		changePasswordDaysLeft(pp, user)

		user.Timeout = pp.Timeout
	}

	if user.IsBlock {
		rm.Set(true, nil, 69)
		return c.JSON(http.StatusAccepted, rm)
	}

	_, err = user.RegistryLogin()
	if err != nil {
		logger_trace.Error.Printf("no se pudo registrar el usuario en usr_loggedusers: %v", err)
		rm.Set(true, nil, 25)
		return c.JSON(http.StatusAccepted, rm)
	}

	// Registra conexiones existosas
	err = u.RegisterLoginLog(u.MachineName, c.RealIP(), c.RealIP(), user.Email, 1, user.ID)
	if err != nil {
		logger_trace.Error.Printf("no se pudo registrar el usuario en mdlLoggerUserLog: %v", err)
	}

	err = user.SetLastLogin()
	if err != nil {
		logger_trace.Error.Printf("no se pudo actualizar la fecha de último login del usario: %s: %v", user.Email, err)
	}

	modules, err := user.GetModules()
	if err != nil {
		logger_trace.Error.Printf("obteniendo los modulos del usuario: %v", err)
		rm.Set(true, nil, 26)
		return c.JSON(http.StatusAccepted, rm)
	}

	user.Password = ""
	jwtID := satoriuuid.NewV4()

	user.SessionID = jwtID.String()

	token, err := GenerateJWT(*user, c.RealIP())
	if err != nil {
		logger_trace.Error.Printf("generando el token del usuario: %v", err)
		rm.Set(true, nil, 33)
		return c.JSON(http.StatusAccepted, rm)
	}

	// Set user to request
	c.Set("user", *user)
	rm.Set(false, map[string]interface{}{"token": token, "modules": modules, "session_id": user.SessionID, "time_out": timeOut}, 30)
	return c.JSON(http.StatusOK, rm)
}

// Logout handler de usuario
func Logout(c echo.Context) error {
	rm := response.Model{}
	u := getUserFromToken(c)

	// Registra Logout
	err := u.RegisterLoginLog(u.MachineName, c.RealIP(), c.RealIP(), u.Email, 2, u.ID)
	if err != nil {
		logger_trace.Error.Printf("no se pudo registrar el usuario en mdlLoggerUserLog: %v", err)
	}

	err = u.Logout()
	if err != nil {
		logger_trace.Error.Printf("intentando ejecutar logout en el handler: %v", err)
		rm.Set(true, nil, 31)
		return c.JSON(http.StatusAccepted, rm)
	}

	rm.Set(false, nil, 29)
	return c.JSON(http.StatusOK, rm)
}

// GetUserConnect handler de usuario
func GetUserConnect(c echo.Context) error {
	rm := response.Model{}
	m := Model{}

	rs, err := m.GetUserConnect()
	if err != nil {
		errStr := fmt.Sprintf("no se pudo consultar los registros: %v", err)
		logger_trace.Error.Printf(errStr)
		rm.Set(true, nil, 22)
		return c.JSON(http.StatusAccepted, rm)
	}

	rm.Set(false, rs, 29)
	return c.JSON(http.StatusOK, rm)
}

// Delete handler userConnected
func DeleteUserConnect(c echo.Context) error {
	rm := response.Model{}
	m := Model{}

	id, err := strconv.ParseInt(c.Param("id"), 10, 64)
	if err != nil {
		errStr := fmt.Sprintf("el id debe ser numérico: %v", err)
		logger_trace.Error.Printf(errStr)
		rm.Set(true, nil, 17)
		return c.JSON(http.StatusAccepted, rm)
	}

	err = m.DeleteUserConnect(id)
	if err != nil {
		errStr := fmt.Sprintf("no se pudo eliminar el registro: %v", err)
		logger_trace.Error.Printf(errStr)
		rm.Set(true, nil, 20)
		return c.JSON(http.StatusAccepted, rm)
	}

	rm.Set(false, nil, 28)
	return c.JSON(http.StatusOK, rm)
}

// Delete handler userConnected
func GetModules(c echo.Context) error {
	rm := response.Model{}
	m := Model{}
	userID := c.Get("user").(Model).ID
	m.ID = userID
	m.Email = c.Get("user").(Model).Email
	modules, err := m.GetModules()
	if err != nil {
		logger_trace.Error.Printf("obteniendo los modulos del usuario: %v", err)
		rm.Set(true, nil, 26)
		return c.JSON(http.StatusAccepted, rm)
	}

	rm.Set(false, map[string]interface{}{"modules": modules}, 29)
	return c.JSON(http.StatusOK, rm)
}

func changePasswordDaysLeft(pp *password_policy.Model, u *Model) {
	if pp.Validity > 0 {
		diff := time.Now().Sub(u.LastChangePassword.Add(time.Duration(pp.Validity)))
		u.ChangePasswordDaysLeft = int(diff.Hours() / 24)
		return
	}

	u.ChangePasswordDaysLeft = -1
}

// Valida si el usuario debe cambiar la contraseña y se marca como true
// el frontend leerá dicho campo y lo enviará a la pantalla correspondiente.
// Aplica para la primera vez que ingresa al sistema.
func mustChangePassword(pp *password_policy.Model, u *Model) {
	if time.Now().After(u.LastChangePassword.Add(time.Duration(pp.Validity) * time.Hour * 24)) {
		err := u.SetChangePassword()
		if err != nil {
			logger_trace.Error.Printf("no se pudo actualizar el campo change_password del usuario: %s, %v", u.Email, err)
		}
		u.ChangePassword = true
	}
}

// Valida si el usuario está bloqueado
// y también desbloquea el usuario si cumple el tiempo temporal de bloqueo
func userUnblock(pp *password_policy.Model, u *Model) {
	if u.IsBlock {
		// Si tiene tiempo de bloqueo, valida el tiempo y desbloquea
		if pp.TimeUnlock > 0 {
			if time.Now().After(u.BlockDate.Add(time.Duration(pp.TimeUnlock) * time.Minute)) {
				err := u.Unblock()
				if err != nil {
					logger_trace.Error.Printf("no se pudo desbloquear el usuario: %s, %v", u.Email, err)
				}
				u.IsBlock = false
			}
		}
	}
}

func autenticationldap(username, bindusername, bindpassword string) ([]string, error) {

	cnf := configuration.FromFile()
	bindpassword = fmt.Sprintf("%s@%s", bindpassword, cnf.LdapDomain)
	var groups []string
	// conecta al directorio activo
	l, err := ld.Dial("tcp", fmt.Sprintf(`"%s":%s`, cnf.LdapServer, cnf.LdapPort))
	if err != nil {
		logger_trace.Error.Printf("No se pudo conectar al Active Directory: %v", err)
		return groups, err
	}
	defer l.Close()

	// Autentica usuario
	err = l.Bind(bindusername, bindpassword)
	if err != nil {
		logger_trace.Error.Printf("No se pudo autenticar al Active Directory: %v", err)
		return groups, err
	}
	domain := strings.Split(cnf.LdapDomain, ".")
	// Busqueda de un usuario por su account name
	searchRequest := ld.NewSearchRequest(
		fmt.Sprintf(`dc=%s,dc=%s`, domain[0], domain[1]),
		ld.ScopeWholeSubtree, ld.NeverDerefAliases, 0, 0, false,
		fmt.Sprintf(`(&(objectClass=*)(sAMAccountName=%s))`, username),
		[]string{"memberOf"},
		nil,
	)

	sr, err := l.Search(searchRequest)
	if err != nil {
		logger_trace.Error.Printf("No se pudo buscar el usuario en Active Directory: %v", err)
		return groups, err
	}

	if len(sr.Entries) != 1 {
		err = errors.New("No existe informacion de usuario en Active Directory")
		logger_trace.Error.Printf("%v", err)
		return groups, err
	}

	for _, v := range sr.Entries {
		for _, k := range v.Attributes {
			for _, j := range k.Values {
				cn := strings.Split(j, ",")
				cnstr := strings.Replace(cn[0], "CN=", "", -1)
				groups = append(groups, cnstr)
			}
		}
	}
	return groups, nil
}

// GetByID handler user
func GetUsersByQueue(c echo.Context) error {
	rm := response.Model{}
	m := Model{}

	QID, err := strconv.ParseInt(c.Param("id"), 10, 64)

	r, err := m.GetUserByQueue(QID)
	if err != nil {
		errStr := fmt.Sprintf("no se pudo consultar el registro: %v", err)
		logger_trace.Error.Printf(errStr)
		rm.Set(true, nil, 22)
		return c.JSON(http.StatusAccepted, rm)
	}

	rm.Set(false, r, 29)
	return c.JSON(http.StatusOK, rm)
}

// Desencriptar valores del payload

func DecryptData(encrypted string) string {

	secret := "394812730425442A472D2F423F452848"

	o := openssl.New()
	dec, err := o.DecryptBytes(secret, []byte(encrypted))
	if err != nil {
		logger_trace.Error.Printf("No es posible decodificar el mensaje: %s\n", err)
	}

	return string(dec)
}
