package user

import (
	"database/sql"
	"strconv"

	"github.com/lib/pq"
	"gitlab.com/e-capture/ecatch-ecm/majosystem/db"
	"gitlab.com/e-capture/ecatch-ecm/majosystem/logger_trace"
)

// sqlserver estructura de DAO postgres
type sqlserver struct{}

const (
	sqlserverInsert  = `INSERT INTO usr.users (name, lastname, identification, phonenumber, email,  password, role_id) VALUES (@name, @lastname, @identification, @phonenumber, @email, @password, @roleid)`
	sqlserverUpdate  = `UPDATE usr.users SET name = @name, lastname = @lastname, identification = @identification, phonenumber = @phonenumber, email = @email, role_id = @roleid WHERE id = @id`
	sqlserverDelete  = `UPDATE usr.users SET is_block = 1, is_disabled = 1, WHERE id = @id`
	sqlserverSelect  = `SELECT u.id, u.name, u.lastname, u.identification, u.phonenumber, u.email, u.failed_attempts, u.last_change_password, u.change_password, u.is_block, u.block_date, u.is_disabled, u.disabled_date, u.role_id, r.name as rolename FROM usr.users u INNER JOIN usr.roles r ON (r.id = u.role_id)`
	sqlserverGetByID = sqlserverSelect + ` WHERE u.id = @id`
	sqlserverGetAll  = sqlserverSelect + ` `

	// sqlserverGetByEmail no usa la constante psqlSelect debido a que se requiere el password
	sqlserverGetByEmail         = `SELECT id, name, lastname, identification, phonenumber, email,  password, failed_attempts, last_change_password, change_password, is_block, block_date, is_disabled, disabled_date, role_id FROM usr.users WHERE email = @email`
	sqlserverIsLogged           = `SELECT count(*) as cantidad FROM usr.loggedusers WHERE user_id = @id`
	sqlserverRegistryLogin      = `INSERT INTO usr.loggedusers (user_id) VALUES (@userid)  SELECT ID = convert(bigint, SCOPE_IDENTITY()) `
	sqlserverLogout             = `DELETE FROM usr.loggedusers WHERE user_id = @id`
	sqlserverGetUsrConnected    = `SELECT logged.id, usr.name, usr.lastname, usr.identification, usr.phonenumber, usr.email, role.id role_id, role.name rolename  FROM usr.loggedusers logged INNER JOIN usr.users usr ON (usr.id = logged.user_id) INNER JOIN usr.roles role ON (usr.role_id = role.id)`
	sqlserverDeleteUsrConnected = `DELETE FROM usr.loggedusers WHERE id = @id`
	sqlserverGetModules         = `SELECT DISTINCT m.name module,c.name componente, c.url_front url, c.class class FROM    usr.modules m WITH (NOLOCK) INNER JOIN usr.components c WITH (NOLOCK) ON (m.id= c.module_id) INNER JOIN usr.elements e WITH (NOLOCK) ON (e.component_id = c.id) INNER JOIN usr.role_usr_elements rue WITH (NOLOCK) ON (e.id = rue.element_id) INNER JOIN usr.users u WITH (NOLOCK) ON (rue.role_id = u.role_id)	WHERE e.name = 'load' and u.email = @email`

	// Verificación de existencia de un determinado dato de usuario
	sqlserverIdentification     = `SELECT COUNT(*) FROM usr.users WHERE identification = @identification`
	sqlserverEmail              = `SELECT COUNT(*) FROM usr.users WHERE email = @email`
	sqlserverGetFailedAttempts  = `SELECT failed_attempts FROM usr.users WHERE email = @email`
	sqlserverFailedAttempt      = `UPDATE usr.users SET failed_attempts = failed_attempts + 1 WHERE email = @email`
	sqlserverFailedAttemptReset = `UPDATE usr.users SET failed_attempts = 0 WHERE email = @email`
	sqlserverBlock              = `UPDATE usr.users SET is_block = 1, block_date = getdate() WHERE id = @id`
	sqlserverUnblock            = `UPDATE usr.users SET is_block = 0, block_date = null WHERE id = @id`
	sqlserverSetChangePassword  = `UPDATE usr.users SET change_password = 1 WHERE id = @id`
	sqlserverDisable            = `UPDATE usr.users SET is_disabled = 1, disabled_date = getdate() WHERE id = @id`
	sqlserverEnable             = `UPDATE usr.users SET is_disabled = 0, disabled_date = getdate() WHERE id = @id`
	sqlserverGetAllEnabled      = `SELECT id, role_id, last_login FROM usr.users WHERE is_disabled = 0`
	sqlserverSetLastLogin       = `UPDATE usr.users SET last_login = getdate() WHERE id = @id`
	sqlserverChangeNewPassword  = `UPDATE usr.users SET password = @password WHERE id = @id`
	sqlserverGetUserByQueue     = `SELECT u.id, u.name, u.lastname, u.identification, u.phonenumber, u.email, u.password, u.failed_attempts, u.last_change_password, u.change_password, u.is_block, u.block_date, u.is_disabled, u.disabled_date, u.role_id,r.name as rolename  FROM wf.queues_usr_roles qr WITH (NOLOCK) INNER JOIN usr.users u WITH (NOLOCK) ON (u.role_id = qr.role_id) INNER JOIN usr.roles r WITH (NOLOCK) ON (r.id = u.role_id) WHERE queues_id = queue_id ORDER BY email`
	sqlserverGetByRouteAndRole  = `SELECT  e.id, url_back, ur.role_id, e.get_value, e.post, e.del, e.put FROM usr.elements e WITH (NOLOCK)  INNER JOIN  usr.role_usr_elements ur WITH (NOLOCK) ON (e.id = ur.element_id) WHERE url_back = @route and ur.role_id = @role`
)

// Create registra en la bd
func (p sqlserver) Create(m *Model) error {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(sqlserverInsert)
	if err != nil {
		logger_trace.Error.Printf("preparando create user: %v", err)
		return err
	}
	defer stmt.Close()

	err = db.Execute(
		stmt,
		sql.Named("name", m.Name),
		sql.Named("lastname", m.Lastname),
		sql.Named("identification", m.Identification),
		sql.Named("phonenumber", m.Phonenumber),
		sql.Named("email", m.Email),
		sql.Named("password", m.EncryptPassword()),
		sql.Named("roleid", m.RoleID),
	)
	if err != nil {
		logger_trace.Error.Printf("ejecutando create user: %v", err)
		return err
	}

	return nil
}

// Update actualiza un registro
func (p sqlserver) Update(m *Model, id int64) error {
	conn := db.GetConnection()
	stmt, err := conn.Prepare(sqlserverUpdate)
	if err != nil {
		logger_trace.Error.Printf("preparando update user: %v", err)
		return err
	}
	defer stmt.Close()

	err = db.ExecAffectingOneRow(
		stmt,
		sql.Named("name", m.Name),
		sql.Named("lastname", m.Lastname),
		sql.Named("identification", m.Identification),
		sql.Named("phonenumber", m.Phonenumber),
		sql.Named("email", m.Email),
		sql.Named("roleid", m.RoleID),
		sql.Named("id", id),
	)
	if err != nil {
		logger_trace.Error.Printf("ejecutando update user: %v", err)
		return err
	}

	return nil
}

// Delete elimina un registro por id
func (p sqlserver) Delete(id int64) error {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(sqlserverDelete)
	if err != nil {
		logger_trace.Error.Printf("preparando delete user: %v", err)
		return err
	}
	defer stmt.Close()

	err = db.ExecAffectingOneRow(stmt, sql.Named("id", id))
	if err != nil {
		logger_trace.Error.Printf("ejecutando delete user: %v", err)
		return err
	}

	return nil
}

// GetByID obtiene un registro por id
func (p sqlserver) GetByID(id int64) (*Model, error) {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(sqlserverGetByID)
	if err != nil {
		logger_trace.Error.Printf("preparando getbyid user: %v", err)
		return nil, err
	}
	defer stmt.Close()

	row := stmt.QueryRow(sql.Named("id", id))
	return p.scanRow(row)
}

// GetAll obtiene toedos los registros
func (p sqlserver) GetAll() (Models, error) {
	ms := Models{}
	conn := db.GetConnection()

	stmt, err := conn.Prepare(sqlserverGetAll)
	if err != nil {
		logger_trace.Error.Printf("preparando GetAll user: %v", err)
		return nil, err
	}
	defer stmt.Close()

	rows, err := stmt.Query()
	if err != nil {
		logger_trace.Error.Printf("ejecutando GetAll user: %v", err)
		return ms, err
	}
	defer rows.Close()

	for rows.Next() {
		m, err := p.scanRow(rows)
		if err != nil {
			logger_trace.Error.Printf("escaneando GetAll user: %v", err)
			return ms, err
		}

		ms = append(ms, *m)
	}

	return ms, nil
}

// scanRow escanea un registro
func (p sqlserver) scanRow(s db.RowScanner) (*Model, error) {
	m := &Model{}
	lcp := pq.NullTime{}
	bd := pq.NullTime{}
	dd := pq.NullTime{}

	err := s.Scan(
		&m.ID,
		&m.Name,
		&m.Lastname,
		&m.Identification,
		&m.Phonenumber,
		&m.Email,
		&m.FailedAttempts,
		&lcp,
		&m.ChangePassword,
		&m.IsBlock,
		&bd,
		&m.IsDisabled,
		&dd,
		&m.RoleID,
		&m.RoleName,
	)
	if err != nil {
		logger_trace.Error.Printf("escaneando en scanRow user: %v", err)
		return nil, err
	}

	return m, nil
}

// GetByEmail valida el login de usuario
func (p sqlserver) GetByEmail(email string) (*Model, error) {
	conn := db.GetConnection()
	m := &Model{}
	lcp := pq.NullTime{}
	bd := pq.NullTime{}
	dd := pq.NullTime{}

	stmt, err := conn.Prepare(sqlserverGetByEmail)
	if err != nil {
		logger_trace.Error.Printf("preparando sqlserverGetByEmail: %s, %v", email, err)
		return nil, err
	}
	defer stmt.Close()

	// No se usa p.scan(row) debido a que se requiere el password
	row := stmt.QueryRow(sql.Named("email", email))
	err = row.Scan(
		&m.ID,
		&m.Name,
		&m.Lastname,
		&m.Identification,
		&m.Phonenumber,
		&m.Email,
		&m.Password,
		&m.FailedAttempts,
		&lcp,
		&m.ChangePassword,
		&m.IsBlock,
		&bd,
		&m.IsDisabled,
		&dd,
		&m.RoleID,
	)
	if err != nil {
		logger_trace.Error.Printf("ejecutando sqlserverGetByEmail %s: %v", email, err)
		return nil, err
	}

	return m, nil
}

// IsLogged verifica si un usuario está logueado
func (p sqlserver) IsLogged(id int64) (int, error) {
	conn := db.GetConnection()
	var i int

	stmt, err := conn.Prepare(sqlserverIsLogged)
	if err != nil {
		logger_trace.Error.Printf("preparando IsLogged user: %v", err)
		return i, err
	}
	defer stmt.Close()

	err = stmt.QueryRow(sql.Named("id", id)).Scan(&i)
	if err == sql.ErrNoRows {
		return i, nil
	}
	if err != nil {
		logger_trace.Error.Printf("ejecutando IsLogged user: %v", err)
		return 0, err
	}

	return i, nil
}

func (p sqlserver) RegistryLogin(id int64) (int64, error) {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(sqlserverRegistryLogin)
	if err != nil {
		logger_trace.Error.Printf("preparando RegistryLogin user: %v", err)
		return 0, err
	}
	defer stmt.Close()

	ID, err := db.ExecGettingID(
		stmt,
		sql.Named("userid", id),
	)
	if err != nil {
		logger_trace.Error.Printf("ejecutando la consulta en Create module: %v", err)
		return 0, err
	}

	return ID, nil
}

// Logout elimina el registro del usuario logueado
func (p sqlserver) Logout(id int64) error {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(sqlserverLogout)
	if err != nil {
		logger_trace.Error.Printf("preparando el logout: %v", err)
		return err
	}
	defer stmt.Close()

	err = db.Execute(stmt, sql.Named("id", id))
	if err != nil {
		logger_trace.Error.Printf("ejecutando logout: %v", err)
		return err
	}

	return nil
}

// GetUserConnect obtiene los usuarios activos
func (p sqlserver) GetUserConnect() (Models, error) {
	ms := Models{}
	conn := db.GetConnection()

	stmt, err := conn.Prepare(sqlserverGetUsrConnected)
	if err != nil {
		logger_trace.Error.Printf("preparando sqlserverGetUsrConnected user: %v", err)
		return nil, err
	}
	defer stmt.Close()

	rows, err := stmt.Query()
	if err != nil {
		logger_trace.Error.Printf("ejecutando sqlserverGetUsrConnected user: %v", err)
		return ms, err
	}
	defer rows.Close()

	mod := Model{}

	for rows.Next() {
		err := rows.Scan(
			&mod.ID,
			&mod.Name,
			&mod.Lastname,
			&mod.Identification,
			&mod.Phonenumber,
			&mod.Email,
			&mod.RoleID,
			&mod.RoleName,
		)

		if err != nil {
			logger_trace.Error.Printf("escaneando sqlserverGetUsrConnected user: %v", err)
			return ms, err
		}

		ms = append(ms, mod)
	}

	return ms, nil
}

// Delete elimina un registro por id de usuarios conectados
func (p sqlserver) DeleteUserConnect(id int64) error {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(sqlserverDeleteUsrConnected)
	if err != nil {
		logger_trace.Error.Printf("preparando delete user: %v", err)
		return err
	}
	defer stmt.Close()

	err = db.ExecAffectingOneRow(stmt, sql.Named("id", id))
	if err != nil {
		logger_trace.Error.Printf("ejecutando delete user: %v", err)
		return err
	}

	return nil
}

// IfExistsIdentification verifica si la identificación existe
func (p sqlserver) IfExistsIdentification(identification int64) (bool, error) {
	conn := db.GetConnection()
	var i bool

	stmt, err := conn.Prepare(sqlserverIdentification)
	if err != nil {
		logger_trace.Error.Printf("preparando IfExistsIdentification identification: %v", err)
		return false, err
	}
	defer stmt.Close()

	n := int64(identification)
	str := strconv.FormatInt(n, 10)

	err = stmt.QueryRow(sql.Named("identification", str)).Scan(&i)
	if err != nil {
		// Si no hay filas significa que no está la identificación
		if err == sql.ErrNoRows {
			return false, nil
		}
		logger_trace.Error.Printf("ejecutando IfExistsIdentification identification: %v", err)
		return false, err
	}

	return i, nil
}

// IfExistsEmail verifica si el email existe
func (p sqlserver) IfExistsEmail(email string) (bool, error) {
	conn := db.GetConnection()
	var i bool

	stmt, err := conn.Prepare(sqlserverEmail)
	if err != nil {
		logger_trace.Error.Printf("preparando IfExistsEmail email: %v", err)
		return false, err
	}
	defer stmt.Close()

	err = stmt.QueryRow(sql.Named("email", email)).Scan(&i)
	if err != nil {
		// Si no hay filas significa que no está la identificación
		if err == sql.ErrNoRows {
			return false, nil
		}
		logger_trace.Error.Printf("ejecutando IfExistsEmail email: %v", err)
		return false, err
	}

	return i, nil
}

// Funcion que obtiene los modulos de un usuario
func (p sqlserver) GetModules(m *Model) (*Modules, error) {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(sqlserverGetModules)
	if err != nil {
		logger_trace.Error.Printf("preparando sqlserverGetModules: %v", err)
		return nil, err
	}
	defer stmt.Close()

	rows, err := stmt.Query(sql.Named("email", m.Email))
	if err != nil {
		logger_trace.Error.Printf("ejecutando sqlserverGetModules: %v", err)
		return nil, err
	}

	module := Module{}
	modules := Modules{}

	for rows.Next() {
		err := rows.Scan(
			&module.Module,
			&module.Componente,
			&module.Url,
			&module.Class,
		)
		if err != nil {
			logger_trace.Error.Printf("escaneando en GetModules user: %v", err)
			return nil, err
		}
		modules = append(modules, module)
	}

	return &modules, nil
}

func (sqlserver) GetFailedAttempts(m *Model) (int, error) {
	var i int
	conn := db.GetConnection()

	stmt, err := conn.Prepare(sqlserverGetFailedAttempts)
	if err != nil {
		logger_trace.Error.Printf("preparando sqlserverGetFailedAttempts: %v", err)
		return 0, err
	}
	defer stmt.Close()

	err = stmt.QueryRow(sql.Named("email", m.Email)).Scan(&i)
	if err != nil {
		logger_trace.Error.Printf("ejecutando sqlserverGetFailedAttempts: %v", err)
		return 0, err
	}

	return i, nil
}

func (sqlserver) FailedAttempt(m *Model) error {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(sqlserverFailedAttempt)
	if err != nil {
		logger_trace.Error.Printf("preparando sqlserverFailedAttempt: %v", err)
		return err
	}
	defer stmt.Close()

	err = db.ExecAffectingOneRow(stmt, sql.Named("email", m.Email))
	if err != nil {
		logger_trace.Error.Printf("ejecutando sqlserverFailedAttempt: %v", err)
		return err
	}

	return nil
}

func (sqlserver) FailedAttemptReset(m *Model) error {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(sqlserverFailedAttemptReset)
	if err != nil {
		logger_trace.Error.Printf("preparando sqlserverFailedAttemptReset: %v", err)
		return err
	}
	defer stmt.Close()

	err = db.ExecAffectingOneRow(stmt, sql.Named("email", m.Email))
	if err != nil {
		logger_trace.Error.Printf("ejecutando sqlserverFailedAttemptReset: %v", err)
		return err
	}

	return nil
}

func (sqlserver) Block(ID int64) error {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(sqlserverBlock)
	if err != nil {
		logger_trace.Error.Printf("preparando sqlserverBlock: %v", err)
		return err
	}
	defer stmt.Close()

	err = db.ExecAffectingOneRow(stmt, sql.Named("id", ID))
	if err != nil {
		logger_trace.Error.Printf("ejecutando sqlserverBlock: %v", err)
		return err
	}

	return nil
}

func (sqlserver) Unblock(ID int64) error {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(sqlserverUnblock)
	if err != nil {
		logger_trace.Error.Printf("preparando sqlserverUnblock: %v", err)
		return err
	}
	defer stmt.Close()

	err = db.ExecAffectingOneRow(stmt, sql.Named("id", ID))
	if err != nil {
		logger_trace.Error.Printf("ejecutando sqlserverUnblock: %v", err)
		return err
	}

	return nil
}

func (sqlserver) SetChangePassword(ID int64) error {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(sqlserverSetChangePassword)
	if err != nil {
		logger_trace.Error.Printf("preparando sqlserverSetChangePassword: %v", err)
		return err
	}
	defer stmt.Close()

	err = db.ExecAffectingOneRow(stmt, sql.Named("id", ID))
	if err != nil {
		logger_trace.Error.Printf("ejecutando sqlserverSetChangePassword: %v", err)
		return err
	}

	return nil
}

func (sqlserver) Disable(ID int64) error {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(sqlserverDisable)
	if err != nil {
		logger_trace.Error.Printf("preparando sqlserverDisable: %v", err)
		return err
	}
	defer stmt.Close()

	err = db.ExecAffectingOneRow(stmt, sql.Named("id", ID))
	if err != nil {
		logger_trace.Error.Printf("ejecutando sqlserverDisable: %v", err)
		return err
	}

	return nil
}

func (sqlserver) Enable(ID int64) error {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(sqlserverEnable)
	if err != nil {
		logger_trace.Error.Printf("preparando sqlserverEnable: %v", err)
		return err
	}
	defer stmt.Close()

	err = db.ExecAffectingOneRow(stmt, sql.Named("id", ID))
	if err != nil {
		logger_trace.Error.Printf("ejecutando sqlserverEnable: %v", err)
		return err
	}

	return nil
}

// GetAllEnabled obtiene los registros que estén habilitados
func (p sqlserver) GetAllEnabled() (Models, error) {
	ms := Models{}
	conn := db.GetConnection()

	stmt, err := conn.Prepare(sqlserverGetAllEnabled)
	if err != nil {
		logger_trace.Error.Printf("preparando sqlserverGetAllEnabled user: %v", err)
		return nil, err
	}
	defer stmt.Close()

	rows, err := stmt.Query()
	if err != nil {
		logger_trace.Error.Printf("ejecutando sqlserverGetAllEnabled user: %v", err)
		return ms, err
	}
	defer rows.Close()

	for rows.Next() {
		m := Model{}
		ll := pq.NullTime{}
		err = rows.Scan(&m.ID, &m.RoleID, &ll)
		if err != nil {
			logger_trace.Error.Printf("escaneando sqlserverGetAllEnabled user: %v", err)
			return ms, err
		}

		m.LastLogin = ll.Time
		ms = append(ms, m)
	}

	return ms, nil
}

func (sqlserver) SetLastLogin(m *Model) error {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(sqlserverSetLastLogin)
	if err != nil {
		logger_trace.Error.Printf("preparando sqlserverSetLastLogin: %v", err)
		return err
	}
	defer stmt.Close()

	err = db.ExecAffectingOneRow(stmt, sql.Named("id", m.ID))
	if err != nil {
		logger_trace.Error.Printf("ejecutando sqlserverSetLastLogin: %v", err)
		return err
	}

	return nil
}

func (p sqlserver) ChangeNewPassword(m *Model) error {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(sqlserverChangeNewPassword)
	if err != nil {
		logger_trace.Error.Printf("preparando sqlserverChangeNewPassword user: %v", err)
		return err
	}
	defer stmt.Close()

	err = db.ExecAffectingOneRow(
		stmt,
		sql.Named("password", m.Password),
		sql.Named("id", m.ID),
	)
	if err != nil {
		logger_trace.Error.Printf("ejecutando sqlserverChangeNewPassword user: %v", err)
		return err
	}

	return nil
}

func (s sqlserver) GetUserByQueue(QueueID int64) (Models, error) {
	conn := db.GetConnection()
	ms := Models{}
	stmt, err := conn.Prepare(sqlserverGetUserByQueue)
	if err != nil {
		logger_trace.Error.Printf("preparando getbyid user: %v", err)
		return nil, err
	}
	defer stmt.Close()

	rows, err := stmt.Query(sql.Named("id", QueueID))
	if err != nil {
		logger_trace.Error.Printf("ejecutando GetAll user: %v", err)
		return ms, err
	}
	defer rows.Close()

	for rows.Next() {
		m, err := s.scanRow(rows)
		if err != nil {
			logger_trace.Error.Printf("escaneando GetAll user: %v", err)
			return ms, err
		}

		ms = append(ms, *m)
	}

	return ms, nil
}

func (s sqlserver) GetByRouteAndRole(mdl *ModelRole) (*ModelRoles, error) {
	conn := db.GetConnection()
	ms := ModelRoles{}
	m := ModelRole{}
	stmt, err := conn.Prepare(sqlserverGetByRouteAndRole)
	if err != nil {
		logger_trace.Error.Printf("preparando sqlserverValidateRoute user: %v", err)
		return nil, err
	}
	defer stmt.Close()

	rows, err := stmt.Query(sql.Named("route", mdl.Route), sql.Named("role", mdl.RoleID))
	if err != nil {
		logger_trace.Error.Printf("ejecutando GetAll user: %v", err)
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		err := rows.Scan(
			&m.ID,
			&m.Route,
			&m.RoleID,
			&m.Get,
			&m.Post,
			&m.Delete,
			&m.Put,
		)
		if err != nil {
			logger_trace.Error.Printf("escaneando en scanRow user: %v", err)
			return nil, err
		}
		ms = append(ms, m)
	}
	return &ms, nil
}
