package user

import (
	"strings"

	"gitlab.com/e-capture/ecatch-ecm/majosystem/configuration"
	"gitlab.com/e-capture/ecatch-ecm/majosystem/logger_trace"
)

var s storage

func init() {
	setStorage()
}

// Storage interface de almacenamiento
type storage interface {
	Create(m *Model) error
	Update(m *Model, id int64) error
	Delete(id int64) error
	GetByID(id int64) (*Model, error)
	GetAll() (Models, error)
	GetByEmail(email string) (*Model, error)
	IsLogged(id int64) (int, error)
	RegistryLogin(id int64) (int64, error)
	Logout(id int64) error
	GetUserConnect() (Models, error)
	DeleteUserConnect(id int64) error
	IfExistsIdentification(identification int64) (bool, error)
	IfExistsEmail(email string) (bool, error)
	GetModules(m *Model) (*Modules, error)
	GetFailedAttempts(m *Model) (int, error)
	FailedAttempt(m *Model) error
	FailedAttemptReset(m *Model) error
	Block(ID int64) error
	Unblock(ID int64) error
	SetChangePassword(ID int64) error
	Disable(ID int64) error
	Enable(ID int64) error
	GetAllEnabled() (Models, error)
	SetLastLogin(m *Model) error
	ChangeNewPassword(m *Model) error
	GetUserByQueue(QueueID int64) (Models, error)
	GetByRouteAndRole(m *ModelRole) (*ModelRoles, error)
}

// setStorage asigna el sistema de almacenamiento
func setStorage() {
	c := configuration.FromFile()
	switch strings.ToLower(c.DBConnection) {
	case "sqlserver":
		s = sqlserver{}
	case "postgres":
		fallthrough
	case "mysql":
		fallthrough
	case "oracle":
		fallthrough
	default:
		logger_trace.Error.Printf("este motor de bd no está configurado aún: %s", c.DBConnection)
	}
}
