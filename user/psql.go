package user

import (
	"database/sql"
	"errors"

	"gitlab.com/e-capture/ecatch-ecm/majosystem/db"
	"gitlab.com/e-capture/ecatch-ecm/majosystem/logger_trace"
)

// psql estructura de DAO postgres
type psql struct{}

const (
	psqlInsert  = `INSERT INTO usr_users (name, lastname, identification, phonenumber, email,  password, role_id) VALUES ($1, $2, $3, $4, $5, $6, $7) RETURNING id`
	psqlUpdate  = `UPDATE usr_users SET name = $1, lastname = $2, identification = $3, phonenumber = $4, email = $5, role_id = $6 WHERE id = $7`
	psqlDelete  = `DELETE FROM usr_users WHERE id = $1`
	psqlSelect  = `SELECT u.id, u.name, u.lastname, u.identification, u.phonenumber, u.email, u.role_id, r.name as rolename FROM usr_users u INNER JOIN usr_roles r ON (r.id = u.role_id)`
	psqlGetByID = psqlSelect + ` WHERE u.id = $1`
	psqlGetAll  = psqlSelect + ` `

	// psqlLogin no usa la constante psqlSelect debido a que se requiere el password
	psqlLogin           = `SELECT id, name, lastname, identification, phonenumber, email, password, role_id FROM usr_users WHERE email = $1`
	psqlIsLogged        = `SELECT id FROM usr_loggedusers WHERE user_id = $1`
	psqlLogout          = `DELETE FROM usr_loggedusers WHERE user_id = $1`
	psqlGetUsrConnected = `SELECT logged.id, usr.name, usr.lastname, usr.identification, usr.phonenumber, usr.email, role.name rolename, logged.created_at, logged.updated_at
					FROM usr_loggedusers logged
					INNER JOIN usr_users usr ON (usr.id = logged.user_id)
					INNER JOIN usr_roles role ON (usr.role_id = role.id)`
	psqlDeleteUsrConnected = `DELETE FROM usr_loggedusers WHERE user_id = $1`
	psqlGetModules         = `SELECT DISTINCT m.name module,c.name componente, c.url_front url FROM    usr_modules m WITH (NOLOCK) INNER JOIN usr_components c WITH (NOLOCK) ON (m.id= c.module_id) INNER JOIN usr_elements e WITH (NOLOCK) ON (e.component_id = c.id) INNER JOIN usr_role_usr_element rue WITH (NOLOCK) ON (e.id = rue.element_id) INNER JOIN usr_users u WITH (NOLOCK) ON (rue.role_id = u.role_id)	WHERE e.name = 'load' and u.email = $1`

	// Verificación de existencia de un determinado dato de usuario
	psqlIdentification = `SELECT COUNT(*) FROM usr_users WHERE identification = $1`
	psqlEmail          = `SELECT COUNT(*) FROM usr_users WHERE email = $1`
)

// Create registra en la bd
func (p psql) Create(m *Model) error {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(psqlInsert)
	if err != nil {
		logger_trace.Error.Printf("preparando create user: %v", err)
		return err
	}
	defer stmt.Close()

	err = stmt.QueryRow(m.Name, m.Lastname, m.Identification, m.Phonenumber, m.Email, m.EncryptPassword(), m.RoleID).Scan(&m.ID)
	if err != nil {
		logger_trace.Error.Printf("ejecutando create user: %v", err)
		return err
	}

	return nil
}

// Update actualiza un registro
func (p psql) Update(m *Model, id int64) error {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(psqlUpdate)
	if err != nil {
		logger_trace.Error.Printf("preparando update user: %v", err)
		return err
	}
	defer stmt.Close()

	err = db.ExecAffectingOneRow(stmt, m.Name, m.Lastname, m.Identification, m.Phonenumber, m.Email, m.RoleID, id)
	if err != nil {
		logger_trace.Error.Printf("ejecutando update user: %v", err)
		return err
	}

	return nil
}

// Delete elimina un registro por id
func (p psql) Delete(id int64) error {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(psqlDelete)
	if err != nil {
		logger_trace.Error.Printf("preparando delete user: %v", err)
		return err
	}
	defer stmt.Close()

	err = db.ExecAffectingOneRow(stmt, id)
	if err != nil {
		logger_trace.Error.Printf("ejecutando delete user: %v", err)
		return err
	}

	return nil
}

// GetByID obtiene un registro por id
func (p psql) GetByID(id int64) (*Model, error) {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(psqlGetByID)
	if err != nil {
		logger_trace.Error.Printf("preparando getbyid user: %v", err)
		return nil, err
	}
	defer stmt.Close()

	row := stmt.QueryRow(id)
	return p.scanRow(row)
}

// GetAll obtiene todos los registros
func (p psql) GetAll() (Models, error) {
	ms := Models{}
	conn := db.GetConnection()

	stmt, err := conn.Prepare(psqlGetAll)
	if err != nil {
		logger_trace.Error.Printf("preparando GetAll user: %v", err)
		return nil, err
	}
	defer stmt.Close()

	rows, err := stmt.Query()
	if err != nil {
		logger_trace.Error.Printf("ejecutando GetAll user: %v", err)
		return ms, err
	}
	defer rows.Close()

	for rows.Next() {
		m, err := p.scanRow(rows)
		if err != nil {
			logger_trace.Error.Printf("escaneando GetAll user: %v", err)
			return ms, err
		}

		ms = append(ms, *m)
	}

	return ms, nil
}

// scanRow escanea un registro
func (p psql) scanRow(s db.RowScanner) (*Model, error) {
	m := &Model{}

	err := s.Scan(
		&m.ID,
		&m.Name,
		&m.Lastname,
		&m.Identification,
		&m.Phonenumber,
		&m.Email,
		&m.RoleID,
		&m.RoleName,
	)
	if err != nil {
		logger_trace.Error.Printf("escaneando en scanRow user: %v", err)
		return nil, err
	}

	return m, nil
}

// Login valida el login de usuario
func (p psql) ValidateLogin(email, password string) (*Model, error) {
	conn := db.GetConnection()
	m := &Model{}

	stmt, err := conn.Prepare(psqlLogin)
	if err != nil {
		logger_trace.Error.Printf("preparando Login user: %v", err)
		return nil, err
	}
	defer stmt.Close()

	// No se usa p.scan(row) debido a que se requiere el password
	row := stmt.QueryRow(email)
	err = row.Scan(
		&m.ID,
		&m.Name,
		&m.Lastname,
		&m.Identification,
		&m.Phonenumber,
		&m.Email,
		&m.Password,
		&m.RoleID,
	)
	if err != nil {
		if err == sql.ErrNoRows {
			// Se devuelve un error genérico para no darle oportunidad a un atacante
			return nil, errors.New("usuario o contraseña no válidos")
		}
		logger_trace.Error.Printf("ejecutando Login user: %v", err)
		return nil, err
	}

	if !m.ComparePassword(password) {
		return nil, errors.New("usuario o contraseña no válidos")
	}
	m.Password = ""

	return m, nil
}

// IsLogged verifica si un usuario está logueado
func (p psql) IsLogged(id int64) (bool, error) {
	conn := db.GetConnection()
	var i int

	stmt, err := conn.Prepare(psqlIsLogged)
	if err != nil {
		logger_trace.Error.Printf("preparando IsLogged user: %v", err)
		return false, err
	}
	defer stmt.Close()

	err = stmt.QueryRow(id).Scan(&i)
	if err != nil {
		// Si no hay filas significa que no está logueado
		if err == sql.ErrNoRows {
			return false, nil
		}
		logger_trace.Error.Printf("ejecutando IsLogged user: %v", err)
		return false, err
	}

	if i > 0 {
		return true, errors.New("el usuario ya está logueado")
	}

	return false, nil
}

// Logout elimina el registro del usuario logueado
func (p psql) Logout(id int64) error {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(psqlLogout)
	if err != nil {
		logger_trace.Error.Printf("preparando el logout: %v", err)
		return err
	}
	defer stmt.Close()

	err = db.ExecAffectingOneRow(stmt, id)
	if err != nil {
		logger_trace.Error.Printf("ejecutando logout: %v", err)
		return err
	}

	return nil
}

// GetUserConnect obtiene los usuarios activos
func (p psql) GetUserConnect() (Models, error) {
	ms := Models{}
	conn := db.GetConnection()

	stmt, err := conn.Prepare(psqlGetUsrConnected)
	if err != nil {
		logger_trace.Error.Printf("preparando psqlGetUsrConnected user: %v", err)
		return nil, err
	}
	defer stmt.Close()

	rows, err := stmt.Query()
	if err != nil {
		logger_trace.Error.Printf("ejecutando psqlGetUsrConnected user: %v", err)
		return ms, err
	}
	defer rows.Close()

	for rows.Next() {
		m, err := p.scanRow(rows)
		if err != nil {
			logger_trace.Error.Printf("escaneando psqlGetUsrConnected user: %v", err)
			return ms, err
		}

		ms = append(ms, *m)
	}

	return ms, nil
}

// Delete elimina un registro por id de usuarios conectados
func (p psql) DeleteUserConnect(id int64) error {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(psqlDeleteUsrConnected)
	if err != nil {
		logger_trace.Error.Printf("preparando psqlDeleteUsrConnected user: %v", err)
		return err
	}
	defer stmt.Close()

	err = db.ExecAffectingOneRow(stmt, id)
	if err != nil {
		logger_trace.Error.Printf("ejecutando psqlDeleteUsrConnected user: %v", err)
		return err
	}

	return nil
}

// IfExistsIdentification verifica si la identificación existe
func (p psql) IfExistsIdentification(identification int64) (bool, error) {
	conn := db.GetConnection()
	var i bool

	stmt, err := conn.Prepare(psqlIdentification)
	if err != nil {
		logger_trace.Error.Printf("preparando IfExistsIdentification identification: %v", err)
		return false, err
	}
	defer stmt.Close()

	err = stmt.QueryRow(sql.Named("identification", identification)).Scan(&i)
	if err != nil {
		// Si no hay filas significa que no está la identificación
		if err == sql.ErrNoRows {
			return false, nil
		}
		logger_trace.Error.Printf("ejecutando IfExistsIdentification identification: %v", err)
		return false, err
	}

	return i, nil
}

// IfExistsEmail verifica si el email existe
func (p psql) IfExistsEmail(email string) (bool, error) {
	conn := db.GetConnection()
	var i bool

	stmt, err := conn.Prepare(psqlEmail)
	if err != nil {
		logger_trace.Error.Printf("preparando IfExistsEmail email: %v", err)
		return false, err
	}
	defer stmt.Close()

	err = stmt.QueryRow(sql.Named("email", email)).Scan(&i)
	if err != nil {
		// Si no hay filas significa que no está la identificación
		if err == sql.ErrNoRows {
			return false, nil
		}
		logger_trace.Error.Printf("ejecutando IfExistsEmail email: %v", err)
		return false, err
	}

	return i, nil
}

// Funcion que obtiene los modulos de un usuario
func (p psql) GetModules(m *Model) (*Modules, error) {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(psqlGetModules)
	if err != nil {
		logger_trace.Error.Printf("preparando sqlserverGetModules: %v", err)
		return nil, err
	}
	defer stmt.Close()

	rows, err := stmt.Query(m.Email)
	if err != nil {
		logger_trace.Error.Printf("ejecutando sqlserverGetModules: %v", err)
		return nil, err
	}

	module := Module{}
	modules := Modules{}

	for rows.Next() {
		err := rows.Scan(
			&module.Module,
			&module.Componente,
			&module.Url,
		)
		if err != nil {
			logger_trace.Error.Printf("escaneando en GetModules user: %v", err)
			return nil, err
		}
		modules = append(modules, module)
	}

	return &modules, nil
}
