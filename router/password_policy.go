package router

import (
	"github.com/labstack/echo"
	"gitlab.com/e-capture/ecatch-ecm/ecmauthentication/password_policy"
	"gitlab.com/e-capture/ecatch-ecm/ecmauthentication/user"
)

func password_policyRoute(e *echo.Echo) {
	// TODO validar con scope
	r := e.Group("/api/v1/password_policy", user.ValidateJWT)

	r.POST("", password_policy.Create)
	r.GET("", password_policy.GetAll)
	r.GET("/:id", password_policy.GetByID)
	r.PUT("/:id", password_policy.Update)
	r.DELETE("/:id", password_policy.Delete)

	r.GET("/role/:id", password_policy.GetByRoleID)
	r.PUT("/role/:id", password_policy.UpdateByRoleID)

}
