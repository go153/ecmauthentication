package router

import (
	"github.com/labstack/echo"
	"gitlab.com/e-capture/ecatch-ecm/ecmauthentication/user"
	"gitlab.com/e-capture/ecatch-ecm/ecmauthentication/white_list_ip"
)

func white_list_ipRoute(e *echo.Echo) {
	r := e.Group("/api/v1/white_list_ip", user.ValidateJWT)

	r.POST("", white_list_ip.Create)
	r.GET("", white_list_ip.GetAll)
	r.GET("/:id", white_list_ip.GetByID)
	r.PUT("/:id", white_list_ip.Update)
	r.DELETE("/:id", white_list_ip.Delete)
}
