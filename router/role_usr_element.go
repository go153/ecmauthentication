package router

import (
	"github.com/labstack/echo"
	"gitlab.com/e-capture/ecatch-ecm/ecmauthentication/role_usr_element"
	"gitlab.com/e-capture/ecatch-ecm/ecmauthentication/user"
)

func role_usr_elementRoute(e *echo.Echo) {
	r := e.Group("/api/v1/role_usr_element", user.ValidateJWT)
	r.POST("", role_usr_element.Create)
	r.GET("", role_usr_element.GetAll)
	r.GET("/:id", role_usr_element.GetByID)
	r.GET("/role/:id", role_usr_element.GetByRoleID)
	r.GET("/component/:component_id/:role_id", role_usr_element.GetByComponentID)
	r.PUT("/:id", role_usr_element.Update)
	r.DELETE("/:id", role_usr_element.Delete)
	r.DELETE("/role/:role_id/:element_id", role_usr_element.DeleteByElementRoleID)
}
