package router

import (
	"github.com/labstack/echo"
	"gitlab.com/e-capture/ecatch-ecm/ecmauthentication/black_list_password"
	"gitlab.com/e-capture/ecatch-ecm/ecmauthentication/user"
)

func black_list_passwordRoute(e *echo.Echo) {
	r := e.Group("/api/v1/black_list_password", user.ValidateJWT)

	r.POST("", black_list_password.Create)
	r.POST("/bulk", black_list_password.BulkLoad)
	r.GET("", black_list_password.GetAll)
	r.GET("/:id", black_list_password.GetByID)
	r.PUT("/:id", black_list_password.Update)
	r.DELETE("/:id", black_list_password.Delete)
}
