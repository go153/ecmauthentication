package router

import (
	"github.com/labstack/echo"
	"gitlab.com/e-capture/ecatch-ecm/ecmauthentication/element"
	"gitlab.com/e-capture/ecatch-ecm/ecmauthentication/user"
)

func elementRoute(e *echo.Echo) {
	r := e.Group("/api/v1/element", user.ValidateJWT)

	r.POST("", element.Create)
	r.GET("", element.GetAll)
	r.GET("/:id", element.GetByID)
	r.PUT("/:id", element.Update)
	r.DELETE("/:id", element.Delete)
	r.GET("/component/:component", element.GetElementByComponent)
}
