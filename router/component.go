package router

import (
	"github.com/labstack/echo"
	"gitlab.com/e-capture/ecatch-ecm/ecmauthentication/component"
	"gitlab.com/e-capture/ecatch-ecm/ecmauthentication/user"
)

func componentRoute(e *echo.Echo) {
	r := e.Group("/api/v1/component", user.ValidateJWT)

	r.POST("", component.Create)
	r.GET("", component.GetAll)
	r.GET("/:id", component.GetByID)
	r.PUT("/:id", component.Update)
	r.DELETE("/:id", component.Delete)
	r.GET("/elements/:component_id", component.GetElementComponent)

}
