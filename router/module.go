package router

import (
	"github.com/labstack/echo"
	"gitlab.com/e-capture/ecatch-ecm/ecmauthentication/module"
	"gitlab.com/e-capture/ecatch-ecm/ecmauthentication/user"
)

func moduleRoute(e *echo.Echo) {
	r := e.Group("/api/v1/module", user.ValidateJWT)

	r.POST("", module.Create)
	r.GET("", module.GetAll)
	r.GET("/:id", module.GetByID)
	r.PUT("/:id", module.Update)
	r.DELETE("/:id", module.Delete)
}
