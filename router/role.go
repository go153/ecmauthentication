package router

import (
	"github.com/labstack/echo"
	"gitlab.com/e-capture/ecatch-ecm/ecmauthentication/role"
	"gitlab.com/e-capture/ecatch-ecm/ecmauthentication/user"
)

func roleRoute(e *echo.Echo) {
	r := e.Group("/api/v2/roles", user.ValidateJWT)

	r.POST("", role.Create)
	r.GET("", role.GetAll)
	r.GET("/:id", role.GetByID)
	r.PUT("/:id", role.Update)
	r.DELETE("/:id", role.Delete)

}
