package router

import (
	"github.com/labstack/echo"
	"gitlab.com/e-capture/ecatch-ecm/ecmauthentication/administration"
	"gitlab.com/e-capture/ecatch-ecm/ecmauthentication/user"
)

func administrationRoute(e *echo.Echo) {
	r := e.Group("/api/v1/administration", user.ValidateJWT)

	r.POST("/user-block", administration.UserLockUnlock)
	r.POST("/user-disable", administration.UserDisableEnable)
}
