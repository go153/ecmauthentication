package router

import (
	"strings"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"gitlab.com/e-capture/ecatch-ecm/majosystem/configuration"
)

func StartService() {
	c := configuration.FromFile()

	e := echo.New()
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: c.GetAllowedDomains(),
	}))
	//e.Use(message_exchange.Register)

	administrationRoute(e)
	black_list_passwordRoute(e)
	componentRoute(e)
	dates_disallowedRoute(e)
	elementRoute(e)
	loggeduserRoute(e)
	moduleRoute(e)
	password_historyRoute(e)
	password_policyRoute(e)
	role_usr_elementRoute(e)
	roleRoute(e)
	// Rutas para el paquete User
	userRoute(e)
	// Rutas para el login
	loginRoute(e)
	// Rutas para la administración de usuarios
	userRouteAdmin(e)
	// Rutas para validar estado del servicio
	healthRoute(e)

	// Inicia el servidor
	if strings.ToLower(c.AppIsSSL) == "t" {
		e.Logger.Fatal(e.StartTLS(":"+c.AppPort, c.AppCertificate, c.AppPrivateKey))
	} else {
		e.Logger.Fatal(e.Start(":" + c.AppPort))
	}
}
