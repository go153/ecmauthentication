package router

import (
	"github.com/labstack/echo"
	"gitlab.com/e-capture/ecatch-ecm/ecmauthentication/loggeduser"
	"gitlab.com/e-capture/ecatch-ecm/ecmauthentication/user"
)

func loggeduserRoute(e *echo.Echo) {
	r := e.Group("/api/v1/loggeduser", user.ValidateJWT)

	r.POST("", loggeduser.Create)
	r.GET("", loggeduser.GetAll)
	r.GET("/:id", loggeduser.GetByID)
	r.PUT("/:id", loggeduser.Update)
	r.DELETE("/:id", loggeduser.Delete)
}
