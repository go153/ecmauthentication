package router

import (
	"github.com/labstack/echo"
	"gitlab.com/e-capture/ecatch-ecm/ecmauthentication/password_history"
	"gitlab.com/e-capture/ecatch-ecm/ecmauthentication/user"
)

func password_historyRoute(e *echo.Echo) {
	r := e.Group("/api/v1/password_history", user.ValidateJWT)

	r.POST("", password_history.Create)
	r.GET("", password_history.GetAll)
	r.GET("/:id", password_history.GetByID)
	r.PUT("/:id", password_history.Update)
	r.DELETE("/:id", password_history.Delete)
}
