package router

import (
	"github.com/labstack/echo"
	"gitlab.com/e-capture/ecatch-ecm/majosystem/health"
)

func healthRoute(e *echo.Echo) {
	r := e.Group("/health")
	r.GET("", health.Health)
}
