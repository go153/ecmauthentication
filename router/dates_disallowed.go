package router

import (
	"github.com/labstack/echo"
	"gitlab.com/e-capture/ecatch-ecm/ecmauthentication/dates_disallowed"
	"gitlab.com/e-capture/ecatch-ecm/ecmauthentication/user"
)

func dates_disallowedRoute(e *echo.Echo) {
	r := e.Group("/api/v1/dates_disallowed", user.ValidateJWT)

	r.POST("", dates_disallowed.Create)
	r.GET("", dates_disallowed.GetAll)
	r.GET("/:id", dates_disallowed.GetByID)
	r.PUT("/:id", dates_disallowed.Update)
	r.DELETE("/:id", dates_disallowed.Delete)
}
