package router

import (
	"github.com/labstack/echo"
	"gitlab.com/e-capture/ecatch-ecm/ecmauthentication/user"
)

func userRoute(e *echo.Echo) {
	e.POST("/api/v2/singon", user.CreateWithoutToken)
	r := e.Group("/api/v2/users", user.ValidateJWT)

	//r.Use(message_exchange.Register)
	r.POST("", user.Create)
	r.GET("", user.GetAll)
	r.GET("/:id", user.GetByID)
	r.GET("/logout", user.Logout)
	r.PUT("/:id", user.Update)
	r.GET("/modules", user.GetModules)
	r.PUT("/passManagement", user.PasswordManagementSelf)
	r.GET("/queue/:id", user.GetUsersByQueue)
}

func loginRoute(e *echo.Echo) {
	r := e.Group("/api/v2")
	//r.Use(message_exchange.Register)
	r.POST("/login", user.Login)
}

func userRouteAdmin(e *echo.Echo) {
	const route = "/api/v2/users-admin"
	s := user.Scope{route}
	r := e.Group(route, user.ValidateJWT)
	// validar si se requiere proteger las rutas
	r.Use(s.ValidatePermissions)

	//r.Use(message_exchange.Register)
	r.DELETE("/:id", user.Delete)
	r.GET("/getusersconnected", user.GetUserConnect)
	r.DELETE("/disconectuser/:id", user.DeleteUserConnect)
	r.GET("/ifexists/identification/:identification", user.IfExistsIdentification)
	r.GET("/ifexists/email/:email", user.IfExistsEmail)
	r.PUT("/restore-password", user.PasswordManagementAdmin)
	r.PUT("/change-password", user.ChangePasswordAdmin)
	r.PUT("/enable-disable", user.UserDisableEnableAdmin)
}
