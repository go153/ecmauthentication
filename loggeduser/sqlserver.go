package loggeduser

import (
	"database/sql"

	"github.com/lib/pq"
	"gitlab.com/e-capture/ecatch-ecm/majosystem/db"
	"gitlab.com/e-capture/ecatch-ecm/majosystem/logger_trace"
)

type sqlserver struct{}

const (
	sqlInsert    = `INSERT INTO usr.loggedusers (user_id) VALUES (@user_id) SELECT ID = convert(bigint, SCOPE_IDENTITY()) `
	sqlUpdate    = `UPDATE usr.loggedusers SET user_id = @user_id,  updated_at = getdate() WHERE id = @id`
	sqlDelete    = `DELETE FROM usr.loggedusers WHERE id = @id`
	sqlGetByID   = `SELECT id, user_id,  created_at, updated_at FROM usr.loggedusers WITH (NOLOCK) WHERE id = @id`
	sqlGetAll    = `SELECT id, user_id,  created_at, updated_at FROM usr.loggedusers WITH (NOLOCK)`
	sqlDeleteAll = `DELETE FROM usr.loggedusers`
)

func (s sqlserver) Create(m *Model) error {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(sqlInsert)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en Create loggeduser: %v", err)
		return err
	}
	defer stmt.Close()

	ID, err := db.ExecGettingID(
		stmt,

		sql.Named("user_id", m.UserId),
	)
	if err != nil {
		logger_trace.Error.Printf("ejecutando la consulta en Create loggeduser: %v", err)
		return err
	}
	m.ID = ID

	return nil
}

func (s sqlserver) Update(ID int64, m *Model) error {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(sqlUpdate)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en Update loggeduser: %v", err)
		return err
	}
	defer stmt.Close()

	err = db.ExecAffectingOneRow(
		stmt,

		sql.Named("user_id", m.UserId),

		sql.Named("id", ID),
	)
	if err != nil {
		logger_trace.Error.Printf("ejecutando la consulta en Update loggeduser: %v", err)
		return err
	}
	m.ID = ID

	return nil
}

func (s sqlserver) Delete(ID int64) error {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(sqlDelete)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en Delete loggeduser: %v", err)
		return err
	}
	defer stmt.Close()

	err = db.ExecAffectingOneRow(
		stmt,
		sql.Named("id", ID),
	)
	if err != nil {
		logger_trace.Error.Printf("ejecutando la consulta en Delete loggeduser: %v", err)
		return err
	}

	return nil
}

func (s sqlserver) GetByID(ID int64) (*Model, error) {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(sqlGetByID)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en GetByID loggeduser: %v", err)
		return nil, err
	}
	defer stmt.Close()

	row := stmt.QueryRow(sql.Named("id", ID))
	return s.scanRow(row)
}

func (s sqlserver) GetAll() (Models, error) {
	conn := db.GetConnection()
	ms := make(Models, 0)

	stmt, err := conn.Prepare(sqlGetAll)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en GetAll loggeduser: %v", err)
		return nil, err
	}
	defer stmt.Close()

	rows, err := stmt.Query()
	if err != nil {
		logger_trace.Error.Printf("ejecutando la consulta en GetAll loggeduser: %v", err)
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		m, err := s.scanRow(rows)
		if err != nil {
			logger_trace.Error.Printf("escaneando el registro en loggeduser: %v", err)
			return ms, err
		}

		ms = append(ms, *m)
	}

	return ms, nil
}

func (s sqlserver) scanRow(rs db.RowScanner) (*Model, error) {
	m := &Model{}
	cn := pq.NullTime{}
	un := pq.NullTime{}

	err := rs.Scan(
		&m.ID,

		&m.UserId,

		&cn,
		&un,
	)
	if err != nil {
		logger_trace.Error.Printf("escaneando el modelo loggeduser: %v", err)
		return nil, err
	}

	m.CreatedAt = cn.Time
	m.UpdatedAt = un.Time
	return m, nil
}

func (s sqlserver) DeleteAll() error {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(sqlDeleteAll)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en Delete loggeduser: %v", err)
		return err
	}
	defer stmt.Close()

	err = db.ExecAffectingOneRow(stmt)
	if err != nil {
		logger_trace.Error.Printf("ejecutando la consulta en Delete loggeduser: %v", err)
		return err
	}

	return nil
}
