package main

import (
	"fmt"

	"gitlab.com/e-capture/ecatch-ecm/ecmauthentication/router"
	"gitlab.com/e-capture/ecatch-ecm/majosystem/encryp"
	"gitlab.com/e-capture/ecatch-ecm/majosystem/logger_trace"
)

func main() {
	fmt.Println("Version 1.0.4")
	/*c := configuration.FromFile()
	dateValidate, err := decriptLicense(c.AppKey)
	if err != nil {
		logger_trace.Error.Print("no se pudo descifrar el valor: %v", err)
		//panic(fmt.Sprintf("LICENCIAMIENTO NO VALIDO"))
	}
	tLicense, _ := time.Parse("2006-01-02", dateValidate[0:10])
	dateNow := time.Now()
	if dateNow.After(tLicense) {
		//panic(fmt.Sprintf("Fecha de licenciamiento vencido, por favor validar la fecha habilitada es: %s", tLicense))
	}*/
	//go inactivity_user.LockUserByInactivityTime()
	//go delete_user_connect.DeleteUserConnect()
	router.StartService()

}

func decriptLicense(license string) (string, error) {
	cip, err := encryp.DecryptGCM(license, "clave secreta")
	if err != nil {
		logger_trace.Error.Print("no se pudo descifrar el valor: %v", err)
		return "", err
	}
	return string(cip), nil
}
