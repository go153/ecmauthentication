package component

import (
	"strings"

	// Esta importación se debe modificar a los paquetes del proyecto
	"gitlab.com/e-capture/ecatch-ecm/majosystem/configuration"
	"gitlab.com/e-capture/ecatch-ecm/majosystem/logger_trace"
)

var s Storage

func init() {
	setStorage()
}

type Storage interface {
	Create(m *Model) error
	Update(ID int64, m *Model) error
	Delete(ID int64) error
	GetByID(ID int64) (*Model, error)
	GetAll() (Models, error)
	GetElementByComponent(ComponentID, roleID int) (ElementByComoponents, error)
}

func setStorage() {
	c := configuration.FromFile()
	switch strings.ToLower(c.DBConnection) {
	case "sqlserver":
		s = sqlserver{}
	case "postgres":
		fallthrough
	case "oci8":
		fallthrough
	default:
		logger_trace.Error.Printf("este motor de bd no está configurado aún: %s", c.DBConnection)
	}
}
