package component

import "time"

type Model struct {
	ID        int64     `json:"id"`
	Name      string    `json:"name"`
	UrlFront  string    `json:"url_front"`
	Class     string    `json:"class"`
	ModuleId  int       `json:"module_id"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}

type Models []Model

func (m *Model) Create() error {
	return s.Create(m)
}

func (m *Model) Update(ID int64) error {
	return s.Update(ID, m)
}

func (m *Model) Delete(ID int64) error {
	return s.Delete(ID)
}

func (m *Model) GetByID(ID int64) (*Model, error) {
	return s.GetByID(ID)
}

func (m *Model) GetAll() (Models, error) {
	return s.GetAll()
}

type ElementByComoponent struct {
	ElementID          int    `json:"element_id"`
	ElementName        string `json:"element_name"`
	ElementDescription string `json:"elemen_description`
	Privilege          int    `json:"privilege"`
}
type ElementByComoponents []ElementByComoponent

func (m *Model) GetElementByComponent(componentID, roleID int) (ElementByComoponents, error) {
	return s.GetElementByComponent(componentID, roleID)
}
