package component

import (
	"github.com/lib/pq"
	"gitlab.com/e-capture/ecatch-ecm/majosystem/db"
	"gitlab.com/e-capture/ecatch-ecm/majosystem/logger_trace"
)

type pgsql struct{}

const (
	pgsqlInsert  = `INSERT INTO usr_components (name, url_front, class, module_id) VALUES ($1, $2, $3, $4)`
	pgsqlUpdate  = `UPDATE usr_components SET name = $1, url_front = $2, class = $3, module_id = $4,  updated_at = getdate() WHERE id = $5`
	pgsqlDelete  = `DELETE FROM usr_components WHERE id = $5`
	pgsqlGetByID = `SELECT c.id, concat(m.name, ' - ', c.name) as name, c.url_front, c.class, c.module_id, c.created_at, c.updated_at FROM dbo.usr_components c WITH (NOLOCK) INNER JOIN dbo.usr_modules m WITH (NOLOCK) on (m.id = c.module_id) WHERE c.id = $5`
	pgsqlGetAll  = `SELECT c.id, concat(m.name, ' - ', c.name) as name, c.url_front, c.class, c.module_id, c.created_at, c.updated_at FROM dbo.usr_components c WITH (NOLOCK) INNER JOIN dbo.usr_modules m WITH (NOLOCK) on (m.id = c.module_id)`
)

func (p pgsql) Create(m *Model) error {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(pgsqlInsert)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en Create component: %v", err)
		return err
	}
	defer stmt.Close()

	ID, err := db.ExecGettingID(
		stmt,
		&m.Name, &m.UrlFront, &m.Class, &m.ModuleId,
	)
	if err != nil {
		logger_trace.Error.Printf("ejecutando la consulta en Create component: %v", err)
		return err
	}
	m.ID = ID

	return nil
}

func (p pgsql) Update(ID int64, m *Model) error {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(pgsqlUpdate)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en Update component: %v", err)
		return err
	}
	defer stmt.Close()

	err = db.ExecAffectingOneRow(
		stmt,
		&m.Name, &m.UrlFront, &m.Class, &m.ModuleId,
		ID,
	)
	if err != nil {
		logger_trace.Error.Printf("ejecutando la consulta en Update component: %v", err)
		return err
	}
	m.ID = ID

	return nil
}

func (p pgsql) Delete(ID int64) error {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(pgsqlDelete)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en Delete component: %v", err)
		return err
	}
	defer stmt.Close()

	err = db.ExecAffectingOneRow(
		stmt,
		ID,
	)
	if err != nil {
		logger_trace.Error.Printf("ejecutando la consulta en Delete component: %v", err)
		return err
	}

	return nil
}

func (p pgsql) GetByID(ID int64) (*Model, error) {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(pgsqlGetByID)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en GetByID component: %v", err)
		return nil, err
	}
	defer stmt.Close()

	row := stmt.QueryRow(ID)
	return p.scanRow(row)
}

func (p pgsql) GetAll() (Models, error) {
	conn := db.GetConnection()
	ms := make(Models, 0)

	stmt, err := conn.Prepare(pgsqlGetAll)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en GetAll component: %v", err)
		return nil, err
	}
	defer stmt.Close()

	rows, err := stmt.Query()
	if err != nil {
		logger_trace.Error.Printf("ejecutando la consulta en GetAll component: %v", err)
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		m, err := p.scanRow(rows)
		if err != nil {
			logger_trace.Error.Printf("escaneando el registro en component: %v", err)
			return ms, err
		}

		ms = append(ms, *m)
	}

	return ms, nil
}

func (p pgsql) scanRow(rs db.RowScanner) (*Model, error) {
	m := &Model{}
	cn := pq.NullTime{}
	un := pq.NullTime{}

	err := rs.Scan(
		&m.ID,

		&m.Name,

		&m.UrlFront,

		&m.Class,

		&m.ModuleId,

		&cn,
		&un,
	)
	if err != nil {
		logger_trace.Error.Printf("escaneando el modelo component: %v", err)
		return nil, err
	}

	m.CreatedAt = cn.Time
	m.UpdatedAt = un.Time
	return m, nil
}
