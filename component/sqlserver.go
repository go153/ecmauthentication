package component

import (
	"database/sql"

	"github.com/lib/pq"
	"gitlab.com/e-capture/ecatch-ecm/majosystem/db"
	"gitlab.com/e-capture/ecatch-ecm/majosystem/logger_trace"
)

type sqlserver struct{}

const (
	sqlInsert                = `INSERT INTO usr.components (name, url_front, class, module_id) VALUES (@name, @url_front, @class, @module_id) SELECT ID = convert(bigint, SCOPE_IDENTITY()) `
	sqlUpdate                = `UPDATE usr.components SET name = @name, url_front = @url_front, class = @class, module_id = @module_id,  updated_at = getdate() WHERE id = @id`
	sqlDelete                = `DELETE FROM usr.components WHERE id = @id`
	sqlGetByID               = `SELECT c.id, concat(m.name, ' - ', c.name) as name, c.url_front, c.class, c.module_id, c.created_at, c.updated_at FROM usr.components c WITH (NOLOCK) INNER JOIN usr.modules m WITH (NOLOCK) on (m.id = c.module_id) WHERE c.id = @id`
	sqlGetAll                = `SELECT c.id, concat(m.name, ' - ', c.name) as name, c.url_front, c.class, c.module_id, c.created_at, c.updated_at FROM usr.components c WITH (NOLOCK) INNER JOIN usr.modules m WITH (NOLOCK) on (m.id = c.module_id)`
	sqlGetElementByComponent = `SELECT  e.id element_id, e.name elementname, e.description element_description ,CASE WHEN rue.role_id = @role_id then 1 ELSE 0 END privilege FROM usr.role_usr_elements rue WITH (NOLOCK) RIGHT JOIN usr.elements e WITH (NOLOCK) ON (rue.element_id = e.id) WHERE e.component_id = @component_id and rue.role_id = @roleID`
)

func (s sqlserver) Create(m *Model) error {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(sqlInsert)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en Create component: %v", err)
		return err
	}
	defer stmt.Close()

	ID, err := db.ExecGettingID(
		stmt,

		sql.Named("name", m.Name),

		sql.Named("url_front", m.UrlFront),

		sql.Named("class", m.Class),

		sql.Named("module_id", m.ModuleId),
	)
	if err != nil {
		logger_trace.Error.Printf("ejecutando la consulta en Create component: %v", err)
		return err
	}
	m.ID = ID

	return nil
}

func (s sqlserver) Update(ID int64, m *Model) error {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(sqlUpdate)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en Update component: %v", err)
		return err
	}
	defer stmt.Close()

	err = db.ExecAffectingOneRow(
		stmt,

		sql.Named("name", m.Name),

		sql.Named("url_front", m.UrlFront),

		sql.Named("class", m.Class),

		sql.Named("module_id", m.ModuleId),

		sql.Named("id", ID),
	)
	if err != nil {
		logger_trace.Error.Printf("ejecutando la consulta en Update component: %v", err)
		return err
	}
	m.ID = ID

	return nil
}

func (s sqlserver) Delete(ID int64) error {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(sqlDelete)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en Delete component: %v", err)
		return err
	}
	defer stmt.Close()

	err = db.ExecAffectingOneRow(
		stmt,
		sql.Named("id", ID),
	)
	if err != nil {
		logger_trace.Error.Printf("ejecutando la consulta en Delete component: %v", err)
		return err
	}

	return nil
}

func (s sqlserver) GetByID(ID int64) (*Model, error) {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(sqlGetByID)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en GetByID component: %v", err)
		return nil, err
	}
	defer stmt.Close()

	row := stmt.QueryRow(sql.Named("id", ID))
	return s.scanRow(row)
}

func (s sqlserver) GetAll() (Models, error) {
	conn := db.GetConnection()
	ms := make(Models, 0)

	stmt, err := conn.Prepare(sqlGetAll)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en GetAll component: %v", err)
		return nil, err
	}
	defer stmt.Close()

	rows, err := stmt.Query()
	if err != nil {
		logger_trace.Error.Printf("ejecutando la consulta en GetAll component: %v", err)
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		m, err := s.scanRow(rows)
		if err != nil {
			logger_trace.Error.Printf("escaneando el registro en component: %v", err)
			return ms, err
		}

		ms = append(ms, *m)
	}

	return ms, nil
}

func (s sqlserver) scanRow(rs db.RowScanner) (*Model, error) {
	m := &Model{}
	cn := pq.NullTime{}
	un := pq.NullTime{}

	err := rs.Scan(
		&m.ID,

		&m.Name,

		&m.UrlFront,

		&m.Class,

		&m.ModuleId,

		&cn,
		&un,
	)
	if err != nil {
		logger_trace.Error.Printf("escaneando el modelo component: %v", err)
		return nil, err
	}

	m.CreatedAt = cn.Time
	m.UpdatedAt = un.Time
	return m, nil
}

func (s sqlserver) GetElementByComponent(componentID, roleID int) (ElementByComoponents, error) {
	conn := db.GetConnection()
	ms := make(ElementByComoponents, 0)

	stmt, err := conn.Prepare(sqlGetElementByComponent)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en GetAll component: %v", err)
		return nil, err
	}
	defer stmt.Close()

	rows, err := stmt.Query(
		sql.Named("role_id", roleID),
		sql.Named("component_id", componentID),
		sql.Named("roleID", roleID),
	)
	if err != nil {
		logger_trace.Error.Printf("ejecutando la consulta en GetAll component: %v", err)
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		m := &ElementByComoponent{}
		err := rows.Scan(
			&m.ElementID,
			&m.ElementName,
			&m.ElementDescription,
			&m.Privilege,
		)
		if err != nil {
			logger_trace.Error.Printf("escaneando el modelo component: %v", err)
			return nil, err
		}

		ms = append(ms, *m)
	}
	return ms, nil
}
